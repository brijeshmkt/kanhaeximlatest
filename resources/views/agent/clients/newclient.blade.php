@extends('admin.layouts.layouts')
@section('content')
<div class="entry_form">
    <div class="container">
        <div>
            <button onclick="goBack()"><i class="fas fa-arrow-left back-btn"></i></button><br><br>
            <script>
            function goBack() {
            window.history.back();
            }
            </script>
            <style type="text/css">
        .nbtn{
              background: blue;
    color: white;
    padding: 8px 20px;
    text-transform: uppercase;
    font-weight: bold;
    border: 1px solid blue;
    display: block;
    text-align: center;
    margin-bottom: 20px;
    margin: 10px auto;
        }
      </style>
        </div>
        <br>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <h3>Add client</h3><br><br><br>
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        <form action="{{ url('agent/addnewclient') }}" method="post" id="myform">
            @csrf
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Client Name*:</label></div>
                    <div class=""><input type="text" @if(old('clientname'))  value="{{ old('clientname') }}" @endif  name="clientname" required="required" id="client_name"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Firm Name*:</label></div>
                    <div class=""><input type="text" @if(old('firmname'))  value="{{ old('firmname') }}" @endif  name="firmname" required="required" id="client_firm_name"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Firm Address*:</label></div>
                    <div class=""><input type="text"  @if(old('firmaddress'))  value="{{ old('firmaddress') }}" @endif   name="firmaddress" required="required" id="client_firm_adrs"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>City*:</label></div>
                    <div class="place autocomplete_txt"><input name="city" @if(old('city'))  value="{{ old('city') }}" @endif   type="text" required="required" id="client_city"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>State*:</label></div>
                    <div class="place autocomplete_txt"><input name="state" @if(old('state'))  value="{{ old('state') }}" @endif type="text" required="required" id="client_state"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Country*:</label></div>
                    <div class="place autocomplete_txt"><input name="country" @if(old('country'))  value="{{ old('country') }}" @endif  type="text" required="required" id="client_country"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Office Address*:<div class="" style="width: 106%;"><input type="checkbox" style="width: 5%"name="checkbox" id="same-address"><span>Address same as Firm Address</span></div></label></div>
                    <div class=""><textarea name="officeaddress" required="required" id="address" id="client_office_adrs">@if(old('officeaddress')) {{ old('officeaddress') }}" @endif </textarea></div>

                </div>
            </div>
            <h3>Person To Contact :- </h3>
            <input type="checkbox" name="checkbox" id="same-as-above"><label for="same_as_above">Same as Above</label>
            <div class="frm_middle clearfix">
                <div class="input clearfix remark">
                    <div class="label_cont autocomplete_txt"><label>Name*:</label></div>
                    <div class=""><input type="text" required="required" name="pcname" @if(old('pcname'))  value="{{ old('pcname') }}" @endif  id="person_name"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Phone no*:</label></div>
                    <div class="place autocomplete_txt"><input name="phoneno" @if(old('phoneno'))  value="{{ old('phoneno') }}" @endif type="text" required="required" id="person_phoneno" onkeypress="isInputNumber(event)></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Office Number:</label></div>
                    <div class="place autocomplete_txt"><input name="officeno" @if(old('officeno'))  value="{{ old('officeno') }}" @endif type="text"  id="person_office_no" onkeypress="isInputNumber(event)></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Landline Number:</label></div>
                    <div class="place autocomplete_txt "><input name="landlineno" @if(old('landlineno'))  value="{{ old('landlineno') }}" @endif type="text"  id="person_landline_number" onkeypress="isInputNumber(event)></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>GST No*:</label></div>
                    <div class="place autocomplete_txt"><input name="gstno" @if(old('gstno'))  value="{{ old('gstno') }}" @endif type="text" required="required" id="person_gstno"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Email*:</label></div>
                    <div class="place autocomplete_txt"><input name="email" @if(old('email'))  value="{{ old('email') }}" @endif  type="email" required="required" id="person_email"></div>
                </div>
            </div>
             
            <input  type="submit"  style="background: blue;padding: 8px 20px;color: white;border: 1px solid blue;font-weight: bold;display: block;text-align: center; margin-bottom: 20px;margin: 10px auto; width:10%;" value="submit" id="submit">
        </form>
    </div>
</div>
</div>
@endsection
@section('js')


<script type="text/javascript">
function isInputNumber(evt){
    var char=String.fromCharCode(evt.which);
     if(!(/[0-9]/.test(char)))
     {
        evt.preventDefault();
     }
}
</script>
<script>
$(document).ready(function(){
$("#same-as-above").click(function(){
var cname = $('#client_name').val();
$('#person_name').val(cname);
})
$("#same-address").click(function(){
var cadrs = $('#client_firm_adrs').val();
$('#address').val(cadrs);
})
});
</script>
@endsection