    @extends('agent.layouts.layouts')
    @section('content')
    <div class="entry_form">
        <div class="container">
            <div>
                <a href="{{url('agent/clients')}}"><button class="btn btn-primary">Go Back</button></a>
               </div>
            <br>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <h3 style="font-size: 24px;font-family: inherit;margin-top: 20px">Edit client</h3>
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            <br><br><br>
            @foreach($clients as $client)
            <form action="{{ url('agent/submiteditclient/') }}/{{$client->id}}" method="post">
                @csrf
                <div class="frm_middle clearfix">
                    <div class="input clearfix ">
                        <div class="label_cont autocomplete_txt"><label>Client Name:</label></div>
                        <div class=""><input type="text"  name="clientname" value="{{ $client->Name}}" required="required"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix ">
                        <div class="label_cont autocomplete_txt"><label>Firm Name:</label></div>
                        <div class=""><input type="text"  name="firmname" value="{{ $client->firmname}}" required="required"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix ">
                        <div class="label_cont autocomplete_txt"><label>Firm Address:</label></div>
                        <div class=""><input type="text"  name="firmaddress" value="{{ $client->firmaddress}}" required="required"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>City:</label></div>
                        <div class="place autocomplete_txt"><input name="city" value="{{ $client->city}}" type="text" required="required"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>State:</label></div>
                        <div class="place autocomplete_txt"><input name="state" value="{{ $client->state}}" type="text" required="required"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>Country:</label></div>
                        <div class="place autocomplete_txt"><input name="country" value="{{ $client->country}}" type="text" required="required"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix ">
                        <div class="label_cont autocomplete_txt"><label>Office Address:</label></div>
                        <div class=""><textarea name="officeaddress" required="required" id="address">{{ $client->officeaddress }}</textarea></div>
                    </div>
                </div>
                <h3>Person To Contact :- </h3>
                <div class="frm_middle clearfix">
                    <div class="input clearfix remark">
                        <div class="label_cont autocomplete_txt"><label>Name:</label></div>
                        <div class=""><input type="text"  name="pcname" value="{{ $client->pcname }}" required="required"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>Phone Number</label></div>
                        <div class="place autocomplete_txt"><input name="phoneno" value="{{ $client->PhoneNo }}"  type="number" required="required"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>Office Number</label></div>
                        <div class="place autocomplete_txt"><input name="officeno" value="{{ $client->officeno }}"  type="number" ></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>Landline Number</label></div>
                        <div class="place autocomplete_txt"><input name="landlineno" value="{{ $client->landlineno }}"  type="number"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>GST No:</label></div>
                        <div class="place autocomplete_txt"><input name="gstno" value="{{ $client->GstNumber }}"   type="text"></div>
                    </div>
                </div>
                <div class="frm_middle clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>Email:</label></div>
                        <div class="place autocomplete_txt"><input name="email"  type="email" value="{{ $client->email }}" required="required"></div>
                    </div>
                </div>
                <input class="btn btn-primary placeorder" type="submit" value="submit" id="submit">
            </form>
            @endforeach
        </div>
    </div>
    </div>
    @endsection