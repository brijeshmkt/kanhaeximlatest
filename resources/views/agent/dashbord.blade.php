@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Agents</p>
@endsection
@section('content')
 <div class="dashboard-box">
  <div class="dashboard-row-col">
        <a href="{{ url('agent/inventary') }}">
          <i class="fas fa-warehouse"></i><span>Inventory</span>
        </a>
      </div>
  <div class="dashboard-row-col">
        <a href="{{ url('agen/order/allOrder') }}">
          <div>
          <i class="fas fa-cart-plus"></i>
        </div>
        <span> Orders</span>
      </a>
</div>

<div class="dashboard-row-col">
        <a href="{{ url('agent/viewallclient') }}">
          <div>
          <i class="fas fa-plus-square"></i>
        </div>
        <span>Create Order</span>
      </a>
</div>
  </div>

@endsection