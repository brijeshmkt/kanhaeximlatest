<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Kanha Exim</title>
		<!-- Bootstrap -->
		<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/style.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/media.css')}}" rel="stylesheet">
		{{-- <link href="{{ asset('css/bootstrap-material-design.min.css')}}" rel="stylesheet"> --}}
		<link href="{{ asset('assets/css/font-awesome.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css')}}">
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/material-icons.min.css?v1.1.2')}}"/>@yield('css')


	</head>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

<div class="header">
    <div class="container">
        <div class="header_inner clearfix">
          <div class="header_left">
            <div class="logo"><a href="#"><img src="{{ asset('assets/images/logo.png')}}"></a></div>

          </div>
          <div class="header_right">
            <div class="head_contact">
              <a href="#"><i class="fa fa-phone" aria-hidden="true"></i><span>0123456789</span></a>
              <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i><span>dumy@gmail.com</a>
            </div>
            <a href="#">kanhaexim@gmail.com</a>
          </div>
        </div>
      </div>
  </div>

  <div class="agent_tab">
  	<div class="container">
  		<ul>
  			<li><a href="{{ url('agent/clients') }}">Home(new order)</a></li>
  			<li><a href="{{ url('agent/orderdetails') }}">All Orders</a></li>
  			<li><a href="{{ url('agent/pendingorder') }}">pending Orders</a></li>
  			<li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
					{{ __('Logout') }}
				</a>

				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form></li>
  		</ul>
  	</div>
  </div>
<div class="container table">
	<div class="card m-b-30">
		@yield('content')

	</div>
</div>

 <div class="footer">
    <div class="container">
    <div class="address">
      <span>Address: </span>
      <p>Plot No. 128-132, Nr. Gulab Nagar, Opp. Suez Farm  Road Behrampura, Ahmedabad - 380022</p>
    </div>
  </div>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery-3.3.1.js')}}"></script>
	<script src="{{ asset('assets/js/jquery-ui.min.js')}}" type="text/javascript"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
	{{--
	<script src="{{ asset('assets/js/jquery.min.js')}}" type="text/JavaScript"></script> --}} {{--
	<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script> --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.dataTables.css')}}">
	<script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{ asset('assets/js/custom.js')}}" type="text/JavaScript"></script>
@yield('js')

</body>
</html>