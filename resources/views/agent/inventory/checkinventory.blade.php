@extends('agent.layouts.layouts')
@section('content')
<center><h3>Inventory Not Available!</h3></center>
<center><h4>Place As Requested order?</h4></center>
{{-- {{ dd($in->id) }} --}}
<form method="post" action="{{ url('agent/orderform/requestorder')}}/{{$in->id}}">
	@csrf
<br>
<center><input type="submit" class="btn btn-default" value="Yes" /><a href="{{url('agent/clients')}}"><input type="button" class="btn btn-default" value="No" /></a></center>
<input type="hidden" name="clientid" value="{{ $in->id }}">
<input type="hidden" name="clientname" value="{{ $in->clientname }}">
<input type="hidden" name="firmname" value="{{ $in->firmname }}">
<input type="hidden" name="firmaddress" value="{{ $in->firmaddress }}">
<input type="hidden" name="phoneno" value="{{ $in->phoneno }}">
<input type="hidden" name="gstnumber" value="{{ $in->gstnumber }}">
<input type="hidden" name="date" value="{{ $in->date }}">
<input type="hidden" name="agent" value="{{ $in->agent }}">
<input type="hidden" name="station" value="{{ $in->station }}">
<input type="hidden" name="tp" value="{{ $in->tp }}">
<input type="hidden" name="payment" value="{{ $in->payment }}">
<input type="hidden" name="terms" value="{{ $in->terms }}">
<input type="hidden" name="remark" value="{{ $in->remark }}">
@foreach($in->trade_name as $key=>$value)
<input type="hidden" name="trade_name[{{ $key }}]" id="tname" value="{{$in->trade_name[$key]}}" />
<input type="hidden" name="design_no[{{ $key }}]" id="tname" value="{{$in->design_no[$key]}}" />
<input type="hidden" value="{{ $in->DesignNo }}" >
<input type="hidden" name="rate[{{ $key }}]"  value="{{$in->rate[$key]}}" />
<input type="hidden" name="quantity[{{ $key }}]"  value="{{$in->rate[$key]}}" />
@endforeach
</form>
@endsection