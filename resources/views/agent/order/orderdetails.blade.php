@extends('agent.layouts.layouts')
@section('content')
<h2 style="font-size: 18px;font-family: inherit">Orders</h2>
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
 <div class="row table">
          <div class="row table">
          <div class="col-sm-12 ">
        <table id="datatable" class="table" style="width:100%">
 				<tr>
 					<th>Order Id</th>
 					<th>Client Name</th>
 					<th>Client Number</th>
 					<th>Design Number</th>
 				</tr>
        @foreach($order_details as $od)
         @if($od['Confirm'] == null)
        <tr>
        <td>{{ $od['id'] }}</td>
        <td>{{ $od->client['Name'] }}</td>
        <td>{{ $od->client['id'] }}</td>
        <td>@foreach($od->orderproducts->pluck('DesignNo') as $dno)
          {{ $dno }} , &nbsp; @endforeach</td>
        </tr>
          @endif
          @endforeach
      </table>
      <div class="row">
        <div class="col-sm-12 col-md-5">
          <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $order_details->total() }} entries</div>
        </div>
        <div class="col-sm-12 col-md-7">
          <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
            {{ $order_details->links() }}

          </div>
        </div>
      </div>
    </div>
 	</div>
 </div>
</div>
@endsection