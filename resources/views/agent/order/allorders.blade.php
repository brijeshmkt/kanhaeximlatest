
@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Orders - All Orders</p>
@endsection
@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<br>
<div class="container">
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer admin_order">
    <h4 class="mt-0 header-title">Search</h4>
   

        @if(Request::is('admin/*'))
                @endif
        <div class="row table">
            <div class="col-sm-3 col-sm-offset-10">
               
            </div>
             <div class="row table">
                <div class="col-sm-12 ">
                    @if(isset($search) )
                    <table id="datatable" class="table" style="width:100%">
                        <tr>
                            <th>Order Number</th>
                            <th>Firm Name</th>
                            <th>Client Name</th>
                            <th>Client Mobile No.</th>
                            <th>Order Agent Name</th>
                            <th>Date</th>
                            <th>Orders (Designs)</th>
                            <th>Color</th>
                            <th>Quantity</th>
                            <th>Action</th>
                            <th>PDF</th>
                        </tr>
                           @foreach($entries as $entry)
                        <tr>
                            <td>{{ $entry->id }}</td>
                            <td>{{ $entry->client['firm_name'] }}</td>
                            <td>{{ $entry->client['client_name'] }}</td>
                            <td>{{ $entry->client['contact_person_phone_number'] }}</td>
                            <td>{{ $entry->users['name'] }}</td>
                            <td>{{ $entry->created_at->format('d/m/Y h:i:A') }}</td>
                            <td>@foreach($entry->orderproducts as $design) @foreach($design->designs as $design_no){{ $design_no->number}}, @endforeach @endforeach</td>
                            <td>@foreach($entry->orderproducts as $inventory){{ $inventory->designs[0]->inventory->color }} , @endforeach</td>
                            <td>@foreach($entry->orderproducts as $quantity){{ $quantity->quantity }} ,@endforeach</td>
                            <td><a class="a-btn"
                            href="@if(Request::is('admin/*')){{url('admin/getId')}}/{{ $entry->id }}@elseif(Request::is('manager/*')){{url('manager/getId')}}/{{ $entry->id }}@endif">Approve</a>
                            @if(Request::is('admin/*'))<a class="a-btn" id="{{ $entry->id }}" onclick="deletedata(this.id)"
                            >Delete</a>@endif
                        </td>
                        <td>
                            <a class="a-btn"  href="{{url('admin/download_order_pdf')}}/{{ $entry->id }}">Download</a>
                        </td>
                        @endforeach
                    </tr>
                </table>
                    @else
                    <table id="datatable" class="table" style="width:100%">
                        <tr>
                            <th>Order Number</th>
                            <th>Firm Name</th>
                            <th>Client Name</th>
                            <th>Client Mobile No.</th>
                            <th>Order Agent Name</th>
                            <th>Date</th>
                            <th>Orders (Designs)</th>
                            <th>Color</th>
                            <th>Quantity</th>
                         
                              @if(AUTH::user()->type == 'admin')
                            <th>Conframation</th>
                            @endif
                            <th>PDF</th>
                        </tr>
                        @foreach($entries as $entry)
                        <tr>
                            <td>{{ $entry->id }}</td>
                            <td>{{ $entry->client['firm_name'] }}</td>
                            <td>{{ $entry->client['client_name'] }}</td>
                            <td>{{ $entry->client['contact_person_phone_number'] }}</td>
                            <td>{{ $entry->users['name'] }}</td>
                            <td>{{ $entry->created_at->format('d/m/Y h:i:A') }}</td>
                            <td>@foreach($entry->orderproducts as $design) @foreach($design->designs as $design_no){{ $design_no->number}}, @endforeach @endforeach</td>
                            <td>


                            </td>
                            <td>@foreach($entry->orderproducts as $quantity){{ $quantity->quantity }} ,@endforeach</td>
                         
                             @if(AUTH::user()->type == 'admin')
                           <td>
                            @if($entry->approval == 1)
                            <a style="background-color:green;" class="a-btn"  href="{{url('admin/admin_approval')}}/{{ $entry->id }}">Approve</a>
                            @endif
                             @if($entry->approval == 0)
                             <a style="background-color:red;" class="a-btn"  href="{{url('admin/admin_approval')}}/{{ $entry->id }}">Unapprove</a>
                              @endif
                             </td>
                             @endif

                        <td>
                            <a class="a-btn"  href="{{url('admin/download_order_pdf')}}/{{ $entry->id }}">Download</a>
                        </td>
                        @endforeach
                    </tr>
                </table>
                @endif
                  @if(isset($search))
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total @if($search->total() != null) {{ $search->total() }} @else 0 @endif entries</div>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                            @if($search->links() != null) {{ $search->links() }} @endif
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total @if($entries->total() != null) {{ $entries->total() }} @else 0 @endif entries</div>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                            @if($entries->links() != null) {{ $entries->links() }} @endif
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('js')
<script>
function deletedata(id) {
    swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this Order !",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            var tr = $('#table_input tr').length
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "@if(isset($value)){{url('admin/getId')}}/{{ $value->id }} @elseif(isset($entry)) {{url('admin/order/deleteentry')}}/"+id+" @endif ",
            })
            swal("Alert! Order has been deleted!", {
                icon: "success",
            }).then(() => {
                location.reload(true);
            });
        } else {
            swal("Your Order is safe!");
        }
    });
};
</script>
@endsection