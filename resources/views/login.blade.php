<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Kanha Exim</title>
    <!-- Bootstrap -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css')}}" rel="stylesheet">
    {{-- <link href="{{ asset('css/bootstrap-material-design.min.css')}}" rel="stylesheet"> --}}
    <link href="{{ asset('assets/css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/material-icons.min.css?v1.1.2')}}"/>

  </head>
  <body class="admin">
    @if (Route::has('login'))
    @auth
    <a href="{{ url('/') }}">Home</a>
    @else
    <div class="login_frm clearfix">
    <div class="brandname"><p>Kanha Exim</p></div>
      <form method="POST" action="{{ route('login') }}">
        @csrf
        <h2>Login</h2>
        <form method="post">
          <div class="input">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" @if(isset($_COOKIE['email']) && $_COOKIE['email'] != null) value="@php print_r($_COOKIE['email']); @endphp" @else value="{{ old('email') }}" @endif required autofocus placeholder="Enter Username">
            @if ($errors->has('email'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
          </div>
          <div class="input">
            <input id="password" placeholder="Enter password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" @if(isset($_COOKIE['password']) && $_COOKIE['password'] != null) value="@php print_r($_COOKIE['password']); @endphp" @endif required>
            @if ($errors->has('password'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
          </div>
          <div class="form-group row">
            <div class="col-md-6 offset-md-4">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="remember" @if(isset($_COOKIE['email']) && $_COOKIE['email'] != null) checked @endif {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                </label>
              </div>
            </div>
          </div>
          <div class="login_submit"><input type="submit" value="Log In" class="a-btn"></div>
          <a class="chng_pass" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
        </form>
      </div>
    </form>
    @endauth
    @endif
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap-material-design.js"></script>
    <script src="js/custom.js" type="text/JavaScript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
  </body>
</html>