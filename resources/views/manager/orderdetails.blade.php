@extends('admin.layouts.layouts')
@section('content')
<div>
<button class="btn btn-primary" onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>
<br>
<h4>Request Order Detail</h4><br><br>
    <div class="pendingorders">
    <label>Order Id: </label>
    <span>{{ $order['id'] }}</span>
  </div>
    <div class="pendingorders">
    <label>Order Placed On: </label>
    <span>{{ $order['Date'] }}</span>
    </div>
    <div class="pendingorders">
    <label>Client Name: </label>
    <span>{{ $client['Name'] }}</span>
  </div>
    <div class="pendingorders">
    <label>Address: </label>
    <span>{{ $client['Address'] }}</span>
    </div>
  <div class="pendingorders">
    <label>Phone Number: </label>
    <span>{{ $client['PhoneNo'] }}</span>
    </div>
  <div class="pendingorders">
    <label>GST Number: </label>
    <span>{{ $client['GstNumber'] }}</span>
  </div>
    <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
 <div class="row table">
          <div class="row table">
          <div class="col-sm-12 ">
            <table id="datatable" class="table" style="width:100%">
        <tr>
          <th>Trade Name</th>
          <th>Design Number</th>
          <th>Quantity</th>
          <th>Price</th>
        </tr>
        @foreach($order->orderproducts as $op)
        <tr>
            <td>{{ $op['TradeName'] }}</td>
            <td>{{ $op['DesignNo'] }}</td>
            <td>{{ $op['Quantity'] }}</td>
            <td>{{ $op['Price'] }}</td>
         </tr>
        @endforeach
      </table>

    </div>
  </div>

@endsection
