@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Dash Board</p>
@endsection
@section('content')
    <div class="dashboard-box">
 
      <div class="dashboard-row-col">
        <a href="{{ url('manager/console') }}">
          <i class="fas fa-user-cog"></i><span>Admin Console</span>
        </a>
      </div>

      <div class="dashboard-row-col">
        <a href="{{ url('manager/millreport') }}">
          <i class="fas fa-industry"></i><span>Mill Reports</span>
        </a>
      </div>

      <div class="dashboard-row-col">
        <a href="{{ url('manager/inventory') }}">
          <i class="fas fa-warehouse"></i><span>Inventory</span>
        </a>
      </div>

      <div class="dashboard-row-col">
        <a href="{{ url('manager/order') }}">
          <i class="fas fa-cart-plus"></i><span>Orders</span>
        </a>
      </div>

      <div class="dashboard-row-col">
        <a href="{{ url('manager/viewallclient') }}">
          <i class="fas fa-plus-square"></i><span>Create Order</span>
        </a>
      </div>

      <div class="dashboard-row-col">
        <a href="{{ url('manager/packaging') }}">
          <i class="fas fa-cubes"></i><span>Packaging</span>
        </a>
      </div>

      <div class="dashboard-row-col">
        <a href="{{ url('manager/notifications') }}">
          <i class="fas fa-bell"></i><span>{{ count($notificationproviders) }}<br> Notifications</span>
          <!-- <span class="badge badge-success a-animate-blink noti-icon-badge"> -->  
        </a>
      </div>
      
    
  </div>

@endsection