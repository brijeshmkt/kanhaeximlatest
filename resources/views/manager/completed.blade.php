@extends('admin.layouts.layouts')
@section('content')
<h2 style="font-size: 18px;font-family: inherit">Completed Orders</h2>
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
  <div class="row table">
    <div class="row table">
      <div class="col-sm-12 ">
        <table id="datatable" class="table" style="width:100%">
          <tr>
            <th>Order Number</th>
            <th>Design Number</th>
            <th>Order Date</th>
            <th>Details</th>
          </tr>
          @foreach($entries as $entry)
          <tr>
            <td>{{ $entry['id'] }}</td>
            <td>@foreach($entry->orderproducts->pluck('DesignNo') as $dno)
            {{ $dno }} ,@endforeach</td>
            <td>{{ $entry['Date'] }}</td>
            <td><a class="btn btn-primary placeorder" href="{{ url('manager/completedorders/orderdetails') }}/{{ $entry['id'] }}">Details</a></td>
          </tr>

          @endforeach
        </table>
        <div class="row">
          <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $entries->total() }} entries</div>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
              {{ $entries->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection