@extends('admin.layouts.layouts')
@section('content')
 <h2 style="font-size: 18px;font-family: inherit">Requested Orders</h2>
 <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
 <div class="row table">
          <div class="row table">
          <div class="col-sm-12 ">
            <table id="datatable" class="table" style="width:100%">
        <tr>
          <th>Order Number</th>
          <th>Order Date</th>
          <th>Design Number</th>
          <th>Action</th>
        </tr>

        @foreach($entries as $entry)
            <tr>
            <td>{{ $entry['id'] }}</td>
            <td>{{ $entry['Date'] }}</td>
            <td>@foreach($entry->orderproducts->pluck('DesignNo') as $dno)
            {{ $dno }} @endforeach</td>
            <td><a class="btn btn-primary placeorder" href="{{ url('manager/requestedorders/orderdetails') }}/{{ $entry['id'] }}">Details</a></td>
          </tr>
        @endforeach
      </table>
      <div class="row">
        <div class="col-sm-12 col-md-5">
          <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $entries->total() }} entries</div>
        </div>
        <div class="col-sm-12 col-md-7">
          <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
            {{ $entries->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
 </div>
</div>
@endsection
