<div class="pendingorders">
    <h1>Packing Slip</h1>
    <label></label>
    <span>{!! DNS1D::getBarcodeHTML($order_details['id'],'C93') !!}</span>
    <br>
    <label>Date: </label>
    <?php $date = date("d/m/y")?>
    <span>{{ $date }}</span>
    <br>
    <label>Bill Number:- </label>
    <span>101</span>
    <br>
    <label>Order Id: </label>
    <span>{{ $order_details['id'] }}</span>
	</div>
    <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
 	<div class="row table">
    <div class="row table">
    <div class="col-sm-12 ">
    <table id="datatable" class="table" style="width:100%">
    <tr>
        <th></th>
        <th>Design Number</th>
        <th>Particulars</th>
        <th>Pieces</th>
        <th>Meters</th>         
        <th>Unit</th>
    </tr>
    <?php $totalmeters=0; $totalquantity=0; ?>
    @foreach($order_details->orderproducts as $op) 
    <tr>
        <td>Great</td>
        <td>{{ $op['DesignNo'] }}</td>
        <td>{{ $op['TradeName'] }}</td>
        <td>{{ $op['Quantity'] }}</td>
        <?php $totalquantity += intval($op['Quantity']); ?>
        <td>{{ $op['Size'] }}</td>
        <?php $totalmeters += floatval($op['Size']); ?>
        <td>MTS</td>

    </tr>
    @endforeach
    <tfoot>
        <tr>
            <th>Total: </th>
            <td>-</td>
            <td>-</td>
            <td>{{ $totalquantity }}</td>
            <td>{{ $totalmeters }}</td>
            <td>-</td>
        </tr>
    </tfoot>
    </table>
	</div>
    </div>