@extends('admin.layouts.layouts')
@section('content')
<div>
<button class="btn btn-primary" onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>

</div>

	<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
   <div class="row table">
          <div class="row table">
          <h3><b>Order Details</b></h3><br>
           @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

          <div class="col-sm-12">
        <table id="datatable" class="table" style="width:100%">
        <thead>
        <tr>
          <th>Order Number</th>
          <th>Trade Name</th>
          <th>Design Number</th>
          {{-- <th>Colour</th> --}}
          <th>Quantity</th>
        </tr>
      </thead>
      <tbody>
        @foreach($pdetails->orderproducts as $orderproduct)
          <tr>
          <input type="hidden" name="orderid" id="orderid" value="{{ $pdetails['id'] }}">
          <td>{{ $pdetails['id'] }}</td>
          <td>{{ $orderproduct['TradeName'] }}</td>
          <td>{{ $orderproduct['DesignNo'] }}</td>
          {{-- <td>{{ $orderproduct['colour'] }}</td> --}}
          <td>{{ $orderproduct['Quantity'] }}</td>
          </tr>
        @endforeach
      </tbody>
      </table>
    </div>
  </div>
<form action="{{ url('manager/order/PackingOrder') }}" method="POST">
              @csrf
	<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
   <div class="row table">
    <div class="row table">
      <label><b>Scanned Orders</b></label>
        <div class="col-sm-12">
        <table id="datatable" class="table" style="width:100%">
        <thead>
        <tr>
          <input type="hidden" name="orderid" id="orderid" value="{{ $pdetails['id'] }}">

          <th>Design Number</th>
          <th>TradeName</th>
          <th>Colour</th>
          <th>Id</th>
        </tr>
        </thead>
        <tbody id="table_pack"></tbody>
        </table>
        <label>Barcode:</label>
        <br>
        <input type="text" name="barcode" id="bcode"><br>
      <input type="submit" class="btn btn-primary placeorder" name="submit" >
      {{-- <a class="btn btn-primary placeorder" id="#packorder" href="{{ url('manager/barcodeCheck') }}/{{ $pdetails['id'] }}">Pack</a> --}}

    </div>
  </div>
</form>
@endsection

@section('js')
<script>
  var count = 0;
         var myTextbox = document.getElementById("bcode");
         myTextbox.onchange = function() {
          var allBarCode = $('.BarcodeNo');
          var allBarCodeArray = [];
          for(var i = 0; i < allBarCode.length; i++){
              allBarCodeArray.push($(allBarCode[i]).val());
          }

          console.log(allBarCodeArray);
             var barcode = myTextbox.value;
             var orderid=$('#orderid').val();
             $.ajax({
                type: "GET",
                data:{ 'barcode':barcode },
                url: "{{ url('manager/barcodeCheck') }}/"+ orderid,
                success: function(Inventory){
                  if(Inventory==0)
                  {
                    alert('Invalid Entry!!!');
                  }
                  else
                  {

                    if(allBarCodeArray.includes(Inventory.Barcode)){
                      alert('Inventory Already Exist');
                    }
                    else
                    {
                      var html = "<tr><td><input type='hidden' name='barCode[]' class='BarcodeNo'  value='" + Inventory.Barcode +"'>" + Inventory.DesignNo + "</td><td>" + Inventory.TradeName+ "</td><td>" + Inventory.colour + "</td><td>" + Inventory.id + "<input type='hidden' name='inventory_ids[]' value=" + Inventory.id + "></td>";
                      $('#table_pack').append(html);
                    }

                    // var allBarCodeNo = allBarCodeArray.toString();
                    // console.log(allBarCodeNo);
                    // if(allBarCodeNo == Inventory.Barcode)
                    // {
                    //   alert('hello');
                    // }
                    // var html = "<tr><td><input type='hidden' class='BarcodeNo' value='" + Inventory.Barcode +"'>" + Inventory.DesignNo + "</td><td>" + Inventory.TradeName+ "</td><td>" + Inventory.colour + "</td><td>" + Inventory.id + "<input type='hidden' name='inventory_ids[]' value=" + Inventory.id + "></td>";
                    // $('#table_pack').append(html);
                  }
                  console.log(Inventory);
                }
             });
             $('#bcode').val("");
          }

         // var count = 0;
         // var myTextbox = document.getElementById("bcode");
         // myTextbox.onchange = function() {
         //     var id = myTextbox.value;
         //     $.ajax({
         //         type: "GET",
         //         url: "{{ url('manager/checkinventory') }}/" + id,
         //         success: function(data1) {
         //             if (!data1) {
         //                 alert("Inventory Out of Stock!!!");
         //             } else if (data1 == '2') {
         //                 alert("Invalid Entry!!!");
         //             } else {
         //                 var html = "<tr><td>" + data1.DesignNo + "</td><td>" + data1.Size + "</td><td>" + data1.id + "<input type='hidden' name='inventory_ids[]' value=" + data1.id + "></td>";
         //                 $('#table_pack').append(html);
         //                 count += 1;

         //                 console.log(count);
         //             }
         //         }
         //     })
         //     $('#bcode').val("");
         // };
         // $('#quant').val('24');

         // $(document).ready(function() {
         //     $(window).keydown(function(event) {
         //         if (event.keyCode == 13) {
         //             event.preventDefault();
         //             return false;
         //         }
         //     });
         // });


         //      // $('#packorder').click(function(){
         //      //   count = 0;
         //      // });

</script>
@endsection