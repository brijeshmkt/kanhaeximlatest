@extends('admin.layouts.layouts')
@section('content')
<br>
<br>
<div>
  <button class="btn btn-primary" onclick="goBack()">Go Back</button>
  <script>
  function goBack() {
  window.history.back();
  }
  </script>
</div>
<br>
<div class="pendingorders">
  <label>Client Details: </label>
  <label>Name: </label>
  <span>{{ $client['Name'] }}</span>
</div>
<div class="pendingorders">
  <label>Address: </label>
  <span>{{ $client['Address'] }}</span>
</div>
<div class="pendingorders">
  <label>Phone Number: </label>
  <span>{{ $client['PhoneNo'] }}</span>
</div>
<div class="pendingorders">
  <label>GST Number: </label>
  <span>{{ $client['GstNumber'] }}</span>
</div>
<div class="pendingorders">
  <label>Phone Number: </label>
  <span>{{ $client['PhoneNo'] }}</span>
</div>
<div class="pendingorders">
  <label>Order placed on:</label>
  <span>{{ $order['Date'] }}</span>
</div>
<div class="pendingorders">
  <label>Station:</label>
  <span>{{ $order['Station'] }}</span>
</div>
<div class="pendingorders">
  <label>Transport:</label>
  <span>{{ $order['T_p'] }}</span>
</div>
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
  <div class="row table">
    <label>Products</label>
    <div class="col-sm-12 ">
      <table id="datatable" class="table" style="width:100%">
        <tr>
          <th>Trade Name</th>
          <th>Design Number</th>
          <th>Quantity</th>
          <th>Price</th>
        </tr>
        @foreach($order->orderproducts as $op)
        <tr>
          <td>{{ $op['TradeName'] }}</td>
          <td>{{ $op['DesignNo'] }}</td>
          <td>{{ $op['Quantity'] }}</td>
          <td>{{ $op['Price'] }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    @endsection