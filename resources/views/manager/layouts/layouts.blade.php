<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Kanha Exim</title>
	<!-- Bootstrap -->
	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/css/style.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/css/media.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/css/font-awesome.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/material-icons.min.css?v1.1.2')}}" /> @yield('css')
	
</head>


<body class="admin manager">
	<div class="admin_inner clearfix">
		{{-- <div class="ad_left">
			<div class="ad_logo">
				<a href="#"><img src="{{ asset('assets/images/kelogo.png')}}"></a>
			</div>
			@include('manager.layouts.sidebar')
		</div> --}}
		<div class="ad_right">
			<div id="navbar_custom">
				<div class="nav_top_left pull-left">
					<form role="search" class="">
						<a href="{{ url('/') }}"><img class="logo" src="{{ asset('assets/images/kelogo.png')}}"></a>
					</form>
				</div>
				<ul class="list-inline pull-right mb-0 mr-3 pull-right">
					<!-- language-->
					{{-- <li class="list-inline-item dropdown notification-list">
						<a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
							<i class="fa fa-envelope noti-icon" aria-hidden="true"></i>
							<span class="badge badge-danger heartbit noti-icon-badge">5</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg" x-placement="bottom-end" style="position: absolute; top: 71px; left: -216px; will-change: top, left;">
							<!-- item-->
							<div class="dropdown-item noti-title align-self-center">
								<h5><span class="badge badge-danger pull-right">745</span>Messages</h5>
							</div>
							<!-- item-->
							<a href="javascript:void(0);" class="dropdown-item notify-item">
								<div class="notify-icon"><img src="{{ asset('assets/images/avatar-2.jpg')}}" alt="user-img" class="img-fluid rounded-circle"> </div>
								<p class="notify-details"><b>Charles M. Jones</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
							</a>
							<!-- item-->
							<a href="javascript:void(0);" class="dropdown-item notify-item">
								<div class="notify-icon"><img src="{{ asset('assets/images/avatar-3.jpg')}}" alt="user-img" class="img-fluid rounded-circle"> </div>
								<p class="notify-details"><b>Thomas J. Mimms</b><small class="text-muted">You have 87 unread messages</small></p>
							</a>
							<!-- item-->
							<a href="javascript:void(0);" class="dropdown-item notify-item">
								<div class="notify-icon"><img src="{{ asset('assets/images/avatar-4.jpg')}}" alt="user-img" class="img-fluid rounded-circle"> </div>
								<p class="notify-details"><b>Luis M. Konrad</b><small class="text-muted">It is a long established fact that a reader will</small></p>
							</a>
							<!-- All-->
							<a href="javascript:void(0);" class="dropdown-item notify-item">
								View All
							</a>
						</div>
					</li> --}}
					{{-- <li class="list-inline-item dropdown notification-list">
						<a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
							<i class="fa fa-bell noti-icon" aria-hidden="true"></i>
							<span class="badge badge-success a-animate-blink noti-icon-badge">3</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h5><span class="badge badge-danger pull-right">87</span>Notification</h5>
							</div>
							<!-- item-->
							<a href="javascript:void(0);" class="dropdown-item notify-item">
								<div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
								<p class="notify-details"><b>Your order is placed</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
							</a>
							<!-- item-->
							<a href="javascript:void(0);" class="dropdown-item notify-item">
								<div class="notify-icon bg-success"><i class="mdi mdi-message"></i></div>
								<p class="notify-details"><b>New Message received</b><small class="text-muted">You have 87 unread messages</small></p>
							</a>
							<!-- item-->
							<a href="javascript:void(0);" class="dropdown-item notify-item">
								<div class="notify-icon bg-warning"><i class="mdi mdi-martini"></i></div>
								<p class="notify-details"><b>Your item is shipped</b><small class="text-muted">It is a long established fact that a reader will</small></p>
							</a>
							<!-- All-->
							<a href="javascript:void(0);" class="dropdown-item notify-item">
								View All
							</a>
						</div>
					</li> --}}
					<li class="list-inline-item dropdown notification-list">
						<a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
							<img src="{{ asset('assets/images/avatar-1.jpg')}}" alt="user" class="rounded-circle img-thumbnail">
						</a>
						<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h5>Welcome</h5>
							</div>
							{{-- <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Profile</a> --}}
							{{-- <a class="dropdown-item" href="#"><i class="mdi-wallet m-r-5 text-muted"></i> My Wallet</a> --}}
							{{-- <a class="dropdown-item d-block" href="#"><span class="badge badge-success pull-right">5</span><i class="mdi mdi-settings m-r-5 text-muted"></i> Settings</a> --}}
							{{-- <a class="dropdown-item" href="#"><i class="mdi-lock-open-outline m-r-5 text-muted"></i> Lock screen</a> --}}
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	</div>

	<div class="container table">
		<div class="card m-b-30 admin_body">
			@yield('content')
		</div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	{{--
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}

	<script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery-3.3.1.js')}}"></script>
	<script src="{{ asset('assets/js/jquery-ui.min.js')}}" type="text/javascript"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
	{{--
	<script src="{{ asset('assets/js/jquery.min.js')}}" type="text/JavaScript"></script> --}} {{--
	<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script> --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.dataTables.css')}}">
	<script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{ asset('assets/js/custom.js')}}" type="text/JavaScript"></script>
	@yield('js')



</body>

</html>
