<div class="menu" id="menu">
  <div id="sidebar-menu">
    <ul class="sidebar_menu_inner">
      <li>
        <a href="{{ url('manager/dashboard') }}">
          <i class="fa fa-tachometer" aria-hidden="true"></i>
          <span>Dashboard</a>
      </li>
      <li class="has_sub">
        <a href="{{ url('manager/pendingorders') }}" class="waves-effect">
          <i class="fa fa-cubes" aria-hidden="true"></i>
          <span>Pending Orders</span>
        </a>
      </li>
      <li class="has_sub">
        <a href="{{ url('manager/completedorders') }}" class="waves-effect">
          <i class="fa fa-cubes" aria-hidden="true"></i>
          <span>Completed Orders</span>
        </a>
      </li>
      <li class="has_sub">
        <a href="{{ url('manager/dashboard/requestedorders') }}" class="waves-effect">
          <i class="fa fa-cubes" aria-hidden="true"></i>
          <span>Requested Orders</span>
        </a>
      </li>
    </ul>
  </div>
</div>
