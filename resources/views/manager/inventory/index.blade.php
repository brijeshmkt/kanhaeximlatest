@extends('admin.layouts.layouts')
@section('content')
<div class="card m-b-30 admin_body">
  <div class="card-body">
    <div class="general-label">
    </div>
    <div class="card-body">
      <h4 class="mt-0 header-title">Inventory</h4>
      <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="datatable_length"></div></div></div>
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="row table"><div class="col-sm-12">
        <table id="datatable" class="display" style="width:100%">
          <thead>
            <tr>
              <th>Design No</th>
              <th>Size</th>
              <th>Tread name</th>
              <th>Barcode</th>
            </tr>
          </thead>
          <tbody>
            @foreach($inventories as $inventory)
            <tr>
              <td>{{ $inventory->rowproduct['DesignNo'] }}</td>
              <td>{{ $inventory->Size }}</td>
              <td>{{ $inventory->rowproduct['TradeName'] }}</td>
              <td><input type="text" name="barcode"></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div></div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
@endsection