<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta http-equiv="X-UA-Compatible" content="IE
		=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	    <meta name="viewport" content="width=device-width,initial-scale=1">
	    <meta name="apple-touch-fullscreen" content="yes">
	    <meta name="format-detection" content="telephone=no">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Kanha Exim</title>
		<!-- Bootstrap -->
		<link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/style.css')}}" rel="stylesheet">
		<!-- <link href="{{ URL::TO('css/style.css')}}" rel="stylesheet"> -->
		<link href="{{ asset('assets/css/media.css')}}" rel="stylesheet">
		<!-- <link href="{{ asset('assets/css/font-awesome.css')}}" rel="stylesheet"> -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

		<link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css')}}">
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/material-icons.min.css?v1.1.2')}}" />
		<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
		<link href="{{ asset('assets/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css">
		@yield('css')
		
		<script type="text/javascript" charset="utf8" src="{{ asset('js/jquery-3.3.1.js')}}"></script>
		
	</head>

	<body class="admin">
	<header>
		<div id="navbar_custom" class="clearfix">
			<div class="nav_top_left pull-left">
				<a href="/admin/dashboard"><img class="logo" src="{{ asset('images/kelogo.png')}}"></a>
			</div>
			<div class="sectiontitle">@yield('sectiontitle')</div>
			<ul class="list-inline mb-0 mr-3 pull-right">

				@if(isset($notificationproviders))
				<!-- language-->
				<li class="list-inline-item dropdown notification-list">
					<a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
						<i class="fa fa-bell noti-icon" aria-hidden="true"></i>
						<span class="badge badge-success a-animate-blink noti-icon-badge"> {{ count($notificationproviders) }} </span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
						<!-- item-->
						<div class="dropdown-item noti-title">
							<h5><span class="badge badge-danger pull-right">{{ count($notificationproviders) }} </span><a style="color: white; " onMouseOver="this.style.color='orange'" onMouseOut="this.style.color='white'" href="{{url('admin/notifications')}}">Notification</a></h5>
						</div>
						<!-- item-->
						@foreach($notificationproviders as $n)
						<a href="javascript:void(0);" class="dropdown-item notify-item">
							<div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
							<p class="notify-details"><b>{{$n->title}}</b><small class="text-muted">{{ $n->description }}.</small></p>
						</a>
						@endforeach
						<!-- All-->
						<a href="{{url('admin/notifications')}}" class="dropdown-item notify-item">
							View All
						</a>
					</div>
				</li>
				@endif

				<li class="list-inline-item dropdown notification-list">
					<a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
						<img src="{{ asset('images/avatar-1.jpg')}}" alt="user" class="rounded-circle img-thumbnail">
					</a>
						<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h5>Welcome</h5>
							</div>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="{{ route('logout') }}"
								onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">
								{{ __('Logout') }}
							</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</div>
				</li>
			</ul>
		</div>
	</header>
		<div class="table">
			<div class="card m-b-30 admin_body">
				@yield('content')
			</div>
		</div>

		<center><footer class="copyright">Developed and Maintained By <b><a href="https://coderadobe.com/">Coderadobe.com</a></b></footer></center>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		
		<script src="{{ asset('js/jquery-ui.min.js')}}" type="text/javascript"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="{{ asset('js/bootstrap.min.js')}}"></script>

		<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
		<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
		<script src="{{ asset('js/tag-it.js')}}" type="text/JavaScript"></script>
		@yield('js')
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		
		<script src="{{ asset('js/custom.js')}}" type="text/JavaScript"></script>
	</body>
</html>