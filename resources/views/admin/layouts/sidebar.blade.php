<div class="menu" id="menu">
    <div id="sidebar-menu">
        <ul class="sidebar_menu_inner">

            <li>
                <a href="{{ url('admin/dashboard') }}">
                    <i class="fa fa-tachometer" aria-hidden="true"></i>
                    <span>Dashboard</a>
            </li>
            <li class="has_sub">
                <a href="{{ route('rowproducts.index') }}">
                    <i class="fa fa-cubes" aria-hidden="true"></i>
                    <span>Mill Report</span>
                </a>
            </li>
            <li class="has_sub">
                <a href="{{ url('admin/inventory') }}" class="waves-effect">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span>
              Inventory
            </span>
                </a>
            </li>
            <li class="has_sub">
                <a href="{{ url('admin/pendingorders') }}" class="waves-effect">
                    <i class="fa fa-address-book"></i>
                    <span>Pending Orders</span>
                </a>
            </li>
            <li class="has_sub">
                <a href="{{ url('admin/order') }}" class="waves-effect">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span>New Order</span>
                </a>
            </li>
            {{-- <span class="float-right"><i class="mdi mdi-chevron-right"></i><i class="fa fa-angle-down" aria-hidden="true"></i></span></a> --}}
            {{-- <ul class="list-unstyled">
              <li><a href="{{ url('admin/oreder') }}">Add Order</a></li>
              <li><a href="{{ url('admin/oreders') }}">All Orders</a></li>
            </ul> --}}
            <li class="has_sub">
                <a href="{{ url('admin/alluser') }}">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                </a>
            </li>
            <li class="has_sub">
                <a href="{{ url('admin/viewallclient') }}">
                    <i class="fa fa-users"></i>
                    <span>Clients</span>
                </a>
            </li>
            </li>
        </ul>
    </div>
</div>

