@extends('admin.layouts.layouts')
@section('content')
<div class="row">
<div class="col-lg-4 col-md-6 col-sm-6 ">
		<div class="dashboard_cont">
			<div class="dash_icon">
				<a href="{{ url('admin/pendingorders') }}">
					<img src="{{ asset('assets/images/pending_orders.png')}}">
				</div>
				<h4>Pending Orders</h4>
				{{-- <span>{{ $completed }}</span> --}}
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-6 col-sm-6 ">
		<div class="dashboard_cont">
			<div class="dash_icon">
				<a href="{{ url('admin/requestedorders') }}">
					<img src="{{ asset('assets/images/requested_oreder.png')}}">
				</div>
				<h4>Requested Orders</h4>
				{{-- <span>{{ $completed }}</span> --}}
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-6 col-sm-6 ">
		<div class="dashboard_cont">
			<div class="dash_icon">
				<a href="{{ url('admin/inventory') }}">
					<img src="{{ asset('assets/images/completed_order.png')}}">
				</div>
				<h4>Inventory</h4>
				{{-- <span>{{ $completed }}</span> --}}
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-6 col-sm-6 ">
		<div class="dashboard_cont">
			<div class="dash_icon">
				<a href="{{ url('admin/completedorders') }}">
					<img src="{{ asset('assets/images/completed_order.png')}}">
				</div>
				<h4>Un Added Mill Report</h4>
				{{-- <span>{{ $completed }}</span> --}}
			</a>
		</div>
	</div>
</div>
@endsection