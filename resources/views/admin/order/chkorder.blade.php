@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Clients - Process Order</p>
@endsection
@section('content')
<div class="entry_form">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="container">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        @include('flash::message')
        <h3 style="text-align: center;">Place Order</h3><br>
        <div class="user-info order_details">
            <label style="text-decoration: underline;">Client Details:</label>
            @foreach ($entries as $entries)

            <ul>

                <li><label>Date:</label><span>{{ $entries->client->created_at->format('d/m/Y H:i:s') }}</span></li>
                <li><label>Client Name:</label><span>{{ $entries->client->client_name}}</span></li>
                <li><label>Firm Name:</label><span>{{ $entries->client->firm_name }}</span></li>
                <li><label>Firm Address:</label><span>{{ $entries->client->firm_address }}</span></li>
                <li><label>Phone No:</label><span>{{ $entries->client->contact_person_phone_number }}</span></li>
                <li><label>GstNumber:</label><span>{{ $entries->client->gst_number }}</span></li>

            </ul>
        </div>

        <form action="{{ url('admin/order/partialProcess') }}" method="post" id="orderform">
            @csrf
            <input type="hidden" name="order_id" value="{{ $entries->id }}">
            <input type="hidden" name="clientid" value="{{ $entries->client->id }}">
            <input type="hidden" name="clientname" value="{{ $entries->client->client_name }}">
            <input type="hidden" name="firmname" value="{{ $entries->client->firm_name }}">
            <input type="hidden" name="firmaddress" value="{{ $entries->client->address }}">
            <input type="hidden" name="phoneno" value="{{ $entries->client->contact_person_phone_number }}">
            <input type="hidden" name="gstnumber" value="{{ $entries->client->gst_number }}">
            <input type="hidden" name="date" value="{{ $entries->client->created_at }}">
            <div class="frm_top clearfix">
                <div class="frm_bottom clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>Agent Name:</label></div>
                        <div class="place agent_name"><input name="agent" type="text" value="{{$entries->users->name}}" readonly="readonly"></div>
                    </div>
                    <div class="input clearfix">
                        <div class="label_cont"><label>Station:</label></div>
                        <div class="place"><input type="text"   value="{{$entries->destination_station }}"  name="station" id="station" readonly="readonly"></div>
                    </div>
                    <div class="input clearfix">
                        <div class="label_cont"><label>Transporter name:</label></div>
                        <div class="place">
                            <input type="text" name="transporter_name" value="{{ $entries->transporter_name }}" readonly="readonly">
                        </div>
                    </div>
                </div>
                <div class="trade_mark clearfix">
                    <table width="100%" id="table">
                        <thead>
                            <tr>
                                <th class="heading">Trade Name</th>
                                <th class="heading">Design No</th>
                                <th class="heading">Quantity(per piece meter)</th>
                                <th class="heading">Rate</th>
                            </tr>
                        </thead>

<tbody id="table_input">

	@foreach($entries->orderproducts as $order_products)
    <tr class="order-row">
        <td>
            <input type="hidden" name="orderLength[]" value="null">

            <select name="tradename_id[]"  class="selectpicker trade-name">
                <option value="">Select Trade Name</option>

                <option value="{{$order_products->tradename_id}}" selected="selected">{{gettradename($order_products->tradename_id)}}</option>
            </select>

        </td>
        <td>

    	<input type="text" class="" placeholder="Enter Design Number" name="design_no[]" value=" {{getdesignnumber($order_products->design_id)}}" readonly="readonly">
        </td>
        <td>

            <input type="number" name="quantity[]" value="{{$order_products->quantity}}" placeholder="Quantity" id="quantity" readonly="readonly">


        </td>
        <td>
            <input type="number" name="rate[]"  value="{{$order_products->rate}}" placeholder="Rate" id="rateInput" readonly="readonly">
        </td>


    </tr>

    @endforeach


</tbody>

</table>
                    </div>




                <div id="response"></div>

                <div class="mill-btns">

                    <button id="check-inventory">Check Inventory</button>
                    <input class="submit-btn" type="submit" value="submit" id="submit">
                    <a href="{{ url()->previous() }}" class="submit-btn">Cancel</a>
                </div>
            </form>
        </div>
    </div>
    @endsection
 @endforeach
@section('js')
<script>

    $( document ).ready(function() {
        $('.submit-btn').hide();

        var designNumbers;

        //Get all designs


        $.ajax({
                url:'/admin/design/all-design-numbers',
                type:'Get',
                success:function(result){
                    designNumbers =  result;
                    $(".myTags").tagit({
                        availableTags: result,
                        placeholderText: 'Enter Design numbers'
                    });
                }
        });



    });

    $('#check-inventory').click(function(event){
        event.preventDefault();
        // orderform
        console.log('check');

        $.ajax({
                url:'/admin/order/check-order',
                type:'POST',
                data:$('#orderform').serialize(),
                success:function(result){
                    //console.log(result);
                    $("#response").html(result);
                    $('.submit-btn').show();
                }

        });
    });



    $('.trade-name').on('change', function (e) {

        var tradename_id = $( this ).val();
        var uri = '/admin/design/by-tradename-ajax/' + tradename_id ;
        var targetElement = $( this ).parent().parent().find('.select-design');

        $.get(uri, function(data){
            html = '<option value="">Select Design</option>';

            $.each(data, function(key, value) {
                html += "<option value='" + value.id + "'>" + value.number + "</option>";
            });

            targetElement.html(html);
        });

    });

</script>

@endsection
