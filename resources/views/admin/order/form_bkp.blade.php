@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Clients - Place Order</p>
@endsection
@section('content')
<div class="entry_form">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="container">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        @include('flash::message')
        <h3 style="text-align: center;">Place Order</h3><br>
        <div class="user-info order_details">
            <label style="text-decoration: underline;">Client Details:</label>
            <ul>
                @foreach($client as $clients)
                <li><label>Date:</label><span>{{ $clients->created_at->format('d/m/Y H:i:s') }}</span></li>
                <li><label>Client Name:</label><span>{{ $clients->client_name }}</span></li>
                <li><label>Firm Name:</label><span>{{ $clients->firm_name }}</span></li>
                <li><label>Firm Address:</label><span>{{ $clients->firm_address }}</span></li>
                <li><label>Phone No:</label><span>{{ $clients->contact_person_phone_number }}</span></li>
                <li><label>GstNumber:</label><span>{{ $clients->gst_number }}</span></li>
                @endforeach
            </ul>
        </div>
        <form action="{{ url('admin/order/checkinventory') }}/{{ $clients->id }}" method="post" id="orderform">
            @csrf
            <input type="hidden" name="clientid" value="{{ $clients->id }}">
            <input type="hidden" name="clientname" value="{{ $clients->client_name }}">
            <input type="hidden" name="firmname" value="{{ $clients->firm_name }}">
            <input type="hidden" name="firmaddress" value="{{ $clients->address }}">
            <input type="hidden" name="phoneno" value="{{ $clients->contact_person_phone_number }}">
            <input type="hidden" name="gstnumber" value="{{ $clients->gst_number }}">
            <input type="hidden" name="date" value="{{ $clients->created_at }}">
            <div class="frm_top clearfix">
                <div class="frm_bottom clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>Agent Name:</label></div>
                        <div class="place agent_name"><input name="agent" type="text" @if(old('agent')) value="{{ old('agent') }}" @else value={{\Auth::user()->name}} @endif></div>
                    </div>
                    <div class="input clearfix">
                        <div class="label_cont"><label>Station:</label></div>
                        <div class="place"><input type="text"  @if(old('station')) value="{{ old('station') }}" @endif name="station" id="station" required="required"></div>
                    </div>
                    <div class="input clearfix">
                        <div class="label_cont"><label>Transpoter name:</label></div>
                        <div class="place"><input type="text" name="tp" @if(old('tp')) value="{{ old('tp') }}" @endif  required="required"></div>
                    </div>
                </div>
                <div class="trade_mark clearfix">
                    <table width="100%" id="table">
                        <thead>
                            <tr>
                                <th class="heading">Trade Name</th>
                                <th class="heading">Design No</th>
                                <th class="heading">Quantity(per piece meter)</th>
                                <th class="heading">Rate</th>
                                <th class="heading"></th>
                            </tr>
                        </thead>
                        <tbody id="table_input">
                            @if(old('trade_name'))
                            @foreach(old('trade_name') as $key=>$value)
                            <tr>
                                <td>
                                    <select name="trade_name[{{ $key }}]" id="tname" class="selectpicker" required="required" data-size="7" data-search="true">
                                        @foreach($tradenames as $tradename)
                                        <option value="{{ $tradename->id }}" >{{ $tradename->tradename }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select name="design_no[{{ $key }}]" id="tname" class="selectpicker" required="required" title="Trade Name" data-size="7">
                                        @foreach($inventoryproviders as $inventoryprovider)
                                        <option @if(old('design_no[$key]')) selected="selected" @endif value="{{ $inventoryprovider->DesignNo }}" >{{ $inventoryprovider->DesignNo }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="number" name="rate[{{ $key }}]" @if(old('rate')[$key]) value="{{ old('rate')[$key] }}" @endif placeholder="Rate" id="rateInput" required="required">
                                </td>
                                <td>
                                    <input type="number" name="quantity[{{ $key }}]"  @if(old('quantity')[$key]) value="{{ old('quantity')[$key] }}" @endif placeholder="Quantity" id="quantity" required="required">
                                </td>
                                <td>
                                    <i class="fa fa-trash addButton" id="removeButton"></i>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td>
                                    <select name="trade_name[0]" id="tname" class="selectpicker" required="required" data-size="7" data-search="true" data-id = 0>
                                        <option value="" >Select Trade Name</option>
                                        @foreach($tradenames as $tradename)
                                        <option value="{{ $tradename->id }}" >{{ $tradename->tradename }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select name="design_no[0]" id="design_no" required="required" title="Design No" data-size="7">
                                        <option value="">Select Design No</option>
                                    </select>
                                </td>
                                <td><input type="number" name="quantity[0]" placeholder="Quantity" id="quantity"
                                required="required"></td>
                                <td><input type="number" name="rate[0]" placeholder="Rate" id="rateInput" value="0"></td>
                            </tr>
                            @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            @if(old('trade_name'))
                            <td colspan="5"><i class="fas fa-plus-square addButton" data-listid ="{{ count(old()['trade_name']) }}"  id="addButton"></i></td>
                            @else
                            <td colspan="5"><i class="fas fa-plus-square addButton" data-listid = 0  id="addButton"></i></td>@endif
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="frm_middle clearfix">
                        <div class="input clearfix remark">
                            <div class="label_cont"><label>Payment Tearm :</label></div>
                            <select name="payment" required="required">
                                <option value="10 - 15 Days-> 3%">10 - 15 Days-> 3%</option>
                                <option value="30 Days-> 2% cash discount">30 Days-> 2% cash discount </option>
                                <option value="730 Days-> Net">730 Days-> Net</option>
                                <option value="Other-> 18% Interest">Other-> 18% Interest</option>
                            </select>
                        </div>
                    </div>
                <label><input type="checkbox" required="required"  @if(old('terms')) checked="checked" @endif style="zoom:1.5;vertical-align: top;"name="terms" id="tnc" >&nbsp;<span>Terms & Condition</span></label>
                <div class="frm_middle clearfix">
                    <div class="input clearfix remark">
                        <div class="label_cont"><label>Remark:</label></div>
                        <div class=""><textarea name="remark"> @if(old('remark')) {{ old('remark') }}" @endif</textarea></div>
                    </div>
                </div>
                <div class="mill-btns">
                    <input class="submit-btn" type="submit" value="submit" id="submit">
                    <a href="{{ url()->previous() }}" class="submit-btn">Cancel</a>
                </div>
            </form>
        </div>
    </div>
    @endsection
    @section('js')
    <script>
    var lastId;
    $("#addButton").click(function() {
    lastId = $(this).data('listid');
    lastId++;
    var html = '<tr><td><select name="trade_name[' + lastId + ']" id="tname" class="selectpicker" data-size="7"><option value="" >Select Trade Name</option>@foreach($tradenames as $tradename)<option value="{{ $tradename->id }}" >{{ $tradename->tradename }}</option>@endforeach</select></td><td><select name="design_no[' + lastId + ']" id="design_no" class="selectpicker" required="required" title="Design No" data-size="7"></select></td><td><input type="number" class="quantity" name="quantity[' + lastId + ']" id="quantity" placeholder="Quantity"></td><td><input type="number" class="rate" name="rate[' + lastId + ']" id="rateInput" placeholder="Rate" value="0"></td><td><i class="fa fa-trash addButton" id="removeButton"></i></td></tr>';
    $(this).data('listid', lastId);
    $('#table_input').append(html);
    $("body").on("click", "#removeButton", function() {
    $(this).parent().parent().remove();
    });
    $('select[name="trade_name['+ lastId +']"]').click(function(e){
    // console.log($(e).attr('id'));
    var val = $(this).val();
    console.log(val);
    var select = $(this).attr("id");
    var value = $(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
    method: "POST",
    data:{select:select, value:value, _token:_token},
    url: "{{ url("admin/search-design-number") }}",
    success: function(result){
    $('select[name="design_no['+ lastId +']"]').html(result);
    }
    })
    })
    });
    $(document).click(function(){
    var val = $('select[name="design_no[1]"]').val();
    // console.log(val);
    // str = "select[name='trade_name["+lastId+"]']";
    // console.log(str);
    })
    $("#tname").change(function(){
    // var id = $("#addButton").data('listid');
    var select = $(this).attr("id");
    var value = $(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
    method: "POST",
    data:{select:select, value:value, _token:_token},
    url: "{{ url("admin/search-design-number") }}",
    success: function(result){
    $("#design_no").html(result);
    }
    })
    });
    $("#tradeInput").autocomplete({
    type: "GET",
    dataType: "JSON",
    source: "{{ url('/findtradenumber') }}",
    select: function(event, ui) {
    // console.log(ui.item.value)
    var name = ui.item.value
    $("#designInput").autocomplete({
    type: "GET",
    dataType: "JSON",
    source: "{{ url('/findtradenumber') }}/" + name,
    select: function(event, ui) {
    console.log("Hello")
    }
    });
    }
    });
    </script>
    @endsection