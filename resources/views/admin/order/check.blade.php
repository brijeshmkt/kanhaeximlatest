
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	}
	table {
	  width: 100%;
	}
</style>

<table>
  <tr>
    <th>Trade Name</th>
    <th>Ordered Quantity</th>
    <th>Rate</th>
    <th>Available</th>
  </tr>


@foreach ($results as $result)
<tr>
	<td>{{ $result['tradename'] }}</td>

	<td>{{ $result['Quantity'] }}</td>
	<td>{{ $result['Rate'] }}</td>


	<td>

			@foreach ( $result['inventories'] as $inventories )
			<div class="trade-name-inventories">
				<div class="trade-name-inventory">
					Design Number: {{ $inventories['design_number'] }}
				</div>

				<div class="trade-name-inventory">
					@foreach ( $inventories['inventory'] as $inventory )

						{{ $inventory->color }} - {{ $inventory->color_count }}
						<br>
					@endforeach
				</div>


			</div>
			@endforeach



	</td>



</tr>
@endforeach

</table>