@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Orders</p>
@endsection
@section('content')
 <div class="dashboard-box">
  <div class="dashboard-row-col">
        <a href="{{ url('admin/order/allOrder') }}">
          <div>
          <i class="fas fa-cart-plus"></i>
        </div>
        <span>All Order</span>
      </a>
</div>
  <div class="dashboard-row-col">

        <a href="{{ url('admin/order/pendingorder') }}">
          <i class="fab fa-product-hunt"></i>
        <span>Partial Orders</span>
      </a>
      </div>
  <div class="dashboard-row-col">
        <a href="{{ url('admin/order/requestedorder') }}">
          <i class="fas fa-exclamation-circle"></i>
        <span>Requested Orders<br>(Out Of Stock)</span>
      </a>
  </div>
  <div class="dashboard-row-col">
        <a href="{{ url('admin/order/confirmorder') }}">
        <i class="fas fa-check-circle"></i>
        <span>Completed Orders</span>
      </a>
    </div>
<div class="dashboard-row-col">
        <a href="{{ url('admin/approvalorder') }}">
        <i class="fas fa-cubes"></i>
        <span>Approval By Admin <br> Orders</span>
      </a>
    </div>
<div class="dashboard-row-col">
        <a href="{{ url('admin/viewallclient') }}">
          <div>
          <i class="fas fa-plus-square"></i>
        </div>
        <span>Create Order</span>
      </a>
</div>

  </div>

@endsection