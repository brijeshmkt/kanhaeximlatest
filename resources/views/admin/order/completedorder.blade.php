@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Completed Orders</p>
@endsection
@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<br>
<div class="container">
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
    <h4 class="mt-0 header-title">Search</h4>
    @if(AUTH::user()->type == 'admin')
    <form action="{{ url('admin/order/completedorder/search') }}" method="get">
        @else
        <form action="{{ url('manager/order/completedorder/search') }}" method="get">
            @endif
            <input type="text" name="aname" placeholder="Agent Name">
            <input type="text" name="mobile" placeholder="Client Mobile No" >
            <input type="text" name="gstno" placeholder="Client Gst No" >
            <button type="submit" class="a-btn" name="submit">Submit</button>
            @if(isset($search))<a href="{{ url('admin/order/completedorder') }}" class="a-btn">Reset</a>@endif
        </form>
        @if(Request::is('admin/*'))
        <a class="a-btn placeorder" style="float:right;padding: 8px 8px" href="{{ url('admin/order/pendingorder') }}">View Pending Orders</a>
        <a class="a-btn placeorder" style="float:right; padding: 8px 8px;margin-right: 10px;" href="{{ url('admin/order/requestedorder') }}">View Requested Order</a>
        @endif
        <div class="row table">
            <div class="row table">
                <div class="col-sm-12 ">
                    @if(isset($search) )
                    <table id="datatable" class="table" style="width:100%">
                        <tr>
                            <th>Order Number</th>
                            <th>Firm Name</th>
                            <th>Client Name</th>
                            <th>Client Mobile No.</th>
                            <th>Order Agent Name</th>
                            <th>Date</th>
                            <th>Orders</th>
                            <th>Quantity</th>
                            {{-- <th>Action</th> --}}
                        </tr>
                        @foreach($search as $entry)
                       
                        <tr>
                            <td>{{ $entry->id }}</td>
                            <td>{{ $entry->client->firm_name }}</td>
                            <td>{{ $entry->client['client_name'] }}</td>
                            <td>{{ $entry->client['contact_person_phone_number'] }}</td>
                            <td>{{ $entry->users['name'] }}</td>
                            <td>{{ $entry->created_at->format('d/m/Y h:i:A') }}</td>
                            <td>{{ $entry->orderproducts->designs->number }} , &nbsp;</td>
                            <td>{{ $entry->orderproducts->quantity }} , &nbsp;</td>
                            @foreach($search as $key => $value)
                            {{-- <td>
                                @if(Request::is('admin/*'))<a class="btn btn-primary placeorder"
                                href="{{url('admin/getId')}}/{{ $value->id }}">Delete</a>@endif
                            </td> --}}
                            <td>
                                <a class="a-btn"
                                href="{{url('admin/order/packingorder')}}/{{ $value->id }}">Download Invoice</a>
                            </td>
                            @endforeach
                            @endforeach
                        </tr>
                    </table>
                    @else
                    <table id="datatable" class="table" style="width:100%">
                        <tr>
                            <th>Order Number</th>
                            <th>Firm Name</th>
                            <th>Client Name</th>
                            <th>Client Mobile No.</th>
                            <th>Order Agent Name</th>
                            <th>Date</th>
                            <th>Orders</th>
                            <th>Quantity</th>
                            <th>Invoice</th>
                        </tr>
                        @foreach($entries as $entry)
                        @foreach($entry->orderproducts as $orderproducts)
                        <tr>
                            <td>{{ $entry->id }}</td>
                            <td>{{ $entry->client->firm_name }}</td>
                            <td>{{ $entry->client['client_name'] }}</td>
                            <td>{{ $entry->client['contact_person_phone_number'] }}</td>
                            <td>{{ $entry->users['name'] }}</td>
                            <td>{{ $entry->created_at->format('d/m/Y h:i:A') }}</td>
                            <td>{{ $orderproducts->designs[0]->number }}</td>
                            <td>{{ $orderproducts->quantity }}</td>
                            {{-- <td>
                                <a class="btn btn-primary placeorder"
                                href="{{url('admin/getId')}}/{{ $entry->id }}">Confirm</a>
                                @if(Request::is('admin/*'))<a class="btn btn-primary placeorder"
                                href="{{url('admin/order/deleteentry')}}/{{ $entry->id }}">Delete</a>@endif
                            </td> --}}
                            <td>
                                <a class="a-btn"
                                href="{{url('admin/order/packingorder')}}/{{ $entry->id }}">Download</a>
                            </td>
                            @endforeach
                            @endforeach
                        </tr>
                    </table>
                    @endif
                    @if(isset($search))
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total @if($search->total() != null) {{ $search->total() }} @else 0 @endif entries</div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                @if($search->links() != null) {{ $search->links() }} @endif
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total @if($entries->total() != null) {{ $entries->total() }} @else 0 @endif entries</div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                @if($entries->links() != null) {{ $entries->links() }} @endif
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
    @endsection