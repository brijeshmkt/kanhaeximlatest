@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Admin Approved Orders</p>
@endsection
@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<br>
<div class="container">
<div id="datatable_wrapper" class="dataTables_wrapper  container-fluid dt-bootstrap4 no-footer admin_order ad_requested_order">
    <h4 class="mt-0 header-title">Search</h4>
   
    <form action="{{ url('admin/order/Confirm/search') }}" method="get">
        
            <input type="text" name="aname" placeholder="Agent Name">
            <input type="text" name="mobile" placeholder="Client Mobile No" >
            <input type="text" name="gstno" placeholder="Client Gst No" >
            <button type="submit" name="submit">  <i class="fa fa-search" aria-hidden="true"></i></button>
            @if(isset($search))<a href="{{ url('admin/order/requestedorders') }}" class="a-btn">Reset</a>
            @endif
        </form>
        @if(Request::is('admin/*'))
        <div class="inventory_form pull-right">
            <a class="a-btn placeorder" href="{{ url('admin/order/pendingorder') }}">View Pending Orders</a>
            <a class="a-btn placeorder" href="{{ url('admin/order/requestedorders') }}">View Requested Order</a>
        </div>

        @endif
        <div class="row table">
            <div class="row table">
                <div class="col-sm-12 ">
                    @if(isset($search) )
                    <table id="datatable" class="table" style="width:100%">
                        <tr>
                            <th>Order Number</th>
                            <th>Firm Name</th>
                            <th>Client Name</th>
                            <th>Client Mobile No.</th>
                            <th>Order Agent Name</th>
                            <th>Date</th>
                            <th>Orders</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                        @foreach($search as $entry)
                         @if($entry->approval == 1)
                        <tr>
                            <td>{{ $entry->id }}</td>
                            <td>{{ $entry->firmname }}</td>
                            <td>{{ $entry->Name }}</td>
                            <td>{{ $entry->PhoneNo }}</td>
                            <td>{{ $entry->name }}</td>
                            <td>{{ $entry->created_at->format('d/m/Y h:i:A') }}</td>
                            <td>
                                @foreach($entry->orderproducts->pluck('designs') as $dno)
                                @foreach($dno as $d)
                                {{ $d->number }} @if($loop->count > 0),@endif &nbsp;
                                @endforeach
                                @endforeach
                            </td>
                            <td>
                                @foreach($entry->orderproducts->pluck('quantity') as $qu)
                                {{ $qu }} , &nbsp;
                                @endforeach
                            </td>
                            @foreach($search as $key => $value)
                            <td>
                                {{-- <a class="a-btn placeorder" href="{{url('admin/getId')}}/{{ $value->id }}">Confirm</a> --}}
                                @if(Request::is('admin/*'))<a class="a-btn placeorder"
                                href="{{url('admin/getId')}}/{{ $value->id }}">Delete</a>@endif
                            </td>
                            @endforeach
                            @endif
                            @endforeach
                        </tr>
                    </table>
                    @else
                    <table id="datatable" class="table" style="width:100%">
                        <tr>
                            <th>Order Number</th>
                            <th>Firm Name</th>
                            <th>Client Name</th>
                            <th>Client Mobile No.</th>
                            <th>Order Agent Name</th>
                            <th>Date</th>
                            <th>Orders</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                        {{-- {{dd($entries)}} --}}
                        @foreach($entries as $entry)
                        @if($entry->approval == 1)
                        <tr>

                            <td>{{ $entry->id }}</td>
                            <td>{{ $entry->client['firm_name'] }}</td>
                            <td>{{ $entry->client['client_name'] }}</td>
                            <td>{{ $entry->client['contact_person_phone_number'] }}</td>
                            <td>{{ $entry->users['name'] }}</td>
                            <td>{{ $entry->created_at->format('d/m/Y h:i:A') }}</td>
                            <td>@foreach($entry->orderproducts as $design) @foreach($design->designs as $design_no){{ $design_no->number}}, @endforeach @endforeach</td>
                            <td>@foreach($entry->orderproducts as $quantity){{ $quantity->quantity }} ,@endforeach</td>
                            <td>
                            <a class="a-btn placeorder" href="{{url('admin/order/packorder')}}/{{ $entry->id }}">Pack</a>
                            </td>
                            @endif
                            @endforeach
                        </tr>
                    </table>
                    @endif
                    @if(isset($search))
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total @if($search->total() != null) {{ $search->total() }} @else 0 @endif entries</div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                @if($search->links() != null) {{ $search->links() }} @endif
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total @if($entries->total() != null) {{ $entries->total() }} @else 0 @endif entries</div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                                @if($entries->links() != null) {{ $entries->links() }} @endif
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
    @endsection