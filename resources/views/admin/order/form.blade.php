
@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Clients - Place Order</p>
@endsection
@section('content')
<div class="entry_form">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="container">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        @include('flash::message')
        <h3 style="text-align: center;">Place Order</h3><br>
        <div class="user-info order_details">
            <label style="text-decoration: underline;">Client Details:</label>
            <ul>
                @foreach($client as $clients)
                <li><label>Date:</label><span>{{ $clients->created_at->format('d/m/Y H:i:s') }}</span></li>
                <li><label>Client Name:</label><span>{{ $clients->client_name }}</span></li>
                <li><label>Firm Name:</label><span>{{ $clients->firm_name }}</span></li>
                <li><label>Firm Address:</label><span>{{ $clients->firm_address }}</span></li>
                <li><label>Phone No:</label><span>{{ $clients->contact_person_phone_number }}</span></li>
                <li><label>GstNumber:</label><span>{{ $clients->gst_number }}</span></li>
                @endforeach
            </ul>
        </div>

        <form action="{{ url('admin/order/place-order') }}" method="post" id="orderform">
            @csrf
            <input type="hidden" name="clientid" value="{{ $clients->id }}">
            <input type="hidden" name="clientname" value="{{ $clients->client_name }}">
            <input type="hidden" name="firmname" value="{{ $clients->firm_name }}">
            <input type="hidden" name="firmaddress" value="{{ $clients->address }}">
            <input type="hidden" name="phoneno" value="{{ $clients->contact_person_phone_number }}">
            <input type="hidden" name="gstnumber" value="{{ $clients->gst_number }}">
            <input type="hidden" name="date" value="{{ $clients->created_at }}">
            <div class="frm_top clearfix">
                <div class="frm_bottom clearfix">
                    <div class="input clearfix">
                        <div class="label_cont"><label>Agent Name:</label></div>
                        <div class="place agent_name"><input name="agent" type="text" @if(old('agent')) value="{{ old('agent') }}" @else value={{\Auth::user()->name}} @endif></div>
                    </div>
                    <div class="input clearfix">
                        <div class="label_cont"><label>Station:</label></div>
                        <div class="place"><input type="text"  @if(old('station')) value="{{ old('station') }}" @endif name="station" id="station" value="test station" required="required"></div>
                    </div>
                    <div class="input clearfix">
                        <div class="label_cont"><label>Transporter name:</label></div>
                        <div class="place">
                            <input type="text" name="transporter_name" @if(old('tp')) value="{{ old('tp') }}" @endif  required="required" value="Some Transporter">
                        </div>
                    </div>
                </div>
                <div class="trade_mark clearfix">
                
                    <table width="100%" id="table">
                        <thead>
                            <tr>
                                <th class="heading">Trade Name</th>
                                <th class="heading">Design No</th>
                                <th class="heading">Quantity(per piece meter)</th>
                                <th class="heading">Rate</th>

                            </tr>
                        </thead>
<tbody id="table_input">

    <tr class="order-row">
        <td>
            <input type="hidden" name="orderLength[]" value="null">
            <select name="tradename_id[]"  class="selectpicker trade-name">
                <option value="">Select Trade Name</option>
                @foreach($tradenames as $tradename)
                <option value="{{ $tradename->id }}" >{{ $tradename->name }}</option>
                @endforeach
            </select>
        </td>
        <td>
            <input type="text" class="myTags" placeholder="Enter Design Number" name="design_no[]">
        </td>
        <td>
            <input type="number" name="quantity[]" value="" placeholder="Quantity" id="quantity">
        </td>
        <td>
            <input type="number" name="rate[]"  value="" placeholder="Rate" id="rateInput">
        </td>


    </tr>

    <tr class="order-row">
        <td>
            <input type="hidden" name="orderLength[]" value="null">
            <select name="tradename_id[]"  class="selectpicker trade-name">
                <option value="">Select Trade Name</option>
                @foreach($tradenames as $tradename)
                <option value="{{ $tradename->id }}" >{{ $tradename->name }}</option>
                @endforeach
            </select>
        </td>
        <td>
            <input type="text" class="myTags" name="design_no[]">
        </td>
        <td>
            <input type="number" name="quantity[]" value="" placeholder="Quantity" id="quantity">
        </td>
        <td>
            <input type="number" name="rate[]"  value="" placeholder="Rate" id="rateInput">
        </td>


    </tr>

    <tr class="order-row">
        <td>
            <input type="hidden" name="orderLength[]" value="null">
            <select name="tradename_id[]"  class="selectpicker trade-name">
                <option value="">Select Trade Name</option>
                @foreach($tradenames as $tradename)
                <option value="{{ $tradename->id }}" >{{ $tradename->name }}</option>
                @endforeach
            </select>
        </td>
        <td>
            <input type="text" class="myTags" name="design_no[]">
        </td>
        <td>
            <input type="number" name="quantity[]" value="" placeholder="Quantity" id="quantity">
        </td>
        <td>
            <input type="number" name="rate[]"  value="" placeholder="Rate" id="rateInput">
        </td>


    </tr>

    <tr class="order-row">
        <td>
            <input type="hidden" name="orderLength[]" value="null">
            <select name="tradename_id[]"  class="selectpicker trade-name">
                <option value="">Select Trade Name</option>
                @foreach($tradenames as $tradename)
                <option value="{{ $tradename->id }}" >{{ $tradename->name }}</option>
                @endforeach
            </select>
        </td>
        <td>
            <input type="text" class="myTags" name="design_no[]">
        </td>
        <td>
            <input type="number" name="quantity[]" value="" placeholder="Quantity" id="quantity">
        </td>
        <td>
            <input type="number" name="rate[]"  value="" placeholder="Rate" id="rateInput">
        </td>


    </tr>

    <tr class="order-row">
        <td>
            <input type="hidden" name="orderLength[]" value="null">
            <select name="tradename_id[]"  class="selectpicker trade-name">
                <option value="">Select Trade Name</option>
                @foreach($tradenames as $tradename)
                <option value="{{ $tradename->id }}" >{{ $tradename->name }}</option>
                @endforeach
            </select>
        </td>
        <td>
            <input type="text" class="myTags" name="design_no[]">
        </td>

        <td>
            <input type="number" name="quantity[]" value="" placeholder="Quantity" id="quantity">
        </td>
        <td>
            <input type="number" name="rate[]"  value="" placeholder="Rate" id="rateInput">
        </td>


    </tr>
    <div id="addplace">


    </div>


</tbody>


</table>
                <p style="margin-left:83%;"><a class="btn btn-info" id="add">
                      <span class=""></span>Addmore
                    </a>
                  </p>
                    </div>

                    <div class="frm_middle clearfix">
                        <div class="input clearfix remark">
                            <div class="label_cont"><label>Payment Tearm :</label></div>
                            <select name="payment" required="required">
                                <option value="10 - 15 Days-> 3%">10 - 15 Days-> 3%</option>
                                <option value="30 Days-> 2% cash discount">30 Days-> 2% cash discount </option>
                                <option value="730 Days-> Net">730 Days-> Net</option>
                                <option value="Other-> 18% Interest">Other-> 18% Interest</option>
                            </select>
                        </div>
                    </div>
                <label><input type="checkbox" required="required"  @if(old('terms')) checked="checked" @endif style="zoom:1.5;vertical-align: top;"name="terms" id="tnc" >&nbsp;<span>Terms & Condition</span></label>
                <div class="frm_middle clearfix">
                    <div class="input clearfix remark">
                        <div class="label_cont"><label>Remark:</label></div>
                        <div class=""><textarea name="remark"> @if(old('remark')) {{ old('remark') }}" @endif</textarea></div>
                    </div>
                </div>

                <div id="response"></div>

                <div class="mill-btns">

                    <button id="check-inventory">Check Inventory</button>
                    <input class="submit-btn" type="submit" value="submit" id="submit">
                    <a href="{{ url()->previous() }}" class="submit-btn">Cancel</a>
                </div>
            </form>
        </div>
    </div>
    @endsection

@section('js')




<script>

    $( document ).ready(function() {
        $('.submit-btn').hide();

        var designNumbers;

        //Get all designs


        $.ajax({
                url:'/admin/design/all-design-numbers',
                type:'Get',
                success:function(result){
                    designNumbers =  result;
                    $(".myTags").tagit({
                        availableTags: result,
                        placeholderText: 'Enter Design numbers'
                    });
                }
        });
        $('#add').click(function(){
        $(this).preventDefault;
        $('#table').append('<tr class="order-row"><td><input type="hidden" name="orderLength[]" value="null"><select name="tradename_id[]"  class="selectpicker trade-name"><option value="">Select Trade Name</option>@foreach($tradenames as $tradename)<option value="{{ $tradename->id }}" >{{ $tradename->name }}</option>@endforeach</select></td> <td><input type="text" class="myTags" name="design_no[]"></td><td><input type="number" name="quantity[]" value="" placeholder="Quantity" id="quantity"></td><td>            <input type="number" name="rate[]"  value="" placeholder="Rate" id="rateInput"></td></tr>'
            );
        $.ajax({
                url:'/admin/design/all-design-numbers',
                type:'Get',
                success:function(result){
                    designNumbers =  result;
                    $(".myTags").tagit({
                        availableTags: result,
                        placeholderText: 'Enter Design numbers'
                    });
                }
        });
    });



    });

    $('#check-inventory').click(function(event){
        event.preventDefault();
        // orderform
       // console.log('check');

        $.ajax({
                url:'/admin/order/check-order',
                type:'POST',
                data:$('#orderform').serialize(),
                success:function(result){
                 // console.log(result{});
                    $("#response").html(result);
                    $('.submit-btn').show();
                }

        });
    });



    $('.trade-name').on('change', function (e) {

        var tradename_id = $( this ).val();
        var uri = '/admin/design/by-tradename-ajax/' + tradename_id ;
        var targetElement = $( this ).parent().parent().find('.select-design');

        $.get(uri, function(data){
            html = '<option value="">Select Design</option>';

            $.each(data, function(key, value) {
                html += "<option value='" + value.id + "'>" + value.number + "</option>";
            });

            targetElement.html(html);
        });


    });







</script>

@endsection
