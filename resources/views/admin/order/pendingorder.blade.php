
@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Orders - Pending Orders</p>
@endsection
@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<br>
<div class="container">
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer admin_order">
    <h4 class="mt-0 header-title">Search</h4>
    @if(AUTH::user()->type == 'admin')
    <form action="{{ url('admin/partial/search') }}" method="get" id="myform">
        @else
        <form action="{{ url('manager/partial/search') }}" method="get">
            @endif
            <input type="text" required="required" id="aname" name="aname" placeholder="Agent Name">
            <input type="text" required="required" id="mobile" name="mobile" placeholder="Client Mobile No" >
            <input type="text" required="required" id="gstno" name="gstno" placeholder="Client Gst No" >
            <button type="submit" class="a-btn" name="submit">Submit</button>
            @if(isset($search))<a href="{{ url('admin/order/pendingorder') }}" class="a-btn">Reset</a>@endif
        </form>
        @if(Request::is('admin/*'))

        @endif
        
        <div class="row table">
            <div class="row table">
                <div class="col-sm-12 ">
                    @if(isset($search) )
                    <table id="datatable" class="table" style="width:100%">
                        <tr>
                            <th>Order Number</th>
                            <th>Firm Name</th>
                            <th>Client Name</th>
                            <th>Client Mobile No.</th>
                            <th>Order Agent Name</th>
                            <th>Date</th>
                            <th>Orders (Designs)</th>
                            <th>Color</th>
                            <th>Quantity</th>
                            <th>Action</th>
                            <th>PDF</th>
                        </tr>
                           @foreach($entries as $entry)
                            @if($entry->approval == 1)
                        <tr>
                            <td>{{ $entry->id }}</td>
                            <td>{{ $entry->client['firm_name'] }}</td>
                            <td>{{ $entry->client['client_name'] }}</td>
                            <td>{{ $entry->client['contact_person_phone_number'] }}</td>
                            <td>{{ $entry->users['name'] }}</td>
                            <td>{{ $entry->created_at->format('d/m/Y h:i:A') }}</td>
                            <td>@foreach($entry->orderproducts as $design) @foreach($design->designs as $design_no){{ $design_no->number}}, @endforeach @endforeach</td>
                            <td></td>
                            <td>@foreach($entry->orderproducts as $quantity){{ $quantity->quantity }} ,@endforeach</td>
                            <td><a class="a-btn"
                            href="@if(Request::is('admin/*')){{url('admin/getId')}}/{{ $entry->id }}@elseif(Request::is('manager/*')){{url('manager/getId')}}/{{ $entry->id }}@endif">Approve</a>
                            @if(Request::is('admin/*'))<a class="a-btn" id="{{ $entry->id }}" onclick="deletedata(this.id)"
                            >Delete</a>@endif
                        </td>
                        <td>
                            <a class="a-btn"  href="{{url('admin/download_order_pdf')}}/{{ $entry->id }}">Download</a>
                        </td>
                        @endif
                        @endforeach
                    </tr>
                </table>
                    @else
                    <table id="datatable" class="table" style="width:100%">
                        <tr>
                            <th>Order Number</th>
                            <th>Firm Name</th>
                            <th>Client Name</th>
                            <th>Client Mobile No.</th>
                            <th>Order Agent Name</th>
                            <th>Date</th>
                            <th>Orders (Designs)</th>
                            <th>Color</th>
                            <th>Quantity</th>
                            <th>Action</th>
                            <th>PDF</th>
                        </tr>
                        @foreach($entries as $entry)
                          @if($entry->approval == 1)
                        <tr>
                            <td>{{ $entry->id }}</td>
                            <td>{{ $entry->client['firm_name'] }}</td>
                            <td>{{ $entry->client['client_name'] }}</td>
                            <td>{{ $entry->client['contact_person_phone_number'] }}</td>
                            <td>{{ $entry->users['name'] }}</td>
                            <td>{{ $entry->created_at->format('d/m/Y h:i:A') }}</td>
                            <td>@foreach($entry->orderproducts as $design) @foreach($design->designs as $design_no){{ $design_no->number}}, @endforeach @endforeach</td>
                            <td></td>
                            <td>@foreach($entry->orderproducts as $quantity){{ $quantity->quantity }} ,@endforeach</td>
                            <td><a class="a-btn" href="{{ url('admin/order/chkpartialOrder') }}/{{ $entry->id }}" method="get">Process</a>

                        </td>
                        <td>
                            <a class="a-btn"  href="{{url('admin/download_order_pdf')}}/{{ $entry->id }}">Download</a>
                        </td>
                        @endif
                        @endforeach
                    </tr>
                </table>
                @endif
                @if(isset($search))
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total @if($search->total() != null) {{ $search->total() }} @else 0 @endif entries</div>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                            @if($search->links() != null) {{ $search->links() }} @endif
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total @if($entries->total() != null) {{ $entries->total() }} @else 0 @endif entries</div>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                            @if($entries->links() != null) {{ $entries->links() }} @endif
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('js')
<script>
function deletedata(id) {
    swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this Order !",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            var tr = $('#table_input tr').length
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "@if(isset($value)){{url('admin/getId')}}/{{ $value->id }} @elseif(isset($entry)) {{url('admin/order/deleteentry')}}/"+id+" @endif ",
            })
            swal("Alert! Order has been deleted!", {
                icon: "success",
            }).then(() => {
                location.reload(true);
            });
        } else {
            swal("Your Order is safe!");
        }
    });
};
</script>
@endsection