@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Admin Console</p>
@endsection
<div class="entry_form">
  <div class="container">
    <div>
      <a class="text-link" href="{{ url()->previous() }}"><i class="fas fa-arrow-left back-btn"></i></a>
      <script>
      function goBack() {
      window.history.back();
      }
      </script>
    </div>
    <br>
    <h2 style="font-size: 18px;font-family: inherit">Create User</h2>
    @if(session()->has('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}
    </div>
    @endif
    @foreach($users as $usr)
    <form action="{{ url('admin/user/update/') }}/{{$usr->id}}" method="post" id="edit-user-form">
      @csrf
      <div class="frm_middle clearfix">
        <div class="input clearfix remark">
          <div class="label_cont autocomplete_txt"><label>Username:</label></div>
          <div class=""><input type="text" value="{{ $usr->name}}" name="uname"></div>
        </div>
      </div>
      <input type="hidden" name="oldpassword" value="{{ $usr->password }}">
      <div class="frm_middle clearfix">
        <div class="input clearfix">
          <div class="label_cont"><label>Email Id:</label></div>
          <div class="place autocomplete_txt">
            <input name="email" value="{{ $usr->email}}"" type="text"></div>
        </div>
      </div>
      <div class="frm_middle clearfix">
        <div class="input clearfix">
          <div class="label_cont"><label>Password:<span class="req-cross">*</span></label></div>
          <div class="place autocomplete_txt">
            <input name="password" type="password" autocomplete="off" id="password" title="Password is required" required></div>
        </div>
      </div>
      <div class="frm_middle clearfix">
        <div class="input clearfix">
          <div class="label_cont"><label>Select Type: </label></div> &nbsp; &nbsp;
          <select class="form control" name="usertype">
            <option value="">Select User Role</option>
            <option value="admin" @if($usr->type == 'admin') selected="selected" @endif>Admin</option>
            <option value="agent"  @if($usr->type == 'agent') selected="selected" @endif>Agent</option>
            <option value="manager" @if($usr->type == 'manager') selected="selected" @endif>Manager</option>
          </select>
        </div>
      </div>
      <input class="submit-btn" type="submit" value="submit" id="submit">
    </form>
    @endforeach
  </div>
</div>
</div>
@endsection