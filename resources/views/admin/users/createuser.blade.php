@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Admin Console</p>
@endsection
<div class="entry_form">
  <div class="container">
    <h2>Create User</h2>
    <span class="required-fields-title"><span class="req-cross">*</span> Required Fields.</span>
    @if(session()->has('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <form id="create-user" action="{{ url('admin/stor') }}" method="post">

      @csrf
      <div class="frm_middle clearfix">
        <div class="input clearfix remark">
          <div class="label_cont autocomplete_txt"><label>Username<span class="req-cross">*</span>:</label></div>
          <input type="text" name="uname" @if(old('uname')) value="{{ old('uname') }}" @endif placeholder="Username" required title="User Name is required">
        </div>
      </div>
      <div class="frm_middle clearfix">
        <div class="input clearfix">
          <div class="label_cont"><label>Password<span class="req-cross">*</span>:</label></div>
          <input name="password" type="password" required="required" placeholder="Password" title="Password is required" >
        </div>
      </div>
      <div class="frm_middle clearfix">
        <div class="input clearfix">
          <div class="label_cont"><label>Email Id<span class="req-cross">*</span>:</label></div>
          <input name="email" @if(old('email')) value="{{ old('email') }}" @endif type="email" required="required" placeholder="Email" title="Please enter valid email id">
        </div>
      </div>
      <div class="frm_middle clearfix">
        <div class="input clearfix">
          <div class="label_cont"><label>Select Type<span class="req-cross">*</span>: </label></div> &nbsp &nbsp
          <select class="form control" name="usertype">
            <option @if(old('usertype') == '') selected="selected" @endif value="">Select User Role</option>
            <option @if(old('usertype') == 'admin') selected="selected" @endif value="admin">Admin</option>
            <option @if(old('usertype') == 'manager') selected="selected" @endif value="manager">Manager</option>
            <option @if(old('usertype') == 'agent') selected="selected" @endif value="agent">Agent</option>
            
          </select>
        </div>
      </div>
     
      <input class="submit-btn" type="submit" value="submit" id="submit" >
    </form>
  </div>
</div>
</div>
@endsection