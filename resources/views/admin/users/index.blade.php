@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Admin Console - Users</p>
@endsection
@section('content')
<div class="container">
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer admin_order">
  @if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
  @endif
  <div class="breadcrumbs">
    <ul>
      <li><a class="text-link" onclick="goBack()" href="#"><i class="fas fa-arrow-left back-btn"></i></a></li>
    </ul>
  </div>
<style type="text/css">
  .pointer {cursor: pointer;}
</style>
  <div class="section-filters">
    <div class="search-form">

      <form action="{{ url('admin/usersearch') }}" method="get">
        <input type="text" name="username" placeholder="Username..." >
          <button type="submit">
            <i class="fa fa-search" aria-hidden="true"></i>
          </button>
      </form>
    </div>
    <div class="button-container">
      <a class="a-btn" href="{{ url('admin/createuser') }}">Add User</a>

    </div>
  </div>


  @if($flag==1)
  <a href="{{ url('admin/alluser') }}" class="button"><i title="reset">Reset</i></a>
  @endif


  <div class="table">
    <div class="col-sm-12">
      <table id="" class="display"  style="width:100%;">
        <thead>
          <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Email</th>
            <th>Type</th>
            <th>Actions</th>
          </tr>
          <tbody>
            @foreach($user as $usr)
            <tr>
              <td>{{ $usr->id }}</td>
              <td>{{ $usr->name }}</td>
              <td>{{ $usr->email }}</td>
              <td>{{ $usr->type }}</td>
              <td class="icons-tab">
                <a href="{{ url('admin/user/edit') }}/{{ $usr['id'] }}"><i title="Edit" class="fa fa-pencil-square-o icon-blue" aria-hidden="true"></i></a>
               

                 <a href="{{ url('admin/user/delete') }}/{{ $usr['id'] }}"> <i class="fa fa-trash icon-blue pointer" title="Delete"  aria-hidden="true" ></i></a>
              </td>
              {{-- <br> --}}
            </tr>
            @endforeach
          </tbody>
        </thead>
      </table>
      <div class="row">
        <div class="col-sm-12 col-md-5">
          <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $user->total() }} entries</div>
        </div>
        <div class="col-sm-12 col-md-7">
          <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
            {{ $user->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  @endsection
  @section('js')
  <script>
  function deletedata(id) {
    alert($id); 
    
};

$('.del').click(function(){
  alert('hiii')
});
  </script>
  @endsection