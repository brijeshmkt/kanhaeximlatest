@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Admin Console</p>
@endsection
@section('content')
	<div class="dashboard-box">
       @if(AUTH::user()->type == 'admin')
      <div class="dashboard-row-col">
				<a href="{{ url('admin/alluser') }}">
					<i class="fas fa-users"></i>
				<span>Users</span>
				{{-- <span>{{ $completed }}</span> --}}
			</a>
		</div>
		@endif
	<div class="dashboard-row-col">
				<a href="{{ url('admin/viewallclient') }}">
					<i class="fas fa-user-tie"></i>
				<span>Clients</span>
				{{-- <span>{{ $completed }}</span> --}}
			</a>
	</div>

	<div class="dashboard-row-col">
				<a href="{{ url('admin/allagents') }}">
					<i class="fas fa-user-secret"></i>
				<span>Agents</span>
				{{-- <span>{{ $completed }}</span> --}}
			</a>
		</div>
	</div>
@endsection