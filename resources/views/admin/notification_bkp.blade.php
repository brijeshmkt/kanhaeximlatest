@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Notifications</p>
@endsection
@section('content')
@if(isset($notify))
<div class="notification-container">
	<div class="user-info notificationcenter">
		<table id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer user-table">
			<tr>
				<th>Date</th>
				<th>type</th>
				<th>detail</th>
			</tr>
			@foreach($notifications as $n)
			<tr>
				<td> {{ $n->created_at->format('d/m/Y H:i') }}</td>
				<td>{{$n->type}}: </td>
				<td>{{ $n->detail }}</td>
			</tr>
			@endforeach

		</table>
		
	</div>
</div>
@endif
@endsection