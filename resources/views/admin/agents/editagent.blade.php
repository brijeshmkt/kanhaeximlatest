@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Admin Console</p>
@endsection
@section('content')
<div class="entry_form">
    <div class="container">
        <div>
            <a class="text-link" onclick="goBack()" href="#"><i class="fas fa-arrow-left back-btn"></i></a>
            <script>
            function goBack() {
                window.history.back();
            }
            </script>
        </div>
        <br>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <h3 style="font-size: 28px;font-family: inherit">Edit Agents</h3><br><br><br>
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        @foreach($agent as $agent)
        <form action="{{ url('admin/editagent/') }}/{{ $agent->id }}" method="post">
            @csrf
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Agent Name:</label></div>
                    <div class=""><input type="text"  name="agentname"  value="{{ $agent->name }}"  required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Firm Name:</label></div>
                    <div class=""><input type="text"  name="firmname" value="{{ $agent->firm_name }}" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Office Address:</label></div>
                    <div class=""><textarea name="officeaddress" required="required" id="address">{{ $agent->address }}</textarea></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>City:</label></div>
                    <div class="place autocomplete_txt"><input name="city"  value="{{ $agent->city }}" type="text" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>State:</label></div>
                    <div class="place autocomplete_txt"><input name="state"  value="{{ $agent->state }}" type="text" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Country:</label></div>
                    <div class="place autocomplete_txt"><input name="country"  value="{{ $agent->country }}" type="text" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Phone no</label></div>
                    <div class="place autocomplete_txt"><input name="phoneno" value="{{ $agent->mobile_number }}" type="number" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Office no</label></div>
                    <div class="place autocomplete_txt"><input name="officeno"  value="{{ $agent->office_phone_number }}" type="number" ></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Landline no</label></div>
                    <div class="place autocomplete_txt"><input name="landlineno"  value="{{ $agent->land_line_phone_number }}" type="number" ></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Email:</label></div>
                    <div class="place autocomplete_txt"><input name="email" value="{{ $agent->email }}" type="Email" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>GST No:</label></div>
                    <div class="place autocomplete_txt"><input name="gstno" value="{{ $agent->gst_number }}"  type="text" required="required"></div>
                </div>
            </div>
            <input type="hidden" value="agent" name="type">
            <input class="submit-btn" type="submit" value="submit" id="submit">
        </form>
        @endforeach
    </div>
</div>
</div>
@endsection