@extends('admin.layouts.layouts')
@section('content')
<div class="entry_form">
    <div class="container">
        <div>
            <button onclick="goBack()"><i class="fas fa-arrow-left back-btn"></i></button><br><br>
            <script>
            function goBack() {
            window.history.back();
            }
            </script>
        </div>
        <br>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <h3 style="font-size: 28px;font-family: inherit">Add Agents</h3><br><br><br>
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        <form action="{{ url('admin/insertagent/') }}" method="post" id="myform">
            @csrf
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Agent Name:<span class="req-cross">*</span></label></div>
                    <div class=""><input type="text"  name="agentname" @if(old('agentname')) value="{{ old('agentname') }}" @endif  required="required" id="agentname" title="Agent Name is required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Firm Name:<span class="req-cross">*</span></label></div>
                    <div class=""><input type="text"  name="firmname" @if(old('firmname')) value="{{ old('firmname') }}" @endif required="required" id="fname" title="Firm name is required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Office Address<span class="req-cross">*</span>:</label></div>
                    <div class="">
                        <textarea name="officeaddress" required="required" id="ofcaddress" title="Office Address is required">@if(old('officeaddress')) {{ old('officeaddress') }}" @endif </textarea></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>City<span class="req-cross">*</span>:</label></div>
                    <div class="place autocomplete_txt">
                        <input type="text" name="city" @if(old('city')) value="{{ old('city') }}" @endif  required="required" id="cname" title="Agent City name is required">
                    </div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>State<span class="req-cross">*</span>:</label></div>
                    <div class="place autocomplete_txt">
                        <input type="text" name="state" @if(old('state')) value="{{ old('state') }}" @endif   required="required" id="astate" title="State is required">
                    </div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Country<span class="req-cross">*</span>:</label></div>
                    <div class="place autocomplete_txt"><input name="country" @if(old('country')) value="{{ old('country') }}" @endif type="text" required="required" id="acountry" title="Country name is required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Phone no<span class="req-cross">*</span>:</label></div>
                    <div class="place autocomplete_txt"><input name="phoneno" @if(old('phoneno')) value="{{ old('phoneno') }}" @endif type="number" required="required" id="aphoneno" title="Agent phone number is required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Office no:</label></div>
                    <div class="place autocomplete_txt"><input name="officeno" @if(old('officeno')) value="{{ old('officeno') }}" @endif  type="number" ></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Landline no:</label></div>
                    <div class="place autocomplete_txt">
                        <input type="number" name="landlineno" @if(old('landlineno')) value="{{ old('landlineno') }}" @endif >
                    </div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Email<span class="req-cross">*</span>:</label></div>
                    <div class="place autocomplete_txt">
                        <input type="email" name="email" @if(old('email')) value="{{ old('email') }}" @endif  required="required" id="aemail" title="Email is required">
                    </div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>GST No:<span class="req-cross">*</span></label></div>
                    <div class="place autocomplete_txt">
                        <input type="text" name="gstno" @if(old('gstno')) value="{{ old('gstno') }}" @endif  required="required" id="agstno" title="Agent GST number is required">
                    </div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Password:<span class="req-cross">*</span></label></div>
                    <div class="place autocomplete_txt">
                        <input name="password" type="password" required="required" id="pass" title="Password is required">
                    </div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Confirm Password<span class="req-cross">*</span>:</label></div>
                    <div class="place autocomplete_txt">
                        <input name="password_confirmation" type="password" required="required" id="cpass" title="Please confirm your password">
                    </div>
                </div>
            </div>
            <input type="hidden" value="agent" name="type">
            <input class="submit-btn" type="submit" value="submit" id="submit">
        </form>
    </div>
</div>
</div>
@endsection