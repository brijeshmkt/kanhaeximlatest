@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Admin Console - Agents</p>
@endsection
@section('content')
<div class="container">
	<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
		@if(session()->has('message'))
		<div class="alert alert-success">
			{{ session()->get('message') }}
		</div>
		@endif
		<div class="breadcrumbs">
			<ul>
				<li><a class="text-link" href="{{url()->previous()}}"><i class="fas fa-arrow-left back-btn"></i></a></li>
			</ul>
		</div>
		<div class="section-filters">
			<div class="search-form">
				<form method="get" action="{{ url('admin/agentsearch') }}">
					<input type="text" name="aid" placeholder="Agent Id">
					<input type="text" name="gstno" placeholder="Agent GstNo.">
					<input type="text" name="phoneno" placeholder="Phone No.">

					<button type="submit">
					<i class="fa fa-search" aria-hidden="true"></i>
					</button>
					<a href="{{ url('admin/allagents') }}">Reset</a>

				</form>

			</div>
			<div class="button-container">
				<a class="a-btn" href="{{ url('admin/newagent') }}">Add</a>
			</div>
		</div>

		<div class="row table">
			<div class="col-sm-12">
				@if(isset($search))
				<table id="datatable" class="display"  style="width:100%;">
					<thead>
						<tr>
							<th>Agent Id</th>
							<th>Agent Name</th>
							<th>Firm Name</th>
							<th>Firm Address</th>
							<th>State</th>
							<th>Phone No</th>
							<th>GST No</th>
							<th>Action</th>
						</tr>
						<tbody>
							@foreach($search as $agent)
							<tr>
								<td>{{ $agent->id }}</td>
								<td>{{ $agent->name }}</td>
								<td>{{ $agent->firm_name }}</td>
								<td>{{ $agent->address }}</td>
								<td>{{ $agent->state }}</td>
								<td>{{ $agent->mobile_number }}</td>
								<td>{{ $agent->gst_number }}</td>
								<td><a href="{{ url('admin/agent/edit') }}/{{ $agent['id'] }}"><i title="Edit" class="fa fa-pencil-square-o icon-blue" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('admin/agent/delete') }}/{{ $agent['id'] }}"><i class="fa fa-trash icon-blue" title="Delete" aria-hidden="true"></i></a></td>
							</tr>
							@endforeach
						</tbody>
					</thead>
				</table>
				@else
				<table id="datatable" class="display"  style="width:100%;">
					<thead>
						<tr>
							<th>Agent Id</th>
							<th>Agent Name</th>
							<th>Firm Name</th>
							<th>Firm Address</th>
							<th>State</th>
							<th>Phone No</th>
							<th>GST No</th>
							<th>Action</th>
						</tr>
						<tbody>
							@foreach($agents as $agent)
							<tr>
								<td>{{ $agent->id }}</td>
								<td>{{ $agent->name }}</td>
								<td>{{ $agent->firm_name }}</td>
								<td>{{ $agent->address }}</td>
								<td>{{ $agent->state }}</td>
								<td>{{ $agent->mobile_number }}</td>
								<td>{{ $agent->gst_number }}</td>
								<td><a href="{{ url('admin/agent/edit') }}/{{ $agent['id'] }}"><i title="Edit" class="fa fa-pencil-square-o icon-blue" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('admin/agent/delete') }}/{{ $agent['id'] }}"><i class="fa fa-trash icon-blue" title="Delete" aria-hidden="true"></i></a></td>
							</tr>
							@endforeach
						</tbody>
					</thead>
				</table>
				@endif
				<div class="row">
					<div class="col-sm-12 col-md-5">
						<div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total @if(isset($search))
						{{ $search->total() }} @else {{ $agents->total() }} @endif entries</div>
					</div>
					<div class="col-sm-12 col-md-7">
						<div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
							@if(isset($search)){{ $search->links() }} @else {{ $agents->links() }} @endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection