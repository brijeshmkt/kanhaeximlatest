@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Dash Board</p>
@endsection
@section('content')
    <div class="dashboard-box">

      <div class="dashboard-row-col">
        <a href="{{ route('admin/console') }}">
          <i class="fas fa-user-cog"></i><span>Admin Console</span>
        </a>
      </div>

        <div class="dashboard-row-col">
        <a href="{{ url('admin/tradename') }}">
          <i class="fas fa-dice-d6"></i><span>Trade Name</span>
          <!-- <span class="badge badge-success a-animate-blink noti-icon-badge"> -->
        </a>
      </div>
      <div class="dashboard-row-col">
        <a href="{{ url('admin/designs') }}">
          <i class="far fa-object-group"></i><span>Designs</span>
        </a>
      </div>
      <div class="dashboard-row-col">
        <a href="{{ url('admin/millreport') }}">
          <i class="fas fa-industry"></i><span>Mill Reports</span>
        </a>
      </div>
      <div class="dashboard-row-col">
        <a href="{{ url('admin/inventory') }}">
          <i class="fas fa-warehouse"></i><span>Inventory</span>
        </a>
      </div>

      <div class="dashboard-row-col">
        <a href="{{ url('admin/order') }}">
          <i class="fas fa-cart-plus"></i><span>Orders</span>
        </a>
      </div>

      <div class="dashboard-row-col">
        <a href="{{ url('admin/viewallclient') }}">
          <i class="fas fa-plus-square"></i><span>Create Order</span>
        </a>
      </div>

       <div class="dashboard-row-col">
        <a href="{{ url('admin/order/packaging') }}">
          <i class="fas fa-cubes"></i><span>Packaging</span>
        </a>
      </div>
   
      <div class="dashboard-row-col">
        <a href="{{ url('admin/notifications') }}">
          <i class="fas fa-bell"></i><span>count over here<br> Notifications</span>
          <!-- <span class="badge badge-success a-animate-blink noti-icon-badge"> -->
        </a>
      </div>

      

  </div>

@endsection