@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Add Inventory</p>
@endsection
<br>
<div class="container">
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="user-info">
  <ul>
    <li><label>Mill id:</label> <span>{{ $millreport->id }}</span></li>
    <li><label>Trade Name:</label> <span> {{ $millreport->tradenames->name }}</span></li>
    <li><label>Lot Numbers:</label> <span>{{ $millreport->lot_numbers }}</span></li>
  	<li><label>Yarn Quality:</label> <span>{{ $millreport->yarn_quality }}</span></li>
  	<li><label>Remaining Meters:</label> <span>{{ $millreport->orignal_meters - $millreport->used_meters }}</span></li>    
  	<li><label>Total Meters:</label> <span>{{ $millreport->orignal_meters }}</span></li>
  </ul>
</div>
                                          
<form id="myForm" class="mb-0" method="post" action="/admin/inventory/store">
  @csrf

  <input type="hidden" name="millreport_id" value="{{ $millreport->id }}">
  <input type="hidden" name="tradename_id" value="{{ $millreport->tradename_id }}">
	<div class="form-group bmd-form-group">
	  <label>Select Design</label>
	  <select name="design_id" required>
	  	<option value="">Select Design</option>
      @foreach($designs as $design)
        <option value="{{ $design->id }}">{{ $design->number }}</option>
      @endforeach
	  </select>
	</div>

	<div class="form-group bmd-form-group">
		<label>Enter Color:</label>
    <input type="text" class="input-color" id="input-color" name="color" placeholder="Bloody Red" value="" required>
  </div>
  <div class="form-group bmd-form-group">
    <label>Enter Color Initial:</label>
    <input type="text" class="input-color" id="input-color" name="color_initial" placeholder="BR" value="" required>
  </div>
    <a id="add-taaka" href="#input-taaka">Add Taakaa's in mtrs For This Color</a>
  <style type="text/css">
    .input-container {
      display: flex;
      width: 100%;
      margin-bottom: 15px;
    }
    .icon {
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      margin-top:25px;
      text-align: center;
      margin-right: 20px;
    }
  </style>  
    <div id="input-taaka" >
          <div class="input-container">
            <i class="icon">1</i>
            <input type="text" class="input-taaka"  name="quantity[]" required class="len1">
          </div>      
    </div>
		
	
	
	<input type="submit" name="submit" value="Submit">


</form>  
</div>
@endsection

@section('js')
<script type="text/javascript">



document.getElementById("myForm").onkeypress = function(e) {
  var key = e.charCode || e.keyCode || 0;     
  if (key == 13) {
    // alert("I told you not to, why did you do it?");
    e.preventDefault();

    var i;
      for (i = 0; i < 10; i++) { 
        $("#input-taaka").append('<input type="text" class="input-taaka" name="quantity[]" class="len" >');
      }

  }
}

  
  $("#add-taaka").click(function() {
    var i;
    
      var test=$('.input-container').length;
       // alert(test);
       if(test==1)
       {
        no=10;
       }else{
        no=11;
       }

    for (i = 1; i < no; i++) { 
     
        t=test+i;

     

      $("#input-taaka").append('<div class="input-container"> <i class="icon">'+t+'</i><input type="text" class="input-taaka" name="quantity[]" class="len1"></div>');

    }

     
  });
</script>
  
@endsection

