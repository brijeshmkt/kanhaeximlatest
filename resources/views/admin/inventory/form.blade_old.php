@extends('admin.layouts.layouts')
@section('content')
  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<form class="mb-0" method="post" action="{{ route('inventory.store') }}" >
  @method('POST')
  @csrf
    <div class="form-group bmd-form-group">
    <label for="exampleInputEmail1" class="bmd-label-floating ">Design No</label>
    <input id="design_no" name="des_number">
    </div>
    <div class="form-group bmd-form-group">
    <label for="exampleInputEmail1" class="bmd-label-floating ">Trade Name</label>
    <input id="design_no" name="trade_name">
    <input type="hidden" id="rowproduct" name="rowproduct">
  </div>

  <div class="form-group bmd-form-group">
    <label for="exampleTextarea" class="bmd-label-floating">Size</label>
    <label for="exampleTextarea" class="bmd-label-floating">Quantity</label>
  </div>
  <div id="p_scents">
    <table id="inputvalue">
      <tr>
        <td>1</td>
        <td><input type="number" value="" placeholder="Size" min="0" step="0.01" name="size[]" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control"></td>
        <td>1</td>
        <td><input type="number" value="" placeholder="Quantity" min="0" step="0.01" name="quantity[]" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control"></td>


      </tr>
    </table>

  </div>
  <a href="#" id="addScnt" class="btn btn-primary btn-raised mb-0">Add Another Input Box</a>
  <button type="submit" name="submit" class="btn btn-primary btn-raised mb-0">Submit</button>
  <a class="btn btn-raised btn-danger mb-0" href="{{ url('/admin/inventory') }}">Cancel</a>
</form>
@endsection
@section('js')
<script>
 $( function() {
   $( "#design_no" ).autocomplete({
     source: "{{ url('admin/getallrowproduct') }}",
     minLength: 2,
     select: function( event, ui ) {
       $('#design_no').val(ui.item.label);
       $('#rowproduct').val(ui.item.id);
     }
   });

   var scntDiv = $('#p_scents');
    var i = $('#p_scents input').length + 1;
    $('#addScnt').on('click', function() {
            $('<tr id="f-'+i+'"><td>'+i+'</td><td><input type="number" value="" placeholder="Size" min="0" step="0.01" name="size[]" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control"><td>'+i+'</td><td><input type="number" value="" placeholder="Quantity" min="0" step="0.01" name="quantity[]" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control"></td><td><a id="remove" onclick="remove('+i+')">Remove</a></td></tr>').appendTo('#inputvalue');
            i++;
            return false;
    });
 } );
function remove(id) {
    $('#f-'+id).remove();
    i--;
}


 // document.getElementsByClassName('remove').val()
</script>
@endsection
