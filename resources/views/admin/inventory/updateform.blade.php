@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Inventory - Edit Inventory</p>
@endsection
@section('content')
<div class="container">
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif
<div class="user-info">
@foreach($inventory as $in)
@if($loop->first)
<div>
<label>Date:</label> <span>@php echo date_format($in->created_at,"d/m/y H:i:s");  @endphp </span>
</div>
<div>
<label>TradeName:</label> <span>{{$in->tradenames->tradename}}</span>
</div>
<div>
<label>Lot No:</label><span>@if($in->millreport['lot_numbers'] !== null)@php echo implode(" , ",json_decode($in->millreport['lot_numbers'])); @endphp @endif</span>
</div>
<div>
<label>Quality:</label></b><span>{{$in->millreport['yarn_quality']}}</span>
</div>
<div>
<label>Total Meters:</label></b><span>{{ $in->millreport['remaining_meters'] }}</span>
</div>
@endif
@endforeach
</div>
@if(isset($in))
<form class="mb-0" method="post"  action="@if(AUTH::user()->type == 'admin'){{ url('admin/milreport/inventory/update') }}/{{$in->millreport_id}} @else{{ url('manager/milreport/inventory/update') }}/{{$in->millreport_id}}@endif" enctype="multipart/form-data">
  @method('POST')
  @csrf

  <div class="form-group bmd-form-group">
    <label for="exampleInputEmail1" class="bmd-label-floating ">Design No</label>
    <input type="number" id="design_no"  value="{{$in->designs->number}}"  name="des_number" required="required">
  </div>
  <input type="hidden" value="{{$in->trade_name}}"  name="tradename"  />
  <input type="hidden" value="{{$in->id}}"  name="orignalid"  />

  <table width="100%" id="table">
    <thead>
      <tr>
        <th class="heading">Colour</th>
        <th class="heading">Quantity(Meters)</th>
        <th class="heading"></th>
      </tr>
    </thead>
    <tbody id="table_input">
      @if($inventory)
      @foreach($inventory as $key=>$value)
          <input type="hidden"  name="rendom">
      <tr id="row-{{$key}}">
        <td>
          <input type="text"  name="colour[{{$key}}]"  value="{{ $value->color }}" placeholder="Colour" id="colour" required="required">
          <input type="hidden"  name="mainid[{{$key}}]"  value="{{ $value->id }}" />
        </td>
        <td>
          <input type="text"  name="quantity[{{$key}}]" value="{{ $value->quantity }}" placeholder="Quantity" id="quantity" required="required">
          <input type="hidden"  name="mainid[{{$key}}]"  value="{{ $value->id }}" />
        </td>
        <td>
          <i class="addButton fa fa-trash" id="removeButton" onclick="myFunction({{$value->id}})"></i>
        </td>
      </tr>
      @endforeach
      @endif
    </tbody>
    <tfoot>
    <tr">
      <td><i class="addButton far fa-plus-square"   data-listid = {{count($inventory)-1}}  id="addButton" value="Add"></td>
    </tr>
    </tfoot>
  </table>
  <br><br>
  <button type="submit" name="submit" class="a-btn">Submit</button>
  <a class="a-btn" href="{{ url('admin/millreport') }}">Cancel</a>
</form>
</div>
  @endif

@endsection
@section('js')
<script>
  function myFunction(id){
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to recover this Colour !",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    {{-- var id = "@if(isset($in)){{ $value->sr_no }}@endif"; --}}
    console.log(id);
    var tr = $('#table_input tr').length
              if(@if(isset($in)) tr > 1 @endif){
                $.ajax({
                    type: "POST",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                     url: "{{ url('/admin/inventory/colour/delete') }}/" + id,
                    })

    swal("Alert! Your Colour has been deleted!", {
    icon: "success",

    }).then(()=>{
      location.reload(true);
    });
      }
      else if(@if(isset($in)) tr > 0 @endif){
      swal("Alert! Inventory can not be empty");
      }
  } else {
    swal("Your Colour file is safe!");
  }
});
};
$("#addButton").click(function () {
var lastId = $(this).data('listid');
var lasrcolour = $("#row-"+lastId+" #colour").val();
lastId++;
var html = '<tr id="row-'+lastId+'" ><td><input type="text" class="size" name ="colour['+ lastId +']" id="colour" value="'+lasrcolour+'" placeholder="Colour"><input type="hidden"  name="mainid[' + lastId + ']"  value="'+lasrcolour+'" /></td><td><input type="text" class="quantity" name="quantity[' + lastId + ']" id="quantity" placeholder="Quantity"><input type="hidden"  name="mainid[' + lastId + ']"  value="null" /></td><td><i  class="addButton fa fa-trash newtrash" id="removeButton" id="" ></i></td></tr>';
$(this).data('listid', lastId);
$('#table_input').append(html);
});
$("body").on("click",".newtrash",function(){
$(this).parent().parent().remove();
});
</script>

@endsection