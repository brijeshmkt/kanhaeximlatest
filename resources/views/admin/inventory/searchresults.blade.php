@extends('admin.layouts.layouts')
@section('content')

@if($values == '[]')
<h1>No results found!!!</h1>
@endif
@if($values!='[]')
	<h4>Searched with</h4><h5>{{ $str }}</h5>
	@foreach($values as $value)
	<label>Design Number</label><span>{{ $value['DesignNo'] }}</span>
	<label>Trade Name</label><span>{{ $value['TradeName'] }}</span>
	<label>Size</label><span>{{ $value['Size'] }}</span>
	<label>Quantity</label><span>{{ $value['Quantity'] }}</span>
	@endforeach
@endif
@endsection
