@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Add Inventory</p>
@endsection
<br>
<div class="container">
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="user-info">
  <ul>
    @foreach($update as $inventory)
    <li>
      <label>Date :&nbsp;</label>
      <span>{{ $inventory->created_at->format('d/m/Y H:i:s') }}</span>
    </li>
    <li>
      <label>Trade Name :&nbsp;</label> <span>{{ $inventory->tradenames->tradename }}</span>
    </li>
    <li>
      <label>Lot No :&nbsp;</label> <span>@php echo implode(" , ",json_decode($inventory->lot_numbers)); @endphp</span>
    </li>
    <li>
      <label>Quality :&nbsp;</label> <span>{{ $inventory->yarn_quality }}</span>
    </li>
    <li>
      <label>Total Meters :&nbsp;</label> <span>{{ $inventory->remaining_meters }}</span>
    </li>
    @endforeach
  </ul>
</div>

<form class="mb-0" method="post" action="@if(AUTH::user()->type == 'admin'){{ url('admin/milreport/inventory/add') }}/{{ $inventory->id }} @else {{ url('manager/milreport/inventory/add') }}/{{ $inventory->id }} @endif" enctype="multipart/form-data" id="add-inventory">
  @method('POST')
  @csrf
  <div class="form-group bmd-form-group">
    <label for="exampleInputEmail1" class="bmd-label-floating ">Design No<span class="req-cross">*</span></label>
    <input type="number" required="required" @if(old('des_number')) value="{{old('des_number')}}" @endif name="des_number" id="design_no">
    <img src=""  alt="Image" height="auto" width="150" id="design_image" />
    @foreach($update as $inventory)
    <input type="hidden" value="{{ $inventory->tradenames->tradename }}" name="tradename" />
    <input type="hidden" value="{{ $inventory->tradenames->id }}" name="tradenameid" />
    <input type="hidden" value="{{ $inventory->total_meters }}" name="milltotalmeter" />
    <input type="hidden" value="{{ $inventory->lotno }}" name="lot" />
    <input type="hidden" value="{{ $inventory->yarn_quality }}" name="quality" />
    <input type="hidden" value="{{ $inventory->width }}" name="width" />
    <input type="hidden" value="{{ $inventory->weight }}" name="weight" />
    <input type="hidden" value="{{ $inventory->number_of_pieces }}" name="pieceno" />
    @endforeach
  </div>
  {{-- <div class="form-group bmd-form-group">
    <label for="image">Image<span class="req-cross">*</span></label>
    <input type="file" required="required" accept="image/*" name="image" id="inventory-image" title="Image is required">
  </div> --}}
  <table width="100%" id="table">
    <thead>
      <tr>
        <th class="heading">Colour<span class="req-cross">*</span></th>
        <th class="heading">Quantity(Meters)<span class="req-cross">*</span></th>
        <th class="heading"></th>
      </tr>
    </thead>
    <tbody id="table_input">
      @if(old('colour'))
      @foreach(old('colour') as $key=>$value)
      <tr id="{{$key}}">
        <td>
          <input type="text" @if(old('colour')[$key]) name="colour[{{$key}}]"  @endif value="{{ $value }}" placeholder="Colour" id="colour" required="required" title="Colour is required">
        </td>
        <td>
          <input type="number" @if(old('quantity')[$key]) name="quantity[{{$key}}]" value="{{ old('quantity')[$key] }}" @endif placeholder="Quantity" id="quantity" required="required" title="Colour is required">
        </td>
        <td>
          <!-- <input type="button" class="submit-btn" id="removeButton" value="remove"> -->
                            <i class="fa fa-trash" aria-hidden="true" id="removeButton"></i>

        </td>
      </tr>
      @endforeach
      @else
      <tr id="row-1" data-listid = 1>
        <td>
          <input type="text" value="" name="colour[1]" placeholder="Colour" id="colour" required="required" title="Colour is required">
        </td>
        <td>
          <input type="number" name="quantity[1]" value=""  placeholder="Quantity" id="quantity" required="required" title="Quantity is required">
        </td>
        {{-- <td>
          <!-- <input type="button" class="submit-btn" id="removeButton" value="remove"> -->
                   <i class="fa fa-trash" aria-hidden="true" id="removeButton"></i>

        </td> --}}
      </tr>
      @endif
    </tbody>
    <tfoot>
    <tr>
      @if(old('colour'))
      <td><i class="fa fa-plus-square" aria-hidden="true" data-listid ="{{ count(old()['colour']) }}" id="addButton"></i></td>
      @else
      <td><i class="fa fa-plus-square" aria-hidden="true" data-listid = 1 id="addButton"></i></td>
      @endif
    </tr>
    </tfoot>
  </table>
  <br><br>
  <div class="mill-btns">
    <button type="submit" name="submit" class="submit-btn">Submit</button>
    <a class="submit-btn" href="{{ url('admin/millreport') }}">Cancel</a>
  </div>
</form>
</div>
@endsection
@section('js')
<script>
$(document).keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == 13){
      event.preventDefault();
        var lastId = $("#addButton").data('listid');
        var lasrcolour = $("#row-" + lastId + " #colour").val();
        var quantity = $("#row-" + lastId + " #quantity").val();
        lastId++;
        var html = '<tr id="row-' + lastId + '" ><td><input type="text" required="required" class="size" name ="colour[' + lastId + ']" id="colour" value="' + lasrcolour + '" placeholder="Colour" title="Colour is required" required="required"></td><td><input type="number" value="' + quantity + '"  class="quantity" name="quantity[' + lastId + ']" id="quantity" placeholder="Quantity" title="Quantity is required" required="required"></td><td><i class="fa fa-trash" aria-hidden="true" id="removeButton"></i></td></tr>';
        $("#addButton").data('listid', lastId);
        $('#table_input').append(html);
    }
});
$("#addButton").click(function() {
    var lastId = $(this).data('listid');
    var lasrcolour = $("#row-" + lastId + " #colour").val();
    lastId++;
    var html = '<tr id="row-' + lastId + '" ><td><input type="text" required="required" class="size" name ="colour[' + lastId + ']" id="colour" value="' + lasrcolour + '" placeholder="Colour" title="Colour is required" required="required"></td><td><input type="number" required="required" class="quantity" name="quantity[' + lastId + ']" id="quantity" placeholder="Quantity" title="Quantity is required" required="required"></td><td><i class="fa fa-trash" aria-hidden="true" id="removeButton"></i></td></tr>';
    $(this).data('listid', lastId);
    $('#table_input').append(html);
});
$("body").on("click", "#removeButton", function() {
    $(this).parent().parent().remove();
});
$("#design_image").hide()
$("#design_no").autocomplete({
            type: "GET",
            dataType: "JSON",
            source: "{{ url('admin/finddesignno') }}",
            select: function(event, ui) {

                var name = ui.item.value
                // var path = document.location.pathname;
                var path = "/"+ui.item.image
                $("#design_image").show()
                $("#design_image").attr("src",path);
            }
        });

</script>
@endsection