@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Inventory</p>
@endsection
@section('content')
<div class="card-body inventory-index">
  <div class="container">
  <div class="general-label">
    <a class="text-link" href="{{ url()->previous() }}"><i class="fas fa-arrow-left back-btn"></i></a>
    <!-- Search form -->
    <h4 class="mt-0 header-title">Search</h4><br><br>
    <form action="@if(AUTH::user()->type == 'admin'){{ url('admin/inventorysearch') }} @else {{ url('manager/inventorysearch') }} @endif" method="get">
      <input type="text" name="trade" placeholder="TradeName..." >
      <input type="text" name="design" placeholder="Design Number...">
      <input type="text" name="qauantity" placeholder="Quantity">

        <button type="submit" name="submit" class="a-btn search">Search</button>
        <a href="{{ url('admin/inventory') }}" class="a-btn">Reset</a>    </form>
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>

  <div class="card-body">
    <h4 class="mt-0 header-title">Inventory</h4>
    <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="datatable_length"></div></div></div>
    <div class="row table"><div class="col-sm-12">
      <table id="datatable" class="display" style="width:100%">
        <thead>
          <tr>
            <th>Mr Id</th>
            <th>Design No</th>
            <th>Trade name</th>
            <th>Colour</th>
            <th>Quantity</th>
            <th>No. Of Pisces</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {{-- {{dd($inventories)}} --}}
          @foreach($inventories as $inventory)
          <tr>
            <td><a href="{{ url('admin/millreport/show') }}/{{ $inventory->millreport_id }}">{{ $inventory->millreport_id }}</a></td>
            <td>{{ $inventory->designs['number'] }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i title="Delete" class="fa fa-trash icon-red" onclick="deletedata({{ $inventory->design_id }})" aria-hidden="true"></i></td>
            <td>{{ $inventory->tradenames->tradename }}</td>
            <td>{{ $inventory->color }}</td>
            <td>{{ $inventory->quantity }}</td>
            <td>{{$inventory->no_of_piece}}</td>

            <td>@if(AUTH::user()->type == 'admin')
              <a href="{{ url('admin/inventory/edit') }}/{{$inventory->millreport_id}}">@else
                <a href="{{ url('manager/inventory/edit') }}/{{$inventory->millreport_id}}">@endif <i title="Edit" class="fa fa-pencil-square-o icon-blue" aria-hidden="true"></i></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                @if(AUTH::user()->type == 'admin')
                <a href="{{ url('admin/inventory/edit') }}/{{$inventory->millreport_id}}">
                  @else
                  <a href="{{ url('manager/inventory/edit') }}/{{$inventory->millreport_id}}">
                    @endif
                  <i title="Delete" class="fa fa-trash icon-red" aria-hidden="true"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="@if(AUTH::user()->type == 'admin'){{ url('admin/inventory/barcode') }}/{{$inventory->design_id}}
                  @else {{ url('manager/inventory/barcode') }}/{{$inventory->design_id}} @endif">Print Barcode</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  {{$inventories->links()}}
  </div>
</div>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script>
function deletedata(id) {
    swal({
            title: "Are you sure to delete ?",
            text: "Once deleted, you will not be able to recover this Inventory !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var tr = $('#table_input tr').length
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "@if(isset($inventory)) @if(AUTH::user()->type == 'admin') {{ url('admin/inventory/destroy') }}/{{$inventory->design_id}} @else  {{ url('manager/inventory/destroy') }}/{{$inventory->design_id}}  @endif @endif",
                })
                swal("Alert! Your Inventory has been deleted!", {
                    icon: "success",
                }).then(() => {
                    location.reload(true);
                });
            } else {
                swal("Your Inventory is safe!");
            }
        });
};
</script>
@endsection