@extends('admin.layouts.layouts')
@section('content')
<div class="container">
<div class="card-body clearfix">
	<h4 class="mt-0 header-title">Select Colour For Barcode</h4>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@endif
	@foreach($barcode as $row)
	<form method="post" action="@if(AUTH::user()->type == 'admin'){{ url('admin/inventory/barcode/generate') }} @else {{ url('manager/inventory/barcode/generate') }} @endif">
		@csrf
		@endforeach
		<input type="submit" class="a-btn pull-right" value="Print For Barcode"/>
		<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
			<div class="row table">
				<div class="col-sm-12">
					<table id="datatable" class="display" style="width:100%">
						<thead>
							<tr>
								<th>Id</th>
								<th>Design No</th>
								<th>Quantity</th>
								<th>Trade Name</th>
								<th>Total Meter</th>
								<th>Colour</th>
								<th><span style="display: inline-block; vertical-align: -webkit-baseline-middle; margin-top: 12px;}">Action</span></th>
								<th><button type="button" class="a-btn pull-right" onclick="checkAll()" value="Check All" style="margin-left: -105px;">Check All</button></th>
							</tr>
							<tbody>
								{{-- {{dd($barcode)}} --}}
								@foreach($barcode as $row)
								<tr>
									{{-- {{dd($row["millreport"]->orignal_meters)}} --}}
									{{-- {{dd($row["millreport"]->orignal_meters)}} --}}
									<td>{{ $row->id }}</td>
									<td>{{ $row->designs->number }}</td>
									<td>{{ $row->quantity }}</td>
									<td>{{ $row->trade_name }}</td>
									@if($row->total_meters != null)
									<td>{{ $row->total_meters }}</td>
									@else
									<td>0.00</td>
									@endif
									@if( !$row["millreport"])
									<td>0.00</td>
									@else
									<td>{{ $row["millreport"]->orignal_meters }}</td>
									@endif
									<td>{{ $row->color }}</td>
									<td><input type="checkbox" id="checkbox" name="barcode[]" value="{{ $row->barcodes->id }}" ></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</form>
			<div class="row">
				<div class="col-sm-12 col-md-5">
					<div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $barcode->total() }} entries</div>
				</div>
				<div class="col-sm-12 col-md-7">
					<div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
						{{ $barcode->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script>
function checkAll() {
    if ($('input:checked').prop("checked") == true) {
        $(":checkbox").attr("checked", false);
    } else {
        $(":checkbox").attr("checked", true);
    }
}
</script>
@endsection
