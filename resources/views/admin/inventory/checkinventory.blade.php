@extends('admin.layouts.layouts')
@section('content')
<center><h3>Inventory Not Available!</h3></center>
<center><h4>Place As Requested order?</h4></center>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{url('admin/orderform')}}/{{$request->clientid}}" method="post">
	@csrf
	<br>
	<input type="submit" class="a-btn" name="submit" value="Yes">
	<a href="{{url('admin/viewallclient')}}"><input type="button" class="a-btn" value="No" /></a>
  <input type="hidden" name="clientid" value="{{ $request->id }}">
            <input type="hidden" name="clientname" value="{{ $request->clientname }}">
            <input type="hidden" name="firmname" value="{{ $request->firmname }}">
            <input type="hidden" name="firmaddress" value="{{ $request->firmaddress }}">
            <input type="hidden" name="phoneno" value="{{ $request->phoneno }}">
            <input type="hidden" name="gstnumber" value="{{ $request->gstnumber }}">
            <input type="hidden" name="date" value="{{ $request->date }}">
            <input type="hidden" name="agent" value="{{ $request->agent }}">
            <input type="hidden" name="station" value="{{ $request->station }}">
            <input type="hidden" name="tp" value="{{ $request->tp }}">
            @foreach($request->trade_name as $value)
			 <input type="hidden" name="trade_name[]" value="{{ $value}}">
			@endforeach
            {{-- <input type="hidden" name="trade_name" value="{{ $request->trade_name }}"> --}}
            @foreach($request->rate as $value)
			 <input type="hidden" name="rate[]" value="{{ $value}}">
			@endforeach
            {{-- <input type="hidden" name="rate" value="{{ $request->rate }}"> --}}
            @foreach($request->design_no as $value)
			 <input type="hidden" name="design_no[]" value="{{ $value}}">
			@endforeach
            {{-- <input type="hidden" name="design_no" value="{{ $request->design_no }}"> --}}
             <input type="hidden" name="payment" value="{{ $request->payment }}">
            <input type="hidden" name="terms" value="{{ $request->terms }}">
            @foreach($request->quantity as $value)
			 <input type="hidden" name="quantity[]" value="{{ $value}}">
			@endforeach
            {{-- <input type="hidden" name="quantity" value="{{ $request->quantity }}"> --}}
</form>

@endsection