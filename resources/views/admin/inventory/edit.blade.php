@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Edit Inventory</p>
@endsection
<br>
<div class="container">
  @if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
  @endif
  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <a href="{{ URL::previous() }}">Back</a>
  <div class="user-info">
    <ul>
      <li><label>Mill id:</label> <span>{{ $mill->id }}</span></li>

      <li><label>Lot Numbers:</label> <span>{{ $mill->lot_numbers }}</span></li>

      <li><label>Remaining Meters:</label> <span>{{ $mill->orignal_meters - $mill->used_meters }}</span></li>
      <li><label>Total Meters:</label> <span>{{ $mill->orignal_meters }}</span></li>
    </ul>
  </div>
  <form id="myForm" class="mb-0" method="post" action="/admin/inventory/update">
    @csrf

    @foreach ($inventories as $inventory)

    <div class="form-group">
      <label>{{ $inventory->id }}</label>
      <input type="text" name="inventory[{{ $inventory->id }}]" value="{{ $inventory->quantity }}">
    </div>
    @endforeach
    <input type="submit" class="a-btn"name="submit" value="Submit">
  </form>
</div>
@endsection