@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Inventory</p>
@endsection
@section('content')
<div class="card-body inventory-index">
  <div class="container">
  <div class="general-label">
    <a class="text-link" href="{{ url()->previous() }}"><i class="fas fa-arrow-left back-btn"></i></a>
    <!-- Search form -->
    
    
    
  <div class="card-body">
    <h4 class="mt-0 header-title">Inventory</h4>
    <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="datatable_length"></div></div></div>
    <div class="row table"><div class="col-sm-12">
      <table id="datatable" class="display" style="width:100%">
        <thead>
          <tr>
            <th>Mill Id</th>
            <th>Trade name</th>
            <th>Design No</th>
            <th>Color</th>
            <th>Count</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          
          @foreach ($inventories as $inventory)
          <tr>
              <td>{{ $inventory->millreport_id}}</td>
              <td>{{ $inventory->name}}</td>
              <td>{{ $inventory->number}}</td>
              <td>{{ $inventory->color}}</td>
              <td>{{ $inventory->total}}</td>
              <td>
                <a href="/admin/inventory/edit/{{ $inventory->millreport_id}}/{{ $inventory->tradename_id }}/{{ $inventory->design_id }}/{{ $inventory->color}}">
                Edit | </a> 

                <a target="_blank" href="/admin/inventory/printbarcode/{{ $inventory->millreport_id}}/{{ $inventory->tradename_id }}/{{ $inventory->design_id }}/{{ $inventory->color_initial}}">Print Barcodes</a>
              </td>
           
          </tr>
           @endforeach    
            </tbody>
          </table>
        </div>
      </div>
    </div>
  
  </div>
</div>
</div>
</div>
</div>
</div>
@endsection
