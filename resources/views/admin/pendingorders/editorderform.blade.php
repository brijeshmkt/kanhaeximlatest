@extends('admin.layouts.layouts')
@section('content')

    <fieldset>
        <legend>Edit {{ $product_details['TradeName'] }} details</legend>
        <label>Order Number:-</label><span>{{ $product_details['Order_id'] }}</span><br>
        <label>Trade Name:-</label><span>{{ $product_details['TradeName'] }}</span><br>
        <label>Design Number:-</label><span>{{ $product_details['DesignNo'] }}</span><br>
        <label>Quantity:-</label><span>{{ $product_details['Quantity'] }}</span><br>
        <label>Previous Price:-</label><span>{{ $product_details['Price'] }}</span><br>
        <form action="{{ url('admin/edit') }}/{{ $product_details['id'] }}" method="get">
            <label>New Price:-</label><input type="text" name="newprice">
            <br>
            <input type="submit" name="Submit" class="btn btn-primary placeorder">
            <a href="{{ URL::previous() }}" class="btn btn-primary placeorder">Back</a>
        </form>
    </fieldset>
@endsection