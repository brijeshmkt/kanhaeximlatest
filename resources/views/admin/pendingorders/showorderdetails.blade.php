@extends('admin.layouts.layouts')
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
     @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <fieldset>
        <legend>Inventory Availability</legend>
    @foreach($details as $detail)
    <label style="color: red;">Design Number:- </label><span style="color: red;">{{ $detail['DesignNo'] }}</span><br>
    <label style="color: red;">Available Quantity:- </label><span style="color: red;">{{ $detail['Quantity'] }}</span><br>
    <br>
    @endforeach
    </fieldset>
    <fieldset>
        <legend>Order Details</legend>
    <label>Order Number:-</label><span>{{ $order_details['id'] }}</span><br>
    <label>Order Date:-</label><span>{{ $order_details['Date'] }}</span><br>
    <label>Station:-</label><span>{{ $order_details['Station'] }}</span><br>
    <label>T/P:-</label><span>{{ $order_details['T_p'] }}</span><br>
    <label>Order Agent Name:-</label><span>{{ $order_details['OrderMakerType'] }}</span><br>
    </fieldset>
    <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

        <div class="row table">
            <div class="row table">
                <div class="col-sm-12 ">
                    <table id="datatable" class="table" style="width:100%">
                        <tr>
                            <th>Trade Name</th>
                            <th>Design Number</th>
                            <th>Quantity</th>
                            <th>Size</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                        @foreach($order_details->orderproducts as $op)
                            <tr>
                                <td>{{ $op['TradeName'] }}</td>
                                <td>{{ $op['DesignNo'] }}</td>
                                <td>{{ $op['Quantity'] }}</td>
                                <td>{{ $op['Size'] }}</td>
                                <td>{{ $op['Price'] }}</td>
                                <td><a class="btn btn-primary placeorder"
                                       href="{{ url('admin/editorder') }}/{{ $op['id'] }}">Edit</a></td>
                            </tr>
                        @endforeach
                    </table>
                    <a class="btn btn-primary placeorder"
                       href="{{ url('admin/confirmorder') }}/{{ $order_details['id'] }}">Confirm Order</a>
                    <a href="{{ url('/admin/order') }}" class="btn btn-primary placeorder" >Back</a>
                    </td>

                </div>
            </div>
@endsection