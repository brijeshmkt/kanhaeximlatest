@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Orders - pending orders</p>
@endsection
@section('content')
@section('content')
<div>
<button class="btn btn-primary" onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>
<br>
    <div class="pendingorders">
    <label>Order Id: </label>
    <span>{{ $order['id'] }}</span>
	</div>
    <div class="pendingorders">
    <label>Order Placed On: </label>
    <span>{{ $order['Date'] }}</span>
    </div>
    <div class="pendingorders">
    <label>Client Name: </label>
    <span>{{ $client['Name'] }}</span>
	</div>
    <div class="pendingorders">
    <label>Address: </label>
    <span>{{ $client['Address'] }}</span>
    </div>
	<div class="pendingorders">
    <label>Phone Number: </label>
    <span>{{ $client['PhoneNo'] }}</span>
    </div>
	<div class="pendingorders">
    <label>GST Number: </label>
    <span>{{ $client['GstNumber'] }}</span>
	</div>
    <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
 <div class="row table">
          <div class="row table">
          <div class="col-sm-12 ">
            <table id="datatable" class="table" style="width:100%">
        <tr>
          <th>Trade Name</th>
          <th>Design Number</th>
          <th>Quantity</th>
          <th>Price</th>
        </tr>
        @foreach($order->orderproducts as $op)
        <tr>
            <td>{{ $op['TradeName'] }}</td>
            <td>{{ $op['DesignNo'] }}</td>
            <td>{{ $op['Quantity'] }}</td>
            <td>{{ $op['Price'] }}</td>
         </tr>
        @endforeach
      </table>

    </div>
  </div>

@endsection





{{-- <h2>Client Details:-</h2><br>
    <h3>Name: </h3><h4>{{ $client['Name'] }}</h4><br>
    <h3>Address: </h3><h4>{{ $client['Address'] }}</h4><br>
    <h3>Phone Number: </h3><h4>{{ $client['PhoneNo'] }}</h4><br>
    <h3>GST Number: </h3><h4>{{ $client['GstNumber'] }}</h4>
    <br>

    <h2>Order Details:-</h2><br>
    <h3>Order placed on: </h3><h4>{{ $order['Date'] }}</h4>
    <h3>Station: </h3><h4>{{ $order['Station'] }}</h4>
    <h3>T/P: </h3><h4>{{ $order['T_p'] }}</h4>
    <h2>Products: </h2>

    <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
 <div class="row table">
          <div class="row table">
          <div class="col-sm-12 ">
            <table id="datatable" class="table" style="width:100%">
        <tr>
          <th>Trade Name</th>
          <th>Design Number</th>
          <th>Quantity</th>
          <th>Price</th>
        </tr>
        @foreach($order->orderproducts as $op)
        <tr>
            <td>{{ $op['TradeName'] }}</td>
            <td>{{ $op['DesignNo'] }}</td>
            <td>{{ $op['Quantity'] }}</td>
            <td>{{ $op['Price'] }}</td>
         </tr>
        @endforeach
      </table>

    </div>
  </div>
@endsection --}}