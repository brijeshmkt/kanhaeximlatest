@extends('admin.layouts.layouts')
@section('content')
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <div class="dataTables_length" id="datatable_length"></div>
          </div>
        </div>
        <h4 class="mt-0 header-title">Search</h4>
  <form action="{{ url('admin/usersearch') }}" method="get">
  <input type="text" name="username" placeholder="Username..." >
  	<div class="inventory_form">
  		<button type="submit" class="btn btn-primary" name="submit">Search</button>
	</div>
  </form>
  @if($flag==1)
  <a href="{{ url('admin/alluser') }}" class="button"><i title="reset">Reset</i></a>
  @endif
<div class="row table">
	<div class="col-sm-12">
        <button type="button" class="btn user_btn"><a href="{{ url('admin/createuser') }}" class="waves-effect">
          <i class="fa fa-user"></i>
          <span>Create User</span>
        </a></button>
        <table id="datatable" class="display"  style="width:100%;">
<thead>
	<tr>
		<th>ID</th>
		<th>Username</th>
		<th>Email</th>
		<th>Type</th>
		<th>Actions</th>
	</tr>
	<tbody>
		@foreach($user as $usr)
		<tr>
			<td>{{ $usr->id }}</td>
			<td>{{ $usr->name }}</td>
			<td>{{ $usr->email }}</td>
			<td>{{ $usr->type }}</td>
			<td><a href="{{ url('admin/user/edit') }}/{{ $usr['id'] }}"><i title="Edit" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-trash" title="Delete" onclick="deletedata()" aria-hidden="true"></i></td>
			<br>
		</tr>
		@endforeach
	</tbody>
</thead>
</table>
<div class="row">
	<div class="col-sm-12 col-md-5">
		<div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $user->total() }} entries</div>
	</div>
	<div class="col-sm-12 col-md-7">
		<div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
			{{ $user->links() }}
		</div>
	</div>
</div>
</div>
</div>

@endsection
@section('js')
<script>
function deletedata(id){
  swal({
  title: "Are you sure to delete?",
  text: "Once deleted, you will not be able to recover this User !",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    {{-- var id = "@if(isset($in)){{ $value->sr_no }}@endif"; --}}
    var tr = $('#table_input tr').length
                $.ajax({
                    type: "DELETE",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                     url: "@if(isset($usr)){{ route('users.destroy',[$usr->id]) }} @endif",
                    })

    swal("Alert! User has been deleted!", {
    icon: "success",

    }).then(()=>{
      location.reload(true);
    });

  } else {
    swal("User is safe!");
  }
});
};
</script>
@endsection