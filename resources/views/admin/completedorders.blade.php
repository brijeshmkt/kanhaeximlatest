@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Completed Orders</p>
@endsection
@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
  <div class="row table">
    <div class="row table">
      <div class="col-sm-12 ">
        <table id="datatable" class="table" style="width:100%">
          <tr>
            <th>Order Number</th>
            <th>Order Date</th>
            <th>Design Number</th>
            <th>Action</th>
          </tr>
          @foreach($entries as $entry)
          <tr>
            <td>{{ $entry['id'] }}</td>
            <td>{{ $entry['Date'] }}</td>
            <td>@foreach($entry->orderproducts->pluck('DesignNo') as $dno)
            {{ $dno }} @endforeach</td>
            {{-- <td><a class="btn btn-primary placeorder" href="{{ url('admin/pendingdetails') }}/{{ $entry['id'] }}">Details</a></td> --}}
            <td><a class="btn btn-primary placeorder" href="#">Details</a></td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</div>
@endsection