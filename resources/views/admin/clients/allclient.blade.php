@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Admin Console - Clients</p>
@endsection
<div class="container">
<div id="datatable_wrapper" class="dataTables_wrapper client-index container-fluid dt-bootstrap4 no-footer">
	<a href="{{ url('admin/insertclient') }}" class="a-btn pull-right">Add Clients</a>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@endif
	<div class="search-form">
	<form method="get" action="{{ url('admin/clientsearch') }}">
		<input type="text" name="cid" placeholder="Client Id">
		<input type="text" name="gstno" placeholder="Client GstNo.">
		<input type="text" name="phoneno" placeholder="Phone No.">
		<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
		<div class="inventory_form">
			<a href="{{ url('admin/viewallclient') }}" class="a-btn">Reset</a>
		</div>
	</form>
	</div>
	<div class="row table">
		<div class="col-sm-12">
			<table id="datatable" class="display"  style="width:100%;">
				<thead>
					<tr>
						<th>ID</th>
						<th>Client Name</th>
						<th>Firm Name</th>
						<th>Firm Address</th>
						<th>Phone No</th>
						<th>GST No</th>
						<th>Action</th>
					</tr>
					<tbody>
						@foreach($clients as $client)
						<tr>
							<td>{{ $client->id }}</td>
							<td>{{ $client->client_name }}</td>
							<td>{{ $client->firm_name }}</td>
							<td>{{ $client->firm_address }},&nbsp;{{ $client->firm_city }},&nbsp;{{ $client->firm_state }},&nbsp;{{ $client->firm_country }}</td>
							<td>{{ $client->contact_person_phone_number }}</td>
							<td>{{ $client->gst_number }}</td>
							<td><a href="{{ url('admin/client/edit') }}/{{ $client['id'] }}"><i title="Edit" class="fa fa-pencil-square-o icon-blue" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="deletedata()"><i class="fa fa-trash icon-red" title="Delete" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('admin/client/placeorder') }}/{{ $client['id'] }}">Place Order</a></td>
						</tr>
						@endforeach
					</tbody>
				</thead>
			</table>
			<div class="row">
				<div class="col-sm-12 col-md-5">
					<div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $clients->total() }} entries</div>
				</div>
				<div class="col-sm-12 col-md-7">
					<div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
						{{ $clients->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	@endsection
	@section('js')
	<script>
	function deletedata(id){
	swal({
	title: "Are you sure to delete?",
	text: "Once deleted, you will not be able to recover this Client !",
	icon: "warning",
	buttons: true,
	dangerMode: true,
	})
	.then((willDelete) => {
	if (willDelete) {
	{{-- var id = "@if(isset($in)){{ $value->sr_no }}@endif"; --}}
	var tr = $('#table_input tr').length
	$.ajax({
	type: "GET",
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: "@if(isset($client)){{ url('admin/client/delete') }}/{{ $client['id'] }}@endif ",
	})
	swal("Alert! Client has been deleted!", {
	icon: "success",
	}).then(()=>{
	location.reload(true);
	});
	} else {
	swal("Your Client is safe!");
	}
	});
	};
	</script>
	@section('js')