@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Admin Console</p>
@endsection
@section('content')
<div class="entry_form">
    <div class="container">
        <div>
            <button onclick="goBack()"><i class="fas fa-arrow-left back-btn"></i></button>
            <script>
            function goBack() {
                window.history.back();
            }
            </script>
        </div>
        <br>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <h3 style="font-size: 24px;font-family: inherit;margin-top: 20px">Edit client</h3>
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        <br><br><br>
        @foreach($clients as $client)
        <form action="{{ url('admin/submiteditclient/') }}/{{$client->id}}" method="post">
            @csrf
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Client Name:</label></div>
                    <div class=""><input type="text"  name="clientname" value="{{ $client->client_name}}" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Firm Name:</label></div>
                    <div class=""><input type="text"  name="firmname" value="{{ $client->firm_name}}" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Firm Address:</label></div>
                    <div class=""><input type="text"  name="firmaddress" value="{{ $client->firm_address}}" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>City:</label></div>
                    <div class="place autocomplete_txt"><input name="city" value="{{ $client->firm_city}}" type="text" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>State:</label></div>
                    <div class="place autocomplete_txt"><input name="state" value="{{ $client->firm_state}}" type="text" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Country:</label></div>
                    <div class="place autocomplete_txt"><input name="country" value="{{ $client->firm_country}}" type="text" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix ">
                    <div class="label_cont autocomplete_txt"><label>Office Address:</label></div>
                    <div class=""><textarea name="officeaddress" required="required" id="address">{{ $client->office_address }}</textarea></div>
                </div>
            </div>
            <h3>Person To Contact :- </h3>
            <div class="frm_middle clearfix">
                <div class="input clearfix remark">
                    <div class="label_cont autocomplete_txt"><label>Name:</label></div>
                    <div class=""><input type="text"  name="pcname" value="{{ $client->contact_person_name }}" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Phone Number</label></div>
                    <div class="place autocomplete_txt"><input name="phoneno" value="{{ $client->contact_person_phone_number }}"  type="text" onkeypress="isInputNumber(event)" required="required"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Office Number</label></div>
                    <div class="place autocomplete_txt"><input name="officeno" value="{{ $client->contact_person_office_phone_number }}"  type="text" onkeypress="isInputNumber(event)" ></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Landline Number</label></div>
                    <div class="place autocomplete_txt"><input name="landlineno" value="{{ $client->contact_person_land_line_phone_number }}"  type="text" onkeypress="isInputNumber(event)"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>GST No:</label></div>
                    <div class="place autocomplete_txt"><input name="gstno" value="{{ $client->gst_number }}"   type="text"></div>
                </div>
            </div>
            <div class="frm_middle clearfix">
                <div class="input clearfix">
                    <div class="label_cont"><label>Email:</label></div>
                    <div class="place autocomplete_txt"><input name="email"  type="email" value="{{ $client->email }}" required="required"></div>
                </div>
            </div>
            <input class="submit-btn" type="submit" value="submit" id="submit">
        </form>
        @endforeach
    </div>
</div>
</div>
<script type="text/javascript">
function isInputNumber(evt){
    var char=String.fromCharCode(evt.which);
     if(!(/[0-9]/.test(char)))
     {
        evt.preventDefault();
     }
}
</script>
@endsection