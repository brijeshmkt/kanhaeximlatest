@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Pack Order</p>
@endsection
@section('content')
<div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
  <div>
    <button class="a-btn" onclick="goBack()">Back</button>
    <script>
    function goBack() {
    window.history.back();
    }
    </script>
  </div>
  <div class="row table">
      <h3><b>Order Details</b></h3><br>
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="col-sm-6">

        <div>
          <label>Client Name:&nbsp;&nbsp; </label><span>{{ $orders['client']->client_name }}</span>
        </div>
        <div>
          <label>Firm Name:&nbsp;&nbsp; </label><span>{{ $orders['client']->firm_name }}</span>
        </div>
        <div>
          <label>Firm Address:&nbsp;&nbsp; </label><span>{{ $orders['client']->firm_address }}</span>
        </div>
        <div>
          <label>Firm City:&nbsp;&nbsp; </label><span>{{ $orders['client']->firm_city }}</span>
        </div>
      </div>
      <div class="col-sm-6">
      </div>
      </div>
      <div class="row table">
        <h3><b>Order Queue</b></h3>
        <form class="barcode-form">
          <input type="text" name="barcode-value" class="barcode-value" required="required">
          <input type="submit" value="Submit" class="a-btn barcode-btn">
        </form>
        <table>
          <thead>
            <tr>
              <th>Tradename</th>
              <th>Design</th>
              <th>Color</th>
              <th>Barcode</th>
            </tr>
          </thead>
          <tbody class="barcode-data">
          </tbody>
        </table>
      </div>
      <a style="display: none;" href="/admin/order/orderpacked/{{ $orders->id }}" class="packorder-btn lin"><i class="fas fa-box" style="margin-right: 5px; "></i>Pack Order</a>
      @endsection
      @section('js')
      <script type="text/javascript">
        $(document).ready(function(){
      $('form.barcode-form').on('submit',function(){
        event.preventDefault();
        var barcode = $('.barcode-value').val();

        var order_id = {{ $orders->id }};
        var client_id = {{ $orders->client_id }};
        if(barcode != ''){

            $.ajax({
            type: 'GET',
            url: '/admin/barcode/'+ barcode +'/'+order_id +'/'+client_id,
            success:function(data){
              if(data==0){ alert("Invalid Entry!!!"); return false;}
              $('.lin').show();
            var html = '<tr><td>' + data.tradename +'</td><td>'+ data.Design +'</td><td>'+ data.color +'</td><td>'+data.barcode+'</td></tr>'
              $('.barcode-data').append(html)
            }
          })
          }else{
            alert("Pleas Enter barcode");
          }
        
      })
      })
      </script>
      @endsection