Id :- {{ $order->client->id }}
<br>
Name :- {{ $order->client->client_name }}
<br>
firmname Name :- {{ $order->client->firm_name }}
<br>
firmname Address :- {{ $order->client->firm_address }}
<br>
City :- {{ $order->client->firm_city }}
<br>
Email :- {{ $order->client->email }}
<br>
PhoneNo :- {{ $order->client->contact_person_phone_number }}
<br>
GstNumber :- {{ $order->client->gst_number }}
<br>
<br>
<br>
<br>
<h3>OrderProduct</h3>

<table border="1">
	<tr>
		<th>Order Id</th>
		<th>TradeName</th>
		<th>DesignNo</th>
		<th>colour</th>
		<th>Price</th>
	</tr>
	<tr>
		<td>{{ $order->id }}</td>
		<td>{{ $order->orderproducts[0]->trade_name }}</td>
		<td>{{ $order->orderproducts[0]->designs[0]->number }}</td>
		<td>{{ $order->orderproducts[0]->designs[0]->inventory->color }}</td>
		<td>{{ $order->orderproducts[0]->rate }}</td>
	</tr>
</table>
<br>
<br>
<br>
<br>
<h3>Inventory</h3>
<table border="1">
	<tr>

		<th>TradeName</th>
		<th>DesignNo</th>
		<th>colour</th>
		<th>Barcode</th>

	</tr>
	<tr>
		<td>{{ $order->orderproducts[0]->trade_name }}</td>
		<td>{{ $order->orderproducts[0]->designs[0]->number }}</td>
		<td>{{ $order->orderproducts[0]->designs[0]->inventory->color }}</td>
		<td>{{ $order->orderproducts[0]->designs[0]->inventory->barcodes->barcode }}</td>
	</tr>
</table>
