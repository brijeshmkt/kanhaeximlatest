@extends('admin.layouts.layouts')

@section('content')
<div class="container rp-userinfo">
<div class="user-info">
        <a href="{{ URL::previous() }}"><i class="fas fa-arrow-left back-btn"></i></a>
    <table class="user-table">
        <tr>
            <th>Type</th>
            <th>Detail</th>
        </tr>
        <tr>
            <td>ID:</td>
            <td>{{ $product->id }}</td>
        </tr>
        <tr>
            <td>Lot Number</td>
            <td>@php echo implode(", ",json_decode($product->lot_numbers)) @endphp</td>
        </tr>
        <tr>
            <td>Trade Name</td>
            <td>{{ $product->tradenames->tradename }}</td>
        </tr>
        <tr>
            <td>Quality</td>
            <td>{{ $product->yarn_quality }}</td>
        </tr>
        <tr>
            <td>Width</td>
            <td>{{ $product->width }}</td>
        </tr>
        <tr>
            <td>Weight</td>
            <td>{{ $product->weight }}</td>
        </tr>
        <tr>
            <td>No of Piece</td>
            <td>{{ $product->number_of_pieces }}</td>
        </tr>
        <tr>
            <td>Total Meters</td>
            <td> {{ $product->remaining_meters }}</td>
        </tr>
        <tr>
            <td>Remarks</td>
            <td>{{ $product->remarks }}</td>
        </tr>
    </table>
</div>
</div>
@endsection