@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Edit Mill Report</p>
@endsection
@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @foreach($update as $mill)
    <form class="mb-0" method="post" action="{{ url('admin/millreport/update') }}/{{ $mill->id }}" enctype="multipart/form-data">
        @method('POST')
        @csrf
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">LOT No.</label>
        </div>

        <div id="p_scents">
            <table id="inputvalue">
                @foreach(json_decode($mill->lot_numbers) as $key => $value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td><input type="number" value="{{ $value }}" required="required" placeholder="LOT No." min="0" step="0.01" name="lot[{{ $key }}]" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control"></td>
                    <td>@if( $key > 0 )<a id="remove" onclick="remove('{{ $key+1 }}')">Remove</a>@endif</td>
                </tr>
                @endforeach
            </table>
        </div>
        <a href="#" id="addScnt" class="btn-dark btn-raised mb-0">Add LOT No.</a>
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Yarn Quality:</label>
            <input type="text"  value="{{ $mill->yarn_quality }}" placeholder="Quality" min="0" step="0.01" name="quality" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="exampleInputEmail1">
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleInputEmail1" class="bmd-label-floating ">Trade Name:</label>
            <input type="text" class="form-control" name="treadname"  value="{{ $mill->tradenames->tradename }}" placeholder="Tread name" id="exampleInputEmail1">
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Width of the cloth:</label>
            <input type="number"  value="{{ $mill->width }}" placeholder="Width" min="0" step="0.01" name="width" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="exampleInputEmail1">
        </div>
         <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Weight in grams:</label>
            <input type="number"  value="{{ $mill->weight }}" placeholder="Waight" min="0" step="0.01" name="waight" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="exampleInputEmail1">
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Number of Pieces</label>
            <input type="number"  value="{{ $mill->number_of_pieces }}" placeholder="No. OF Pieces" min="0" step="0.01" name="pieceno" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="exampleInputEmail1">
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Total Meters</label>
            <input type="number"  value="{{ $mill->remaining_meters }}" placeholder="Total Meters" min="0" step="0.01" name="totalmeter" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="exampleInputEmail1">
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Remarks</label>
            <input type="text" class="form-control" name="remarks"  value="{{ $mill->remarks }}" id="exampleInputEmail1" placeholder="Remarks">
        </div>
        <div class="mill-btns">
        <button type="submit" name="submit" class="submit-btn">Submit</button>
        <a class="submit-btn" href="{{ url('/admin/millreport') }}">Cancel</a>
        </div>
    </form>
</div>
@endforeach


@endsection
@section('js')
<script>
 $( function() {

   var scntDiv = $('#p_scents');
    var i = $('#p_scents input').length + 1;
    $('#addScnt').on('click', function() {
            $('<tr id="f-'+i+'"><td>'+i+'</td><td><input type="number" required="required" value="" placeholder="LOT No" min="0" step="0.01" name="lot['+i+']" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control"><td><a id="remove" onclick="remove('+i+')">Remove</a></td></tr>').appendTo('#inputvalue');
            i++;
            return false;
    });
 } );
function remove(id) {
    $('#f-'+id).remove();
    i--;
}

$("#trade-name").autocomplete({
    type: "GET",
    dataType: "JSON",
    source: "{{ url('admin/findtradenames') }}",
    select: function(event, ui) {
        var name = $("#trade-name").val();
        $("#trade-name").attr("#tradenames");
    }
});
</script>
@endsection
