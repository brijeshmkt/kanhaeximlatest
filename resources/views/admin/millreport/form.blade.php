@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Mill Report - Add New Mill</p>
@endsection
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    
    <form class="mb-0 mill_form" id="create-mill-form" method="post" action="/admin/millreport/store" id="add-inventory">
        @method('POST')
        @csrf

        <div class="form-group bmd-form-group">
            <label class="bmd-label-floating">Lot Numbers<span class="req-cross">*</span></label>
            <div class="info-box">
            <i class="fa fa-info-circle info-btn" aria-hidden="true"></i>
            <span class="info">For multiple lot numbers use comma separated values E.g. 123, ab123</span>
            </div>
            <input type="text" required="required" placeholder="123, abc415" name="lot_numbers" class="form-control">
        </div>
        
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Yarn Quality<span class="req-cross">*</span></label>
            <div class="info-box">
            <i class="fa fa-info-circle info-btn" aria-hidden="true"></i>
            <span class="info">Quality name given by the manufacturer to determine the quality of the yarn</span>
            </div>
            <input type="text" @if(old('quality')) value="{{ old('quality') }}" @endif placeholder="Name of Material Quality" name="quality" class="form-control" id="yarn-quality" required
            title="Yarn quality name required">
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleInputEmail1" class="bmd-label-floating ">Trade name<span class="req-cross">*</span></label>
            <div class="info-box">
            <i class="fa fa-info-circle info-btn" aria-hidden="true"></i>
            <span class="info">Custom name to distinguish the deal or trade.</span>
            </div>
            <select class="form-control" name="tradename_id" required>
                <option value="">Select Trade Name</option>
                
                @foreach( $tradenames as $tradename)
                    <option value="{{ $tradename->id }}">{{ $tradename->name }}</option>
                    
                @endforeach

                

            </select>
            
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Width of the cloth (mtrs)<span class="req-cross">*</span></label>
            <input type="text" @if(old('width')) value="{{ old('width') }}" @endif placeholder="Width of the cloth to determine the width in Meters" min="0" step="0.01" name="width" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="cloth-width" title="Width of the cloth is required" required onkeypress="isInputNumber(event)">
            
        </div>
         <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Weight in grams<span class="req-cross">*</span></label>
            <div class="info-box">
            <i class="fa fa-info-circle info-btn" aria-hidden="true"></i>
            <span class="info">Weight of the cloth in grams.</span>
            </div>
            <input type="text" @if(old('weight')) value="{{ old('weight') }}" @endif placeholder="Weight of the cloth in grams" min="0" step="0.01" name="weight" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="yarnweight" required title="Yarn weight is required" onkeypress="isInputNumber(event)">
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Number of Pieces</label>
            <div class="info-box">
            <i class="fa fa-info-circle info-btn" aria-hidden="true"></i>
            <span class="info">Pieces made(cut) from the lot</span>
            </div>
            <input type="text" @if(old('pieceno')) value="{{ old('pieceno') }}" @endif placeholder="Number of pieces" min="0" step="0.01" name="pieceno" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="exampleInputEmail1" onkeypress="isInputNumber(event)">
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Total Meters</label>
            <input type="text" @if(old('orignal_meters')) value="{{ old('orignal_meters') }}" @endif placeholder="Total Meters of complete lot" min="0" step="0.01" name="orignal_meters" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="total-meters" required title="Total Meters is required" onkeypress="isInputNumber(event)">
        </div>
        <div class="form-group bmd-form-group">
            <label for="exampleTextarea" class="bmd-label-floating">Remarks</label>
            <input type="text" class="form-control" name="remarks" @if(old('remarks')) value="{{ old('remarks') }}" @endif id="exampleInputEmail1" placeholder="Remarks if any..">
        </div>
        <div class="mill-btns">
        <input type="submit" name="submit" class="submit-btn" value="submit">
        <a class="submit-btn" href="{{ url('/admin/millreport') }}">Cancel</a>
        </div>
    </form>
</div>


@endsection
@section('js')
<script type="text/javascript">
function isInputNumber(evt){
    var char=String.fromCharCode(evt.which);
     if(!(/[0-9]/.test(char)))
     {
        evt.preventDefault();
     }
}
</script>
<script>
 $( function() {

   var scntDiv = $('#p_scents');
    var i = $('#p_scents input').length + 1;
    $('#addScnt').on('click', function() {
            $('<tr id="f-'+i+'"><td>'+i+'</td><td><input type="number" required="required" value="" placeholder="LOT No" min="0" step="0.01" name="lot['+i+']" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control"><td></td><td><a id="remove" onclick="remove('+i+')">Remove</a></td></tr>').appendTo('#inputvalue');
            i++;
            return false;
    });
 } );
function remove(id) {
    $('#f-'+id).remove();
    i--;
}


$("#trade-name").autocomplete({
    type: "GET",
    dataType: "JSON",
    source: "{{ url('admin/findtradenames') }}",
    select: function(event, ui) {
        var name = $("#trade-name").val();
        $("#trade-name").attr("#tradenames");
    }
});
</script>
@endsection
