@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')

  @if ( isset($unusedInventory) )
    <p>Unused Mill Reports</p>
  @else
    <p>Mill Report</p>
  @endif
@endsection
<div class="card-body millreport clearfix">
  <div class="container">
    <div class="breadcrumbs">
      <a class="text-link" href="{{ url()->previous() }}"><i class="fas fa-arrow-left back-btn"></i></a>
      <div class="section-filters">
        <div class="search-form">
          <form action="{{ url('admin/rowproductsearch') }}" method="get">
            <input type="text" name="trade" placeholder="Trade Name..." >
            <input type="text" name="mrno" placeholder="MR No" >
            <button type="submit">
            <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </form>
          <a href="/admin/millreport">Clear Search</a>
        </div>


        <div class="button-container">
          <li>
            <a class="a-btn" href="{{url('admin/millreport/create') }} ">Add New</a>
          </li>
          
          @if (!isset($unusedInventory) ) 
          <li>
            <a class="a-btn" href="/admin/pendingmill">View Un-used Mill </a>
          </li>
          @else
            <li>
              <a class="a-btn" href="/admin/millreport">View All</a>
            </li>
          @endif





          
          
          
          
        </div>
      </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

  <div class="card-body clearfix">
    <p class="text-muted font-14">
    </p>
    @if(session()->has('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}
    </div>
    @endif

    <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
      <div class="row">
        <div class="col-sm-12 col-md-6">
        </div>
      </div>
      <div class="row table">
        <div class="col-sm-12">
          <table id="datatable" class="display" style="width:100%">
            <thead>
              <tr>
                <th>MR. No.</th>
                <th>Date</th>
                <th>LOT No.</th>
                <th>Quality</th>
                <th>Trade Name</th>
                <th>Used Meters</th>
                <th>Total Meter</th>
                <th>Action</th>
              </tr>
              <tbody>
                @foreach($row_product as $row)
                <tr>
                  <td><a href="/admin/millreport/show/{{ $row->id }}">{{ $row->id }}</a></td>
                  <td>{{ $row->created_at->format('d/m/Y H:i') }}</td>
                  <td>{{ $row->lot_numbers}}</td>
                  <td>{{ $row->yarn_quality}}</td>
                  <td>{{ $row->tradenames->name}}</td>
                  <td>{{ $row->used_meters}}</td>
                  <td>{{ $row->orignal_meters}}</td>
                  
                  <td class="icons-tab">
                    @if (isset($unusedInventory) ) 
                    <a href="{{ url('admin/millreport/addinventory') }}/{{ $row->id }}">
                      <i class="fa fa-plus-square icon-blue" aria-hidden="true" title="Add Inventory"></i>
                    </a>
                    @endif
                    
                    <a href="{{ url('admin/millreport/edit') }}/{{ $row->id }}">
                      <i class="fas fa-edit icon-blue"></i>
                    </a>
                     

                    <i class="fa fa-trash icon-red" aria-hidden="true" onclick="deletedata({{ $row->id }})" title="delete"></i>

                  </td>
                </tr>
                @endforeach
              </tbody>
            </tbody>
          </table>
          
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-md-5">
          @if(isset($pendingmill))
          <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $pendingmill->total() }} entries</div>
          @else
          <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $row_product->total() }} entries</div>
          @endif
        </div>
        <div class="col-sm-12 col-md-7">
          @if(isset($pendingmill))
          <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
            {{ $pendingmill->links() }}
          </div>
          @else
          <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
            {{ $row_product->links() }}
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script>
function deletedata(id){
swal({
title: "Are you sure to delete ?",
text: "Once deleted, you will not be able to recover this Mill Report !",
icon: "warning",
buttons: true,
dangerMode: true,
})
.then((willDelete) => {
if (willDelete) {
{{-- var id = "@if(isset($in)){{ $value->sr_no }}@endif"; --}}
var tr = $('#table_input tr').length
var url = "{{ url('admin/millreport/destroy') }}";
url += '/'+id;
$.ajax({
type: "POST",
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
},
url : url,
})
swal("Alert! Your Mill has been deleted!", {
icon: "success",
}).then(()=>{
location.reload(true);
});
} else {
swal("Your Mill is safe!");
}
});
};

</script>
@endsection