@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Notifications</p>
@endsection
@section('content')

<div class="notification-container">
	<div class="user-info notificationcenter">
		<table id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer user-table">
			<tr>
				<th>Date</th>
				<th>Module</th>
				<th>Action</th>
				<th>Detail</th>
			</tr>

			@foreach($notifications as $n)

			<tr>
				<td> {{ $n->created_at->format('d/m/Y H:i') }}</td>
				<td>{{$n->type}} </td>
				<td>{{ $n->action }}</td>
				<td>
				<?php
					$str=str_replace(",\"","^\"",$n->detail);
						$ar=explode("^",$str);
						$arn=array();
						
						foreach ($ar as  $v)
						 {
						 	$v1=explode("\":", $v);
						 	
							 @$val[ltrim($v1[0],"{\"")] = trim($v1[1],"\"");
							 
						}
						
						foreach ($val as $key => $value) {

								echo "".strtoupper($key)." = ".ucwords($value)." "." "."<br>";
								
								
						}

				?>
					
					
				</td>
			</tr>
			@endforeach

		</table>
		
	</div>
</div>

@endsection