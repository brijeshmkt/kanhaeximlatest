Date :- {{ Date("d/m/Y H:i:s") }}
<br>
Phone No. :- {{ $order[0]->client->contact_person_phone_number }}
<br>
Firm Name :- {{ $order[0]->client->firm_name }}
<br>
Name :- {{ $order[0]->client->client_name }}
<br>
FirmAddress:- {{ $order[0]->client->firm_address }}
<br>
OfficeAddress:- {{ $order[0]->client->office_address }}
<br>
Station :- {{ $order[0]->destination_station }}
<br>
Transporter Name :- {{ $order[0]->transporter_name }}
<br>
Trade Name :-  @foreach($tradename as $qu){{ $qu->name}}, &nbsp; @endforeach
<br>
Design No. :- @foreach($order[0]->orderproducts[0]->designs as $qu){{ $qu->number }} , &nbsp; @endforeach
<br>
Rate :- @foreach($order[0]->orderproducts as $qu){{ $qu->rate }} , &nbsp; @endforeach
<br>
Quantity :- @foreach($order[0]->orderproducts as $qu){{ $qu->quantity }} , &nbsp; @endforeach
<br>
Gst No. :- {{ $order[0]->client->gst_number }}
<br>
Remark :- {{ $order[0]->remark }}
<br>
