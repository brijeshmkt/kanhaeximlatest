@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>All Designs</p>
@endsection
<div class="container">
		<a href="{{ url('admin/dashboard')}}"><i class="fas fa-arrow-left back-btn"></i></a>
	<div class="upperbar">
		<a href="{{ url('admin/add-design') }}"><i class="fas fa-plus-square icon-blue"></i>&nbsp;Add Design</a>
	</div>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@endif
	<div class="row table">
		<div class="col-sm-12">
			<table id="datatable" class="display"  style="width:100%;">
				<thead>
					<tr>
						<th>Trade Name</th>
						
						<th>Design Number</th>
						<th>Image</th>
						<th>Description</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach($alldesigns as $design)
					<tr>
						<td>{{ $design->tradename->name }}</td>
						
						<td>{{$design->number}}</td>
						<td><img src="{{asset('public/'.$design->image)}}" value="{{$design->image}}" alt="Image" height="auto" width="150" /></td>
						<td>{{$design->description}}</td>
						
					</tr>
					@endforeach
				</tbody>
			</table>
			<div class="row">
				<div class="col-sm-12 col-md-5">
					<div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Total {{ $alldesigns->total() }} entries</div>
				</div>
				<div class="col-sm-12 col-md-7">
					<div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
						{{ $alldesigns->links() }}

					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection