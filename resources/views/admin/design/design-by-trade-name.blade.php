@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Design By Trade Name</p>
@endsection
<div class="container">
		<a href="{{ url('/')}}"><i class="fas fa-arrow-left back-btn"></i></a>
	<div class="upperbar">
		<a href="{{ url('admin/add-design') }}"><i class="fas fa-plus-square icon-blue"></i>&nbsp;Add Design</a>
	</div>
	
	
	<h1>Design By tradeName</h1>
	<div class="row table">
		<div class="col-sm-12">
			<table id="datatable" class="display">
				<thead>
					<tr>
						
						<th>Design Number</th>
						<th>Image</th>
						<th>Description</th>
						
					</tr>
				</thead>
				<tbody>
					{{ $designs }}
					@foreach($designs as $design)
					<tr>
						
						
						<td>{{$design->number}}</td>
						<td><img src="{{asset($design->image)}}" value="{{$design->image}}" alt="Image" height="auto" width="150" /></td>
						<td>{{$design->description}}</td>
						
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	@endsection