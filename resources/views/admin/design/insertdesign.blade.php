@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Add Design</p>
@endsection
<div class="notification-container">
	<div class="container">
	{{-- {{ dd($<table></table>radename) }} --}}
	<a href="{{ url('/admin/designs')}}"><i class="fas fa-arrow-left back-btn"></i></a>
	@if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- {{ dd($tradename) }} --}}
<form action="{{ url('admin/insert-design')}}" method="post" id="myform" enctype="multipart/form-data">
	@csrf
	<div>
	<label>Design Number<span class="req-cross">*</span>: </label>
	<input type="text" name="dnum" id="dnum" required="required">
	</div>
	<div>
	<label>Design Image<span class="req-cross">*</span>: </label>
	<input type="file" required="required" accept="image/*" name="dimage" id="dimage" title="Image is required">
	</div>
	<div>
	<label>Description: </label>
	<input type="text" name="ddes" id="ddes">
	</div>
	<div>
	<label>Trade Name: </label>
	<select name="tradename">
		@foreach($tradenames as $tradename)
		<option value="{{$tradename->id}}">{{$tradename->name}}</option>
		@endforeach
	</select>
	</div>
	<div>
		<input type="submit" value="Submit" class="submit-btn">
	</div>
</form>
</div>
</div>
@endsection