@extends('admin.layouts.layouts')
@section('content')
@section('sectiontitle')
<p>Add Design</p>
@endsection
<div class="notification-container">
	<div class="container">
	
	<div class="upperbar">
		<a href="{{ url('admin/tradename') }}">View TradeNames</a>
	</div>
	
	<div class="row table">
		<div class="col-sm-12">
			<table id="datatable" class="display">
				<thead>
					<tr>
						<th>Design Number</th>
						<th>Image</th>
						<th>Description</th>
						<th>Trade Name</th>
						
					</tr>
				</thead>
				<tbody>
					
					@foreach($greatDesigns as $design)
					<tr>
						
						
						<td>{{$design->number}}</td>
						<td><img src="{{asset($design->image)}}"/></td>
						<td>{{$design->description}}</td>
						<td>{{$design->tradename->name}}</td>
						
					</tr>
					@endforeach
				</tbody>
			</table>	
     	</div>
     </div>   

	</div>
</div>
@endsection