@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Trade names - Insert</p>
@endsection
@section('content')
<div class="container">
	<div class="tradename-form">
		<a href="{{ url('admin/tradename')}}"><i class="fas fa-arrow-left back-btn"></i></a>
		<form method="POST" action="{{ url('admin/insert-tradename') }}" id="myform">
			@csrf
			<div class="form-group">
				<label>Trade Name: </label>
				<input type="text" name="name" required="required">
			</div>
			<div class="form-group">
				<label>Initial: </label>
				<input type="text" name="initial" required="required">
			</div>
			<div class="form-group">
				<input type="submit" name="submit" class="a-btn" value="Submit">
			</div>
		</form>
	</div>
</div>
@endsection