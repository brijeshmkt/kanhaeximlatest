@extends('admin.layouts.layouts')
@section('sectiontitle')
<p>Trade names</p>
@endsection
@section('content')
<div class="container">
		<a href="{{ url('admin/dashboard')}}"><i class="fas fa-arrow-left back-btn"></i></a>
	<div class="upperbar">
		<a href="{{ url('admin/add-tradename') }}"><i class="fas fa-plus-square icon-blue"></i>&nbsp;Add Tradename</a>
	</div>

	  <div class="row table">
            <div class="row table">
                <div class="col-sm-12 ">
			        <table id="datatable" class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Initial</th>
								<th>Name</th>
								<th>View</th>
							</tr>
						</thead>
						<tbody>
					@foreach($tradenames as $tradename)
						<tr>
							<td>{{$tradename->id}}</td>
							<td>{{$tradename->name}}</td>
							<td>{{$tradename->initial}}</td>
							<td><a href="/admin/tradename-designs/{{$tradename->id}}">Designs</a></td>
						</tr>
					@endforeach
						</tbody>
					</table>
				</div>
			</div>
	</div>

@endsection