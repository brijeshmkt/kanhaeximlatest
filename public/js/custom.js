// Datepicker
$(function() {
    $("#datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    }).datepicker("setDate", "0");
});

$(document).ready(function() {
    $('#tnc').click(function() {
        if ($(this).prop("checked") == true) {
            this.value = "1";
        }
    });
    // Auto suggection
    // Add and Remove class in Admin Menu 
    $(".has_sub").click(function() {
        $(this).toggleClass("nav-active");
    });
});

// for go back
function goBack() {
    window.history.back();
}

// datatable

// $(document).ready(function() {
//     $('.inventory-index table').DataTable( {
//         "pageLength": 10,
//     } );
// } );
// $.fn.dataTable.ext.errMode = 'throw';

// form validation

$("#create-user").validate();
$("#create-mill-form").validate();
$("#edit-user-form").validate();
$("#new-agent-form").validate();
$("#orderform").validate();

    $("#add-inventory").validate({
       rules: {
           des_number: {
               number: true,
               required: true
           },
           quantity: {
               number: true,
               required: true
           }
       },
       messages: {
           des_number: {
               number: "Numbers Only",
           },
           quantity: {
               number: "Please input with number only",
               required: "Please input with number only"
           },
       }
   }); 
    $("#myform").validate({
        groups: {  // consolidate messages into one
            name: "aname mobile gstno"
        },
     rules: {
         aname: {
             required: {
                 depends: function(element) {
                     return $("#mobile").val() !== null;;
                     return $("#gstno").val() !== null;
                 }
             }
         },
         mobile: {
             required: {
                 depends: function(element) {
                     return $("#aname").val() !== null;;
                     return $("#gstno").val() !== null;
                 }
             }
         },
         gstno: {
             required: {
                 depends: function(element) {
                     return $("#mobile").val() !== null;;
                     return $("#aname").val() !== null;
                 }
             }
         },pass: {
                required : true,
                
          },cpass: {
                required : true,
                equalTo: "#pass",
          },officeaddress: {
                required : true,
          },
           "#pass": {
                required : true,
                
          },cpass: {
                required : true,
                equalTo: "#pass",
          },officeaddress: {
                required : true,
          },
         
     }
     
 });

    

jQuery(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("tbody tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});