<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		//
		DB::table('users')->insert([

			[
				'name' => "admin",
				'email' => "admin@admin.com",
				'type' => "admin",
				'role' => "admin",
				'password' => bcrypt('admin'),
			], [
				'name' => "manager",
				'email' => "manager@manager.com",
				'type' => "manager",
				'role' => "manager",
				'password' => bcrypt('manager'),
			], 
			[
				'name' => "agent",
				'email' => "agent@agent.com",
				'type' => "agent",
				'role' => "agent",
				'password' => bcrypt('agent'),
			],
			[
				'name' => "Brijesh",
				'email' => "a@a.com",
				'type' => "admin",
				'role' => "admin",
				'password' => bcrypt('123456'),
			],
		]);
	}
}
