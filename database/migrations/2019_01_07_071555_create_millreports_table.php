<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMillreportsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('millreports', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('lot_numbers');
			$table->string('yarn_quality');
			$table->integer('tradename_id');
			$table->float('width', 8, 2);
			$table->float('weight', 8, 2);
			$table->integer('number_of_pieces')->nullable();
			$table->integer('used_meters')->default(0);
			$table->integer('orignal_meters');
			$table->string('remarks')->nullable();
			
			$table->timestamps();
		});

		DB::table('millreports')->insert([
				'user_id' => 1,
				'lot_numbers' => 'abc112, 1223',
				'yarn_quality' => 'Blue Denim',
				'tradename_id' => 1,
				'width' => 10.00,
				'weight' => 10.00,
				'number_of_pieces' => 16,
				'used_meters' => 0,
				'orignal_meters' => 1000,
				'created_at' => date("Y-m-d H:i:s")
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('millreports');
	}
}
