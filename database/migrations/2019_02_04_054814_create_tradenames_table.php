<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradenamesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('tradenames', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->string('initial')->nullable();
		});

		DB::table('tradenames')->insert(['name' => 'Spark', 'initial' => 'SP']);
		DB::table('tradenames')->insert(['name' => 'Anchor', 'initial' => 'AN']);
		DB::table('tradenames')->insert(['name' => 'Royalty', 'initial' => 'RO']);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 *
	 */
	public function down() {
		Schema::dropIfExists('tradenames');
	}
}
