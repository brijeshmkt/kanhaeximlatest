<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('orders', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('client_id');
			$table->string('destination_station');
			$table->string('transporter_name');
			$table->string('payment_terms')->comment('user select payment terms');
			$table->string('terms')->comment('terms and conditions'); // Terms and conditions
			$table->integer('approval')->default(0);//admin 0 unapproval and 1 upproval by admin
			$table->string('remarks')->nullable();
			$table->string('status')->comment('unapprove,approve,outofstock,completed')->nullable();
			$table->string('ordermakertype');
			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('orders');
	}
}
