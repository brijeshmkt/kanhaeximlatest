<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('inventories', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('millreport_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('tradename_id')->nullable();
			$table->integer('design_id')->nullable();

			$table->string('color')->nullable();
			$table->string('color_initial')->nullable();
			$table->integer('quantity')->nullable();
			$table->integer('order_id')->nullable();
			
			$table->boolean('deleted')->default(0);
			$table->timestamps();
			

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('inventories');
	}
}
