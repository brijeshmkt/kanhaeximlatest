<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('clients', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('client_name');
			$table->string('firm_name')->nullable();
			$table->string('firm_address')->nullable();
			$table->string('firm_city')->nullable();
			$table->string('firm_state')->nullable();
			$table->string('firm_country')->nullable();
			$table->string('office_address')->nullable();
			$table->string('contact_person_name')->nullable();
			$table->string('contact_person_phone_number')->nullable();
			$table->string('contact_person_office_phone_number')->nullable();
			$table->string('contact_person_land_line_phone_number')->nullable();
			$table->string('gst_number')->nullable();
			$table->string('email')->nullable();
			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('clients');
	}
}
