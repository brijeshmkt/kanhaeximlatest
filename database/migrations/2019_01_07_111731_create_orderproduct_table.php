<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('order_products', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('order_id');
			$table->integer('tradename_id');
			$table->string('design_id');
			$table->integer('quantity');
			$table->float('rate', 10, 2);
			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('order_products');
	}
}
