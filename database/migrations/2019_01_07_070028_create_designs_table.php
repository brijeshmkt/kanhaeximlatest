<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('designs', function (Blueprint $table) {
			$table->increments('id');
			$table->string('number');
			$table->string('image')->nullable();;
			$table->integer('tradename_id');
			$table->string('description')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

		DB::table('designs')->insert(['number' => '111', 'tradename_id' => '1']);
		DB::table('designs')->insert(['number' => '222', 'tradename_id' => '1']);
		DB::table('designs')->insert(['number' => '333', 'tradename_id' => '1']);

		DB::table('designs')->insert(['number' => '444', 'tradename_id' => '2']);
		DB::table('designs')->insert(['number' => '555', 'tradename_id' => '2']);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('designs');
	}
}
