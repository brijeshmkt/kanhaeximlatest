<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('type')->default('admin');
			$table->string('role')->nullable();
			$table->string('email')->unique();
			$table->string('agent_id')->nullable();
			$table->string('client_id')->nullable();
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});

		$pwd = bcrypt('123456');
		DB::table('users')->insert([
			'name' => 'BAdmin', 
			'email' => 'a@a.com', 
			'password' => $pwd
		]);
		DB::table('users')->insert(['name' => 'admin', 'email' => 'admin@admin.com', 'password' => $pwd]);
		DB::table('users')->insert(['name' => 'agent', 'email' => 'agent@agent.com', 'type' => 'agent', 'password' => $pwd]);
		DB::table('users')->insert(['name' => 'manager', 'email' => 'manager@manager.com', 'type' => 'manager', 'password' => $pwd]);

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('users');
	}
}
