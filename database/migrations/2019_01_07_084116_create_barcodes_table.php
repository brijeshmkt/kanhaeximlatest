<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarcodesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('barcodes', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('inventory_id');
			$table->string('color');
			$table->string('tradename');
			$table->integer('design_id');
			$table->string('barcode');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('barcodes');
	}
}
