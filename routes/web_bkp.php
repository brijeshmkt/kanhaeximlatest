<?php



// Trade Name Module
Route::get('admin/tradename', 'TradenameController@index');
Route::get('admin/tradename-designs/{id}', 'DesignController@byTradename');
Route::get('admin/add-tradename', 'TradenameController@create');
Route::post('admin/insert-tradename', 'TradenameController@store');

Route::get('admin/inventory/newedit', 'InventoryController@newEdit');
// Delete all junk that is not required.

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// Login Route Start
Route::get('/', function () {
	if (isset(Auth::user()->type)) {
		switch (Auth::user()->type) {
		case 'admin':
			return redirect('/admin/dashboard');
			break;
		case 'manager':
			return redirect('/manager/dashboard');
			break;
		case 'agent':
			return redirect('/agent/clients');
			break;
		}
	} else {
		return view('login');
	}
});
// Route::get('/login', 'Auth\LoginController@authenticated')->name('login');
Auth::routes();
// Login Route End

// Admin Route Start
Route::middleware(['auth', 'admin'])->prefix('admin')->group(function () {
// dashboard routes
	Route::get('dashboard', 'DashboardController@index');
	Route::get('console', 'DashboardController@console')->name('admin/console');
	// dashboard routes end
	// client route start
	Route::get('/viewallclient', 'ClientController@index');
	Route::get('/insertclient', 'ClientController@create');
	Route::post('/addnewclient', 'ClientController@store');
	Route::get('/clientsearch', 'ClientController@search');
	Route::get('client/edit/{id}', 'ClientController@edit');
	Route::get('client/delete/{id}', 'ClientController@destroy');
	Route::post('submiteditclient/{id}', 'ClientController@update');
	// client route end
	// admin agent form route start
	Route::get('/allagents', 'AgentController@allagent');
	Route::get('/newagent', 'AgentController@newagent');
	Route::post('/insertagent', 'AgentController@insertagent');
	Route::get('agent/edit/{id}', 'AgentController@edit');
	Route::post('editagent/{id}', 'AgentController@update');
	Route::get('agent/delete/{id}', 'AgentController@destroy');
	Route::get('agentsearch', 'AgentController@search');
	// admin agent form route end
	// Mill report and inventory
	Route::get('pendingmill', 'MillController@pendingmill');
	Route::post('millreport/create', 'MillController@create');
	Route::post('millreport/store', 'MillController@store');
	Route::get('millreport/edit/{id}', 'MillController@edit');
	Route::get('millreport/show/{id}', 'MillController@show');
	Route::post('millreport/update/{id}', 'MillController@update');
	Route::post('millreport/destroy/{id}', 'MillController@destroy');

	Route::get('millreport/addinventory/{id}', 'InventoryController@add');
	Route::post('/milreport/inventory/add/{id}', 'MillController@storeinventory');
	Route::post('/milreport/inventory/update/{id}', 'MillController@updateinventory');

	Route::post('inventory/store', 'InventoryController@store');
	Route::post('inventory/colour/delete/{id}', 'InventoryController@colourdelete');
	Route::post('inventory/destroy/{id}', 'InventoryController@destroy');
	Route::post('inventory/design_no/{tname}', 'InventoryController@design_no');

	Route::get('inventory/edit/{id}', 'InventoryController@edit');
	

	// Route::get('inventory/edit-inventories/id/{id}/color/{color}', 'InventoryController@editInventories');

	Route::get('inventory/barcode/{designno}', 'InventoryController@barcode');
	Route::post('inventory/barcode/generate', 'InventoryController@barcodegenerate');

	Route::post('millreport/create', 'MillController@create');
	Route::get('/rowproductsearch', 'MillController@searchrowproduct');
	Route::get('/inventorysearch', 'InventoryController@searchforinventory');
	Route::get('findtradenames', 'TradenameController@searchajax');
	Route::resources([
		'millreport' => 'MillController',
		// 'inventory' => 'InventoryController',
		// 'users' => 'UserController',
	]);
	// Mill report and inventory end
	// Order form route start
	Route::get('order', 'OrderFormController@index');
	Route::get('order/neworder', 'OrderFormController@create');
	Route::get('order/pendingorder', 'OrderFormController@pendingorder');
	Route::get('order/requestedorder', 'OrderFormController@requestedorder');
	Route::get('order/completedorder', 'OrderFormController@completedorder');
	Route::get('order/confirmorder', 'OrderFormController@confirmorder');
	Route::post('orderform/{id}', 'OrderFormController@store');
	// Route::post('orderform/{id}', function () {
	// 	return 'Hello World';
	// });
	Route::get('order/deleteentry/{id}', 'OrderFormController@destroy');
	Route::get('client/placeorder/{id}', 'OrderFormController@create');
	Route::post('orderform/requestorder/{id}', 'OrderFormController@saverequested');
	Route::get('getId/{id}', 'OrderFormController@orderconform');
	Route::get('pendingorder/search', 'OrderFormController@ordersearch');
	Route::get('order/requestedorder/search', 'OrderFormController@requestsearch');
	Route::get('order/completedorder/search', 'OrderFormController@completedordersearch');
	Route::get('confirmorder/packorder/{id}', 'CompleteOrderController@completedorder');
	Route::get('download_order_pdf/{id}', 'OrderFormController@download_pdf');
	Route::post('order/checkinventory/{id}', 'OrderFormController@checkinventory');

	// Order form route end
	//User Route Start
	Route::get('alluser', 'UserController@index');
	Route::get('createuser', 'UserController@crate');
	Route::get('user/edit/{id}', 'UserController@edit');
	Route::post('user/update/{id}', 'UserController@update');
	Route::get('user/delete/{id}', 'UserController@delete');
	Route::get('/usersearch', 'UserController@searchuser');
	Route::get('/notifications', 'DashboardController@notify');
	//User Route End

	// Packaging
	Route::get('/packaging', 'PackagingController@index');
	Route::get('/packaging/create', 'PackagingController@create');
	Route::post('order/packingorder', 'PackingController@PackingOrder');
	Route::get('order/packingorder/{id}', 'PackingController@generate_packing_invoice');

	// Designs
	Route::get('/designs', 'DesignController@index');
	Route::get('/add-design', 'DesignController@show');
	Route::get('/design/tradename/{id}', 'DesignController@designByTradeName');
	Route::post('/insert-design', 'DesignController@insert');
	Route::get('/finddesignno', 'DesignController@search');
	Route::get('/check-inventory/{data}', 'OrderFormController@checkInventory');
	
	
	Route::post('/search-design-number', 'DesignController@searchDesign');
});

