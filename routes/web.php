<?php



Auth::routes();
//edite
//Route::get('admin/alluser ','DashboardController@AllUser');
Route::get('admin/allagents', 'AgentController@allagent');
Route::get('admin/newagent', 'AgentController@newagent');
Route::get('admin/clientsearch','ClientController@search');
Route::get('admin/agentsearch','AgentController@search');
Route::get('admin/alluser','UserController@index');	
Route::get('admin/user/edit/{id}','UserController@edit');
Route::post('admin/user/update/{id}','UserController@update');
Route::get('admin/usersearch','UserController@searchuser');
Route::get('admin/user/delete/{id}','UserController@destroy');
Route::get('admin/createuser','UserController@crate');
Route::post('admin/stor','UserController@store');
Route::get('admin/rowproductsearch','MillController@searchrowproduct');
Route::get('admin/pendingorder/search','OrderFormController@alloredersearch');
Route::get('admin/partial/search','OrderFormController@pendingordersearch');
Route::get('admin/outofstock/search','OrderFormController@requestedordersearch');
Route::get('admin/order/Confirm/search','OrderFormController@confirmorderssearch');
Route::get('admin/download_order_pdf/{id}','OrderFormController@download_pdf');
Route::get('admin/admin_approval/{id}','OrderFormController@approval');
Route::get('admin/approvalorder','OrderFormController@approvalorder');



Route::get('agent/clients','AgentController@index');
Route::get('agent/inventary','AgentController@inventary');
Route::get('agent/viewallclient','AgentController@viewallclient');
Route::get('agent/insertclient','AgentController@insertclient');
Route::post('agent/addnewclient','AgentController@addnewclient')->name('agent/addnewclient');
Route::get('agent/design/alldesigns', 'DesignController@allDesignsAjax');
Route::get('agent/design/all-design-numbers', 'DesignController@allDesignNumbersAjax');
Route::post('agent/order/place-order', 'OrderFormController@processOrder');
Route::get('agen/order/allOrder','AgentController@allorder');
Route::get('agent/design/tradename/{id}', 'DesignController@designByTradeName');
Route::get('agent/client/placeorder/{id}', 'AgentController@placeorder');






Route::post('admin/insertagent','AgentController@insertagent');
Route::get('console', 'DashboardController@console')->name('admin/console');
Route::middleware(['auth', 'admin'])->prefix('admin')->group(function () {
Route::get('barcode/{order_id}/{barcode}/{client_id}', 'PackagingController@show');
Route::get('pending-orders','PendingOrderController@index');
	// Trade Name Module
	Route::get('tradename', 'TradenameController@index');
	Route::get('tradename-designs/{id}', 'DesignController@byTradename');
	Route::get('add-tradename', 'TradenameController@create');
	Route::post('insert-tradename', 'TradenameController@store');

	Route::get('/design/by-tradename-ajax/{tradename_id}', 'DesignController@byTradenameAjax');

	// Mill report, tradename_id, design_id
	Route::get('inventory/edit/{millreport_id}/{tradename_id}/{design_id}/{color}', 'InventoryController@edit');
	Route::get('inventory/printbarcode/{millreport_id}/{tradename_id}/{design_id}/{color}',
		'InventoryController@printBarcode');
	Route::post('millreport/destroy/{id}', 'MillController@destroy');

	// Delete all junk that is not required.

	// Route::get('auth/logout', 'Auth\LoginController@logout')->name('logout');

	Route::get('dashboard', 'DashboardController@index');
	
	Route::get('millreport', 'MillController@index');
	Route::post('millreport/store', 'MillController@store');
	Route::get('millreport/edit/{id}', 'MillController@edit');
	Route::get('millreport/create', 'MillController@create');

	Route::get('pendingmill', 'MillController@pendingmill');
	Route::get('millreport/addinventory/{id}', 'InventoryController@add');

	//change


	Route::post('inventory/store', 'InventoryController@store');
	Route::get('inventory', 'InventoryController@index');
	Route::post('inventory/update', 'InventoryController@update');

	Route::get('notifications', 'NotificationController@index');
///'AgentController@insertagent'

	

	// Designs
	Route::get('/designs', 'DesignController@index');
	Route::get('/add-design', 'DesignController@show');
	Route::get('/design/tradename/{id}', 'DesignController@designByTradeName');
	Route::post('/insert-design', 'DesignController@insert');
	Route::get('/design/alldesigns', 'DesignController@allDesignsAjax');
	Route::get('/design/all-design-numbers', 'DesignController@allDesignNumbersAjax');

	Route::get('order/searchOrder', 'OrderFormController@searchOrder');

	Route::get('order/chkpartialOrder/{id}', 'OrderFormController@chkpartialOrder');
	Route::get('order/allOrder', 'OrderFormController@allOrder');
	Route::get('order/pendingorder', 'OrderFormController@pendingorder');
	Route::get('order/requestedorder', 'OrderFormController@requestedorders');
	Route::get('order', 'OrderFormController@index');
	Route::get('/viewallclient', 'ClientController@index');
	Route::get('/insertclient', 'ClientController@create');
	Route::post('/addnewclient', 'ClientController@store');
	Route::get('client/edit/{id}', 'ClientController@edit');
	Route::get('client/delete/{id}', 'ClientController@destroy');
	Route::post('submiteditclient/{id}', 'ClientController@update');
	
	Route::get('client/placeorder/{id}', 'OrderFormController@create');
	Route::get('order/add-order-form-column', 'OrderFormController@addOrderFormColumn');
	Route::post('order/check-order', 'OrderFormController@checkOrder');
	Route::post('order/place-order', 'OrderFormController@processOrder');
	Route::post('order/partialProcess', 'OrderFormController@partialProcess');
	Route::get('order/confirmorder', 'OrderFormController@confirmorders');
	//edite
	Route::get('order/packaging', function(){
		return redirect('/admin/order/confirmorder');
	});

	//'OrderFormController@confirmorders'
	Route::get('order/packorder/{id}', 'PackingController@packOrder');
	Route::get('order/orderpacked/{id}', 'PackagingController@orderPacked');

});

Auth::routes();


//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','FrontendController@index');
