-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 20, 2018 at 03:57 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.1.20-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kanhaexim-new`
--

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` bigint(20) NOT NULL,
  `agent_name` varchar(255) DEFAULT NULL,
  `firmname` varchar(255) DEFAULT NULL,
  `officeaddress` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `mobileno` bigint(255) DEFAULT NULL,
  `officeno` bigint(255) DEFAULT NULL,
  `landlineno` bigint(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gstno` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`id`, `agent_name`, `firmname`, `officeaddress`, `city`, `state`, `country`, `mobileno`, `officeno`, `landlineno`, `email`, `gstno`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'geetaa', 'patela', 'Ahmedabad', 'Ahmedabad', 'gujarati', 'india', 1234567890, 123, 1234567890, 'demo@demo.com', '20', '2018-10-18 07:03:37', '2018-10-18 11:26:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firmname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firmaddress` text COLLATE utf8mb4_unicode_ci,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `officeaddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pcname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PhoneNo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `officeno` int(255) DEFAULT NULL,
  `landlineno` int(255) DEFAULT NULL,
  `GstNumber` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_id` int(255) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `Name`, `firmname`, `firmaddress`, `city`, `state`, `country`, `officeaddress`, `pcname`, `email`, `PhoneNo`, `officeno`, `landlineno`, `GstNumber`, `agent_name`, `agent_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(8, 'dhaval', 'keval', 'B-507, Titanium City Center Near, Anand Nagar', 'AHMEDABAD', 'gujarat', 'india', 'Prahalad nagar', 'Ravi Solanki', 'new@gmail.com', '9974765682', NULL, NULL, '10', 'Admin', 0, NULL, '2018-10-02 01:01:36', '2018-10-02 01:01:36'),
(12, 'dfadf', 'patel', 'dasfds', 'ahmedabad', 'gujarati', 'dfdasf', 'dgt', 'fdsafdas', 'geeta@gmail.com', '9974765682', NULL, NULL, '443', 'Admin', 0, NULL, '2018-10-18 12:03:29', '2018-10-18 12:03:29'),
(13, 'dfadf', 'patel', 'dasfds', 'adffg', 'gujarat', 'india', 'sdafa', 'fdsafdas', 'amazon@angloinfo.com', '1234567890', NULL, NULL, '123', 'Agent', 2, NULL, '2018-10-18 12:34:36', '2018-10-18 12:34:36'),
(15, 'trteyyyyyyyyy', 'tt', 'tttttt', 'tt', 'tt', 'tt', 'tt', 'tt', 'tt@tt.com', '4545', 45435435, 2147483647, '4545', 'Agent', 2, NULL, '2018-10-18 12:36:04', '2018-10-20 04:54:48');

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mr_id` int(255) NOT NULL,
  `sr_no` int(255) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DesignNo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colour` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TradeName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Confirm` int(11) DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `mr_id`, `sr_no`, `image`, `DesignNo`, `colour`, `Barcode`, `TradeName`, `Quantity`, `Confirm`, `available`, `deleted_at`, `created_at`, `updated_at`) VALUES
(21, 15, 1, 'images/1538572971.jpg', '123', 'red', 'AB2018RE123/21', 'abc', 100, NULL, 1, NULL, '2018-10-03 07:52:51', '2018-10-20 07:20:18'),
(22, 15, 2, 'images/1538572971.jpg', '123', 'red', 'AB2018RE123/22', 'abc', 200, NULL, 1, NULL, '2018-10-03 07:52:51', '2018-10-20 07:20:18'),
(23, 15, 3, 'images/1538572971.jpg', '123', 'red', 'AB2018RE123/23', 'abc', 300, NULL, 1, NULL, '2018-10-03 07:52:52', '2018-10-20 07:20:18'),
(24, 15, 4, 'images/1538572971.jpg', '123', 'green', 'AB2018GR123/24', 'abc', 100, NULL, 1, NULL, '2018-10-03 07:52:52', '2018-10-20 07:20:18'),
(25, 15, 5, 'images/1538572971.jpg', '123', 'white', 'AB2018WH123/25', 'abc', 200, NULL, 1, NULL, '2018-10-03 07:52:52', '2018-10-20 07:20:18'),
(26, 15, 6, 'images/1538572971.jpg', '123', 'keval', 'AB2018KE123/26', 'abc', 3000, NULL, 1, NULL, '2018-10-03 07:52:52', '2018-10-20 07:20:18'),
(28, 16, NULL, 'images/1538586406.jpg', '1231', 'red', 'NE2018RE1231/28', 'new', 10, NULL, 1, NULL, '2018-10-03 11:36:46', '2018-10-03 11:36:46'),
(39, 18, 2065406374, 'images/1540019287.png', '43211234', 'red', 'SP2018RE43211234/39', 'Spark', 900, NULL, 1, NULL, '2018-10-20 07:08:07', '2018-10-20 07:08:07');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `millreports`
--

CREATE TABLE `millreports` (
  `id` int(255) NOT NULL,
  `lotno` text NOT NULL,
  `quality` varchar(255) NOT NULL,
  `tradename` varchar(255) NOT NULL,
  `width` double(8,2) NOT NULL,
  `weight` double(8,2) NOT NULL,
  `pieceno` int(255) NOT NULL,
  `total_meters` int(255) NOT NULL,
  `orignal_meters` int(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `millreports`
--

INSERT INTO `millreports` (`id`, `lotno`, `quality`, `tradename`, `width`, `weight`, `pieceno`, `total_meters`, `orignal_meters`, `remarks`, `created_at`, `updated_at`, `deleted_at`) VALUES
(15, '["10"]', '1000', 'abc', 100.00, 10.00, 10, 50000, 50000, '10', '2018-10-01 23:34:38', '2018-10-18 09:09:37', NULL),
(16, '["20"]', '20', 'new', 10.00, 10.00, 10, 50000, 50000, '10', '2018-10-01 23:45:31', '2018-10-02 01:21:26', NULL),
(17, '["10"]', '1000', '10', 1.00, 10.00, 1, 10000, 10000, '1', '2018-10-02 00:49:15', '2018-10-18 11:30:03', NULL),
(18, '["555","666"]', '1000ugfgu', 'Spark', 52.00, 452.00, 15, 1000, 1000, NULL, '2018-10-17 09:18:32', '2018-10-17 09:18:32', NULL),
(24, '["10"]', '10', 'Spark', 100.00, 100.00, 1, 100, 100, NULL, '2018-10-20 14:57:43', '2018-10-20 14:57:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `notifiable_id` varchar(255) DEFAULT NULL,
  `notifiable_type` varchar(255) DEFAULT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `read_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_id`, `notifiable_type`, `heading`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
(12, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-02 05:04:38', '2018-10-02 05:04:38', '2018-10-02 05:04:38'),
(13, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Benn Added In The Mill.', '2018-10-02 05:07:02', '2018-10-02 05:07:02', '2018-10-02 05:07:02'),
(14, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-02 05:15:31', '2018-10-02 05:15:31', '2018-10-02 05:15:31'),
(15, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-02 06:19:15', '2018-10-02 06:19:15', '2018-10-02 06:19:15'),
(16, NULL, NULL, NULL, 'Client Created', 'Client dhaval Has Been Created By Admin', '2018-10-02 06:31:36', '2018-10-02 06:31:36', '2018-10-02 06:31:36'),
(17, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-02 06:51:04', '2018-10-02 06:51:04', '2018-10-02 06:51:04'),
(18, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-02 06:51:26', '2018-10-02 06:51:26', '2018-10-02 06:51:26'),
(19, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Been Added In The Mill.', '2018-10-02 06:52:40', '2018-10-02 06:52:40', '2018-10-02 06:52:40'),
(20, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Been Added In The Mill.', '2018-10-02 12:48:21', '2018-10-02 12:48:21', '2018-10-02 12:48:21'),
(21, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-02 13:25:24', '2018-10-02 13:25:24', '2018-10-02 13:25:24'),
(22, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Been Added In The Mill.', '2018-10-02 13:26:16', '2018-10-02 13:26:16', '2018-10-02 13:26:16'),
(23, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-03 11:21:16', '2018-10-03 11:21:16', '2018-10-03 11:21:16'),
(24, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Been Added In The Mill.', '2018-10-03 12:07:27', '2018-10-03 12:07:27', '2018-10-03 12:07:27'),
(25, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 12:08:54', '2018-10-03 12:08:54', '2018-10-03 12:08:54'),
(26, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 12:09:14', '2018-10-03 12:09:14', '2018-10-03 12:09:14'),
(27, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 12:19:43', '2018-10-03 12:19:43', '2018-10-03 12:19:43'),
(28, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 12:21:01', '2018-10-03 12:21:01', '2018-10-03 12:21:01'),
(29, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 12:33:29', '2018-10-03 12:33:29', '2018-10-03 12:33:29'),
(30, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 12:34:58', '2018-10-03 12:34:58', '2018-10-03 12:34:58'),
(31, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 12:38:52', '2018-10-03 12:38:52', '2018-10-03 12:38:52'),
(32, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 12:39:47', '2018-10-03 12:39:47', '2018-10-03 12:39:47'),
(33, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 13:04:16', '2018-10-03 13:04:16', '2018-10-03 13:04:16'),
(34, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 13:05:56', '2018-10-03 13:05:56', '2018-10-03 13:05:56'),
(35, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 13:07:22', '2018-10-03 13:07:22', '2018-10-03 13:07:22'),
(36, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Been Added In The Mill.', '2018-10-03 13:22:52', '2018-10-03 13:22:52', '2018-10-03 13:22:52'),
(37, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 13:23:09', '2018-10-03 13:23:09', '2018-10-03 13:23:09'),
(38, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 16:53:27', '2018-10-03 16:53:27', '2018-10-03 16:53:27'),
(39, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 16:54:29', '2018-10-03 16:54:29', '2018-10-03 16:54:29'),
(40, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 16:54:51', '2018-10-03 16:54:51', '2018-10-03 16:54:51'),
(41, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 17:01:01', '2018-10-03 17:01:01', '2018-10-03 17:01:01'),
(42, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 17:01:14', '2018-10-03 17:01:14', '2018-10-03 17:01:14'),
(43, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 17:03:03', '2018-10-03 17:03:03', '2018-10-03 17:03:03'),
(44, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 17:03:15', '2018-10-03 17:03:15', '2018-10-03 17:03:15'),
(45, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Been Added In The Mill.', '2018-10-03 17:06:46', '2018-10-03 17:06:46', '2018-10-03 17:06:46'),
(46, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Been Added In The Mill.', '2018-10-03 17:08:19', '2018-10-03 17:08:19', '2018-10-03 17:08:19'),
(47, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-03 17:57:34', '2018-10-03 17:57:34', '2018-10-03 17:57:34'),
(48, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-17 09:18:32', '2018-10-17 09:18:32', '2018-10-17 09:18:32'),
(49, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Been Added In The Mill.', '2018-10-17 09:24:00', '2018-10-17 09:24:00', '2018-10-17 09:24:00'),
(50, NULL, NULL, NULL, 'Client Created', 'Client Abc Has Been Created By Admin', '2018-10-17 12:57:58', '2018-10-17 12:57:58', '2018-10-17 12:57:58'),
(51, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-17 13:00:34', '2018-10-17 13:00:34', '2018-10-17 13:00:34'),
(52, NULL, NULL, NULL, 'New Agent Added', 'You Have Added New Agent', '2018-10-18 05:54:19', '2018-10-18 05:54:19', '2018-10-18 05:54:19'),
(53, NULL, NULL, NULL, 'New Agent Added', 'You Have Added New Agent', '2018-10-18 06:03:51', '2018-10-18 06:03:51', '2018-10-18 06:03:51'),
(54, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 06:05:16', '2018-10-18 06:05:16', '2018-10-18 06:05:16'),
(55, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 06:05:34', '2018-10-18 06:05:34', '2018-10-18 06:05:34'),
(56, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-18 06:06:57', '2018-10-18 06:06:57', '2018-10-18 06:06:57'),
(57, NULL, NULL, NULL, 'New Agent Added', 'You Have Added New Agent', '2018-10-18 07:03:37', '2018-10-18 07:03:37', '2018-10-18 07:03:37'),
(58, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 07:09:10', '2018-10-18 07:09:10', '2018-10-18 07:09:10'),
(59, NULL, NULL, NULL, 'Agent Detail Updated', 'You Have Updated 3 No Agent Datail', '2018-10-18 07:12:43', '2018-10-18 07:12:43', '2018-10-18 07:12:43'),
(60, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 07:13:17', '2018-10-18 07:13:17', '2018-10-18 07:13:17'),
(61, NULL, NULL, NULL, 'Client Created', 'Client dfadf Has Been Created By Admin', '2018-10-18 07:47:59', '2018-10-18 07:47:59', '2018-10-18 07:47:59'),
(62, NULL, NULL, NULL, 'Agent Detail Updated', 'You Have Updated 3 No Agent Datail', '2018-10-18 07:49:30', '2018-10-18 07:49:30', '2018-10-18 07:49:30'),
(63, NULL, NULL, NULL, 'Agent Detail Updated', 'You Have Updated 3 No Agent Datail', '2018-10-18 07:50:17', '2018-10-18 07:50:17', '2018-10-18 07:50:17'),
(64, NULL, NULL, NULL, 'Agent Detail Updated', 'You Have Updated 3 No Agent Datail', '2018-10-18 07:57:14', '2018-10-18 07:57:14', '2018-10-18 07:57:14'),
(65, NULL, NULL, NULL, 'Agent Detail Updated', 'You Have Updated 3 No Agent Datail', '2018-10-18 07:57:50', '2018-10-18 07:57:50', '2018-10-18 07:57:50'),
(66, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:02:16', '2018-10-18 08:02:16', '2018-10-18 08:02:16'),
(67, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:08:45', '2018-10-18 08:08:45', '2018-10-18 08:08:45'),
(68, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:14:30', '2018-10-18 08:14:30', '2018-10-18 08:14:30'),
(69, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:16:17', '2018-10-18 08:16:17', '2018-10-18 08:16:17'),
(70, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:17:53', '2018-10-18 08:17:53', '2018-10-18 08:17:53'),
(71, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:26:06', '2018-10-18 08:26:06', '2018-10-18 08:26:06'),
(72, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:28:29', '2018-10-18 08:28:29', '2018-10-18 08:28:29'),
(73, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:28:46', '2018-10-18 08:28:46', '2018-10-18 08:28:46'),
(74, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:29:10', '2018-10-18 08:29:10', '2018-10-18 08:29:10'),
(75, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:33:10', '2018-10-18 08:33:10', '2018-10-18 08:33:10'),
(76, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:33:18', '2018-10-18 08:33:18', '2018-10-18 08:33:18'),
(77, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:33:30', '2018-10-18 08:33:30', '2018-10-18 08:33:30'),
(78, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 08:34:39', '2018-10-18 08:34:39', '2018-10-18 08:34:39'),
(79, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 09:09:37', '2018-10-18 09:09:37', '2018-10-18 09:09:37'),
(80, NULL, NULL, NULL, 'Client Created', 'Client dfadf Has Been Created By Admin', '2018-10-18 10:26:17', '2018-10-18 10:26:17', '2018-10-18 10:26:17'),
(81, NULL, NULL, NULL, 'Agent Detail Updated', 'You Have Updated 3 No Agent Datail', '2018-10-18 10:55:07', '2018-10-18 10:55:07', '2018-10-18 10:55:07'),
(82, NULL, NULL, NULL, 'Agent Detail Updated', 'You Have Updated 3 No Agent Datail', '2018-10-18 11:26:09', '2018-10-18 11:26:09', '2018-10-18 11:26:09'),
(83, NULL, NULL, NULL, 'New Agent Added', 'You Have Added New Agent', '2018-10-18 11:27:01', '2018-10-18 11:27:01', '2018-10-18 11:27:01'),
(84, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 11:30:03', '2018-10-18 11:30:03', '2018-10-18 11:30:03'),
(85, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-18 11:31:32', '2018-10-18 11:31:32', '2018-10-18 11:31:32'),
(86, NULL, NULL, NULL, 'Client Created', 'Client dfadf Has Been Created By Admin', '2018-10-18 12:03:29', '2018-10-18 12:03:29', '2018-10-18 12:03:29'),
(87, NULL, NULL, NULL, 'Client Created', 'Client dfadf Has Been Created By Agent', '2018-10-18 12:34:36', '2018-10-18 12:34:36', '2018-10-18 12:34:36'),
(88, NULL, NULL, NULL, 'Client Created', 'Client df Has Been Created By Agent', '2018-10-18 12:35:08', '2018-10-18 12:35:08', '2018-10-18 12:35:08'),
(89, NULL, NULL, NULL, '14 No Client Updated', '14 No Client Detail Has Been Updated', '2018-10-18 12:35:24', '2018-10-18 12:35:24', '2018-10-18 12:35:24'),
(90, NULL, NULL, NULL, 'Client Created', 'Client trtey Has Been Created By Agent', '2018-10-18 12:36:04', '2018-10-18 12:36:04', '2018-10-18 12:36:04'),
(91, NULL, NULL, NULL, 'Mill Updated', 'Your Mill Has Been Updated', '2018-10-18 16:55:55', '2018-10-18 16:55:55', '2018-10-18 16:55:55'),
(92, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-19 11:07:06', '2018-10-19 11:07:06', '2018-10-19 11:07:06'),
(93, NULL, NULL, NULL, '15 No Client Updated', '15 No Client Detail Has Been Updated', '2018-10-20 04:54:48', '2018-10-20 04:54:48', '2018-10-20 04:54:48'),
(94, NULL, NULL, NULL, 'New Agent Added', 'You Have Added New Agent', '2018-10-20 04:56:08', '2018-10-20 04:56:08', '2018-10-20 04:56:08'),
(95, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-20 06:16:10', '2018-10-20 06:16:10', '2018-10-20 06:16:10'),
(96, NULL, NULL, NULL, 'Inventory Created', 'Your Inventory Has Been Added In The Mill.', '2018-10-20 07:08:07', '2018-10-20 07:08:07', '2018-10-20 07:08:07'),
(97, NULL, NULL, NULL, 'Inventory Updated', 'Your Inventory Has Been Updated.', '2018-10-20 07:20:18', '2018-10-20 07:20:18', '2018-10-20 07:20:18'),
(98, NULL, NULL, NULL, 'Your Mill Is Created', 'Mill Created', '2018-10-20 14:57:43', '2018-10-20 14:57:43', '2018-10-20 14:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `Date` date NOT NULL,
  `Station` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `T_p` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Remark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Terms_Status` tinyint(2) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `ClientId` int(255) NOT NULL,
  `OrderMakerType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Confirm` tinyint(1) DEFAULT NULL,
  `Requested` tinyint(1) DEFAULT NULL,
  `Packed` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `Date`, `Station`, `T_p`, `Remark`, `Terms_Status`, `payment_method`, `UserId`, `ClientId`, `OrderMakerType`, `Confirm`, `Requested`, `Packed`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2018-10-02', 'amd', 'surat', '10', 1, '10 - 15 Days-> 3% Net', 1, 8, 'admin', NULL, 1, NULL, NULL, '2018-10-19 11:05:03', '2018-10-19 11:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `TradeName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DesignNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Quantity` double(8,2) NOT NULL,
  `Price` double(8,2) NOT NULL,
  `Size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `TradeName`, `DesignNo`, `Quantity`, `Price`, `Size`, `Order_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(57, '12', '1010', 11.00, 1.00, NULL, 58, '2018-10-01 03:50:10', '2018-10-01 03:50:10', NULL),
(58, '10', '1010', 10.00, 10.00, NULL, 59, '2018-10-01 03:51:49', '2018-10-01 03:51:49', NULL),
(59, '10', '1', 4.00, 4.00, NULL, 60, '2018-10-01 04:12:03', '2018-10-01 04:12:03', NULL),
(60, '10', '1', 4.00, 4.00, NULL, 61, '2018-10-01 04:12:44', '2018-10-01 04:12:44', NULL),
(61, '10', '20', 10.00, 10.00, NULL, 61, '2018-10-01 04:12:44', '2018-10-01 04:12:44', NULL),
(62, '10', '1010', 1.00, 11.00, NULL, 62, '2018-10-01 05:45:49', '2018-10-01 05:45:49', NULL),
(63, 'new', '1231', 1.00, 1.00, NULL, 1, '2018-10-19 11:05:03', '2018-10-19 11:05:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@admin.com', '$2y$10$RjAxfbC7JmcKkOhR.fC/ke6rJ3QzgdTaTVpB64fDr6G.z0LFjmvH.', '2018-09-26 05:08:51'),
('keval.coderadobe@gmail.com', '$2y$10$KXMEDXRAM1NEU61CKYM/zuabL0iirq5DhrG6zVzyxJCBms9t3dRJq', '2018-09-26 05:53:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `agent_id`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `type`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$blgZRWdZbAqvTT1v8TiX1OcPcxODpzaaMt9ja2.qmIN9sjyJGpZni', NULL, 'yjGuxILFSil2uJV03DgDTx6jquPwGCqW6QVsUUhYa3A0PAGOhHBoDGmtjm6i', NULL, NULL, NULL, 'admin'),
(2, 'Agent', 'agent@agent.com', '$2y$10$5F7Vejylh077F05QDWwg4eKpXt//Lvw.UEhu6xD.oi72eRp9/00V.', NULL, 'g5cIqyYmMghIUchb6rwkDcjIV1Yw2YvISqEkfbyqZWONcC8L9FaVCefxv23r', NULL, NULL, NULL, 'agent'),
(3, 'geetaa', 'demo@demo.com', '$2y$10$i2pPYB4mF.ioUPVdxs9D4eYa3oy8At31HNynblwTUz8HfDWnEnlkO', NULL, '4yi8Pj4o1Mx9Kwn8vv2xGkzxPMl2fRDNyKzXaNgJM8iTA24G9GVn368FkvLR', NULL, '2018-10-18 11:26:09', NULL, 'manager'),
(13, 'ravii', 'ravi@ravi.com', '$2y$10$C/1pBET7/wARIEIbgrvXx.1L2uBXgSFGK/DxNu1Wf9vbA0kTOSK/C', NULL, 'o5Do7F9qPbC5xAttZPF5jV3YEE2aPrbuF9lXCZW6ntXLd7DP6k4qBX05WEMw', '2018-10-18 11:19:55', '2018-10-20 04:54:22', NULL, 'agent'),
(15, 'manager', 'manager@manager.com', '$2y$10$ZaPHhVBVexgpAM52gs7TgOW6dDkMnZGaX/5p5DuRKZLe.Aq3Eoquy', NULL, 'gWYQHJHHhb49B42TlJZAjl7PcMJDmipFX3XkJw9V0k2YXnFCzNrW47XNMWTA', '2018-10-18 12:44:34', '2018-10-18 12:44:34', NULL, 'manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `millreports`
--
ALTER TABLE `millreports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `millreports`
--
ALTER TABLE `millreports`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
