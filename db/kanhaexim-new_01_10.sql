-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 01, 2018 at 07:07 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kanhaexim-new`
--

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` bigint(20) NOT NULL,
  `agent_name` varchar(255) DEFAULT NULL,
  `firmname` varchar(255) DEFAULT NULL,
  `officeaddress` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `mobileno` bigint(255) DEFAULT NULL,
  `officeno` bigint(255) DEFAULT NULL,
  `landlineno` bigint(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gstno` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firmname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firmaddress` text COLLATE utf8mb4_unicode_ci,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `officeaddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pcname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PhoneNo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `officeno` int(255) DEFAULT NULL,
  `landlineno` int(255) DEFAULT NULL,
  `GstNumber` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `Name`, `firmname`, `firmaddress`, `city`, `state`, `country`, `officeaddress`, `pcname`, `email`, `PhoneNo`, `officeno`, `landlineno`, `GstNumber`, `agent_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, '1230', '132', '1321', '132', '123', '132', '132', 'k', 'dfs@gm.com', '9974765682', NULL, NULL, 'afsds', 'Agent', NULL, '2018-09-18 05:45:59', '2018-10-01 02:21:43'),
(6, 'sdgad', '3210', 'B-507, Titanium City Center Near, Anand Nagar', 'AHMEDABAD', 'gujarat', 'india', 'Prahalad nagar\"\"\"', 'Ravi Solanki', 'keval.coderadobe@gmail.com', '9974765682', NULL, NULL, '123', 'Agent', NULL, '2018-09-18 06:19:30', '2018-09-21 04:56:20'),
(7, '10', '1', '1', '1', '11', '1', '1\"\"\"\"\"', '11', 'ke@g.com', '1234567890', NULL, NULL, '000', 'Agent', NULL, '2018-09-28 04:08:32', '2018-09-28 04:08:32');

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mr_id` int(255) NOT NULL,
  `sr_no` int(255) DEFAULT NULL,
  `DesignNo` double(8,2) NOT NULL,
  `colour` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TradeName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Confirm` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `mr_id`, `sr_no`, `DesignNo`, `colour`, `Barcode`, `TradeName`, `Quantity`, `Confirm`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 8, 0, 1.00, '', '', '', NULL, NULL, NULL, NULL, NULL),
(4, 12, 1, 1010.00, '2', NULL, '10', 3, NULL, NULL, '2018-09-21 03:15:15', '2018-09-21 03:15:15'),
(5, 14, NULL, 20.00, '20', NULL, '12', 20, NULL, NULL, '2018-09-21 04:05:59', '2018-09-21 04:05:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `millreports`
--

CREATE TABLE `millreports` (
  `id` int(255) NOT NULL,
  `lotno` text NOT NULL,
  `quality` varchar(255) NOT NULL,
  `tradename` varchar(255) NOT NULL,
  `width` double(8,2) NOT NULL,
  `weight` double(8,2) NOT NULL,
  `pieceno` int(255) NOT NULL,
  `total_meters` int(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `millreports`
--

INSERT INTO `millreports` (`id`, `lotno`, `quality`, `tradename`, `width`, `weight`, `pieceno`, `total_meters`, `remarks`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, '[\"111\",\"2222\"]', '10', '10', 1.00, 10.00, 10, 10, '10', '2018-09-20 04:26:33', '2018-09-21 04:26:52', NULL),
(12, '[\"1000\"]', '010', '10', 0.00, 0.00, 0, 20, 'no', '2018-09-20 04:56:38', '2018-09-21 04:28:21', NULL),
(14, '[\"10\",\"20\"]', 'keval', '12', 12.00, 12.00, 12, 12, '12', '2018-09-21 04:02:11', '2018-09-21 04:49:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `notifiable_id` varchar(255) DEFAULT NULL,
  `notifiable_type` varchar(255) DEFAULT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `read_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_id`, `notifiable_type`, `heading`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, 'Client Created', 'Client keval123131 Has Been Created ByAgent', '2018-09-28 09:56:05', '2018-09-28 09:56:05', '2018-09-28 09:56:05'),
(2, NULL, NULL, NULL, '10 No Client Updated', '10 No Client Detail Has Been Updated', '2018-09-28 10:05:35', '2018-09-28 10:05:35', '2018-09-28 10:05:35'),
(3, NULL, NULL, NULL, '10 No Client Updated', '10 No Client Detail Has Been Updated', '2018-09-28 10:07:51', '2018-09-28 10:07:51', '2018-09-28 10:07:51'),
(4, NULL, NULL, NULL, '10 No Client Updated', '10 No Client Detail Has Been Updated', '2018-09-28 10:08:11', '2018-09-28 10:08:11', '2018-09-28 10:08:11'),
(5, NULL, NULL, NULL, '10 No Client Updated', '10 No Client Detail Has Been Updated', '2018-09-28 10:08:35', '2018-09-28 10:08:35', '2018-09-28 10:08:35'),
(6, NULL, NULL, NULL, '10 No Client Updated', '10 No Client Detail Has Been Updated', '2018-09-28 10:09:49', '2018-09-28 10:09:49', '2018-09-28 10:09:49'),
(7, NULL, NULL, NULL, '10 No Client Updated', '10 No Client Detail Has Been Updated', '2018-09-28 10:09:58', '2018-09-28 10:09:58', '2018-09-28 10:09:58'),
(8, NULL, NULL, NULL, '10 No Client Updated', '10 No Client Detail Has Been Updated', '2018-09-28 10:10:56', '2018-09-28 10:10:56', '2018-09-28 10:10:56'),
(9, NULL, NULL, NULL, '10 No Client Updated', '10 No Client Detail Has Been Updated', '2018-09-28 10:11:01', '2018-09-28 10:11:01', '2018-09-28 10:11:01'),
(10, NULL, NULL, NULL, '10 No Client Updated', '10 No Client Detail Has Been Updated', '2018-09-28 10:11:13', '2018-09-28 10:11:13', '2018-09-28 10:11:13'),
(11, NULL, NULL, NULL, '2 No Client Updated', '2 No Client Detail Has Been Updated', '2018-10-01 07:51:43', '2018-10-01 07:51:43', '2018-10-01 07:51:43');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `Date` date NOT NULL,
  `Station` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `T_p` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Remark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Terms_Status` tinyint(2) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `ClientId` int(255) NOT NULL,
  `OrderMakerType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Confirm` tinyint(1) DEFAULT NULL,
  `Requested` tinyint(1) DEFAULT NULL,
  `Packed` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `Date`, `Station`, `T_p`, `Remark`, `Terms_Status`, `payment_method`, `UserId`, `ClientId`, `OrderMakerType`, `Confirm`, `Requested`, `Packed`, `deleted_at`, `created_at`, `updated_at`) VALUES
(58, '2019-02-01', '2', '10', '10', 1, '60 - 70-> Net', 2, 6, 'agent', NULL, NULL, NULL, NULL, '2018-10-01 03:50:10', '2018-10-01 03:50:10'),
(59, '2018-10-17', '10', '10', '10', 1, '60 - 70-> Net', 2, 6, 'agent', NULL, NULL, NULL, NULL, '2018-10-01 03:51:49', '2018-10-01 03:51:49'),
(60, '2018-02-01', '44', '4', '10', 1, '30-> 20% cash discount', 2, 10, 'agent', NULL, NULL, NULL, NULL, '2018-10-01 04:12:03', '2018-10-01 04:12:03'),
(61, '2018-02-01', '44', '4', '10', 1, 'Other-> 18% Interest', 2, 10, 'agent', 1, NULL, NULL, NULL, '2018-10-01 04:12:44', '2018-10-01 04:12:44'),
(62, '2018-01-01', '1', '1', '10', 1, '10 - 15-> 3% Net', 1, 7, 'admin', NULL, NULL, NULL, NULL, '2018-10-01 05:45:49', '2018-10-01 05:45:49');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `TradeName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DesignNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Quantity` double(8,2) NOT NULL,
  `Price` double(8,2) NOT NULL,
  `Size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `TradeName`, `DesignNo`, `Quantity`, `Price`, `Size`, `Order_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(57, '12', '1010', 11.00, 1.00, NULL, 58, '2018-10-01 03:50:10', '2018-10-01 03:50:10', NULL),
(58, '10', '1010', 10.00, 10.00, NULL, 59, '2018-10-01 03:51:49', '2018-10-01 03:51:49', NULL),
(59, '10', '1', 4.00, 4.00, NULL, 60, '2018-10-01 04:12:03', '2018-10-01 04:12:03', NULL),
(60, '10', '1', 4.00, 4.00, NULL, 61, '2018-10-01 04:12:44', '2018-10-01 04:12:44', NULL),
(61, '10', '20', 10.00, 10.00, NULL, 61, '2018-10-01 04:12:44', '2018-10-01 04:12:44', NULL),
(62, '10', '1010', 1.00, 11.00, NULL, 62, '2018-10-01 05:45:49', '2018-10-01 05:45:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@admin.com', '$2y$10$RjAxfbC7JmcKkOhR.fC/ke6rJ3QzgdTaTVpB64fDr6G.z0LFjmvH.', '2018-09-26 05:08:51'),
('keval.coderadobe@gmail.com', '$2y$10$KXMEDXRAM1NEU61CKYM/zuabL0iirq5DhrG6zVzyxJCBms9t3dRJq', '2018-09-26 05:53:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `agent_id`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `type`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$blgZRWdZbAqvTT1v8TiX1OcPcxODpzaaMt9ja2.qmIN9sjyJGpZni', NULL, '2akr6IIm6ht4eDjkuKNldKTW9vwQhLgdSSfyvMh7zSVSnEZa8hJt3ffxOedC', NULL, NULL, NULL, 'admin'),
(2, 'Agent', 'agent@agent.com', '$2y$10$5F7Vejylh077F05QDWwg4eKpXt//Lvw.UEhu6xD.oi72eRp9/00V.', NULL, 'e2UhjmReM8cRV8Xg46VQRhPPRGRNQShlvqNcqQJlFGS0OH8mfYkcA9VrR3es', NULL, NULL, NULL, 'agent'),
(3, 'Manager', 'manager@manager.com', '$2y$10$i2pPYB4mF.ioUPVdxs9D4eYa3oy8At31HNynblwTUz8HfDWnEnlkO', NULL, 'l5JkBcep3mHMz7hILnwemnOWaQ80JsNAUogy2uO7BGm9f5Zs4Q1VctjiZEeE', NULL, NULL, NULL, 'manager'),
(4, 'keval', 'keval.coderadobe@gmail.com', '$2y$10$WRsDX8x7M9zzNSF4De7//uc7PQRfijIB5RMGMqwBfnl83REQW5I3m', NULL, 'RbLSDOwOqoZgMkkxokGjrqTgSziFWAUta0VEb25KNOxJ8ZV09rIfTj4N195K', NULL, '2018-09-26 05:48:36', NULL, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `millreports`
--
ALTER TABLE `millreports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `millreports`
--
ALTER TABLE `millreports`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
