<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller {
	/*
		    |--------------------------------------------------------------------------
		    | Login Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller handles authenticating users for the application and
		    | redirecting them to your home screen. The controller uses a trait
		    | to conveniently provide its functionality to your applications.
		    |
	*/

	use AuthenticatesUsers;

	public function __construct() {
		$this->middleware('guest')->except('logout');
	}
	protected $redirectTo = '/';

	protected function authenticated(Request $request) {
		if (isset($_POST["remember"])) {
			if (!empty($_POST["remember"])) {
				setcookie("email", $_POST["email"], time() + (10 * 365 * 24 * 60 * 60));
				setcookie("password", $_POST["password"], time() + (10 * 365 * 24 * 60 * 60));
			} else {
				setcookie("email", $_POST["email"], time() + (10 * 365 * 24 * 60 * 60));
				setcookie("password", $_POST["password"], time() + (10 * 365 * 24 * 60 * 60));
			}
		}
		
		if (isset(Auth::user()->type)) {

			switch (Auth::user()->type) {
			case 'admin':
				return redirect('/admin/dashboard');
				break;
			case 'manager':
				return redirect('/admin/dashboard');
				break;
			case 'agent':
				return redirect('/agent/clients');
				break;
			}
		} else {
			return view('login');
		}
	}

}
