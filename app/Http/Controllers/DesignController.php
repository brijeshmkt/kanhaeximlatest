<?php

namespace App\Http\Controllers;
use App\Model\Designs;
use App\Model\Tradename;
use Illuminate\Http\Request;
use DB;
class DesignController extends Controller {

	public function index() {
		$alldesigns = Designs::orderBy('id', 'DESC')
			->paginate(10);
		return view('admin.design.index', compact('alldesigns'));
	}

	public function byTradenameAjax($tradeName_id) {
		$id = $tradeName_id;
		$greatDesigns = Designs::where('tradename_id', $id)->get();
		$data = ['greatDesigns' => $greatDesigns];

		return $greatDesigns;
	}

	public function allDesignsAjax() {
		return Designs::all();
	}

	public function allDesignNumbersAjax() {
		return Designs::all()->pluck('number');
	}

	public function byTradename($tradeName_id) {
		$id = $tradeName_id;
		$greatDesigns = Designs::where('tradename_id', $id)->get();
		$data = ['greatDesigns' => $greatDesigns];

		return view('admin.design.tradename', $data);
	}
	
	public function show() {
		$tradenames = Tradename::all();
		return view('admin.design.insertdesign', compact('tradenames'));
	}
	public function insert(Request $request) {
		$this->validate($request, [
			'dnum' => 'required|unique:designs,number',
			'dimage' => 'required',
			'tradename' => 'required',
		],
			[
				'dnum.required' => 'Design number is required.',
				'dnum.unique' => 'Design number should be unique.',
				'tradename.unique' => 'Trade name is required.',
			]

		);
		$image = time() . '.' . $request->dimage->getClientOriginalExtension();
		$request->dimage->move(public_path('design_images'), $image);
		$image = 'design_images/' . $image;

		$design = new Designs;
		$design->number = $request->input('dnum');
		$design->image = $image;
		$design->description = $request->input('ddes');
		$design->tradename_id = $request->tradename;
		$design->save();

		if(!$design->id) {
			return back()->with('message', 'Error in inserting the data');
		}

		return redirect('admin/designs')->with('message', 'Design Inserted');

	}
	public function search(Request $request) {
		$request->term;
		$design_nos = Designs::select('number', 'image')->where('number', 'like', '%' . $request->term . '%')->get();
		$val = [];
		foreach ($design_nos as $design_no) {
			$val[] = ['name' => $design_no["number"], 'value' => $design_no["number"], 'image' => $design_no["image"]];
		}
		// dd(print_r($val));
		return response()->json($val);
	}

public function searchDesign(Request $req){
		$trade_id = $req->value;
		$designs = Designs::select('id','number')->where('tradename_id',$trade_id)->get();
		$output='';
		foreach ($designs as $design) {
			$output .= '<option value="'.$design->id.'">'.$design->number.'</option>';
			}
		echo "$output";

}
}
