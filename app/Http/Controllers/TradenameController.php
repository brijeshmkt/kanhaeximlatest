<?php

namespace App\Http\Controllers;

use App\Model\Tradename;
use Illuminate\Http\Request;

class TradenameController extends Controller {
	
	public function index() {
		$tradenames = Tradename::all();
		$data = ['tradenames' => $tradenames];
		return view("admin.tradename.index", $data);
	}

	public function create() {
		return view("admin.tradename.form");
	}
	
	public function store(Request $request) {
		$tradename = new Tradename();
		$tradename->name = $request->name;
		$tradename->initial = $request->initial;
		$tradename->save();
		return redirect('admin/tradename');
	}

	public function searchajax(Request $request) {
		$tradenames = Tradename::where('tradename', 'like', '%' . $request->term . '%')->orwhere('design_id', '!=', NULL)->get();
		foreach ($tradenames as $trade_names) {
			$val[] = ['name' => "treadname", 'value' => $trade_names->tradename];
		}
		return response()->json($val);
	}
}
