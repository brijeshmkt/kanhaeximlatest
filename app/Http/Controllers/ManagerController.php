<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Model\Inventory;
use App\Model\Order;
use App\Model\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;

class ManagerController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		
		return view('manager.dashboard');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function barcodeCheck(Request $req,$orderid) {
		
		
		$barcode=$req->barcode;

		if ($barcode !== null) {
			$Inventory=Inventory::where('Barcode','=',$barcode)->first();
			if($Inventory){
			$Inventory_TradeName=$Inventory->TradeName;
			$Inventory_DesignNo=$Inventory->DesignNo;
			$All_Inventory=Inventory::where('TradeName','=',$Inventory_TradeName)->where('DesignNo','=',$Inventory_DesignNo)->get();
			$orderproduct=OrderProduct::where('Order_id','=',$orderid)->first(['TradeName','DesignNo']);
			$orderproduct_TradeName=$orderproduct->TradeName;
			$orderproduct_DesignNo=$orderproduct->DesignNo;
			if($Inventory_TradeName==$orderproduct_TradeName&&$Inventory_DesignNo==$orderproduct_DesignNo){
				return response()->json($Inventory);
			}
			else
			{
				return response()->json('0');
		
			}
		}
		} else {
			return back()->withErrors('Plase Check Barcode');	
		}

	}
	public function generateSlip($order_details) {
		$data = ['Hello Kanha Exim'];
		$pdf = PDF::loadView('manager.sample', $order_details);
		return $pdf->download('sample');
		// $pdf = App::make('dompdf.wrapper');
		// $pdf->loadHTML('<h1>Test</h1>');
		// return $pdf->stream();
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
	public function inventoryCheck($id) {
		$get = Inventory::where('id', '=', $id)->first();
		// dd($get);
		if ($get) {
			$designno = $get->DesignNo;
			$size = $get->Size;
			$values = Inventory::where('DesignNo', '=', $designno)->where('Size', '=', $size)->first();
			return response()->json($values);
		} else {
			return response()->json('0');
		}

	}
	public function packOrder(Request $request, $id) {
		dd($request->testtext);
	}
}
