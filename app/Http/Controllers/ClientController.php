<?php

namespace App\Http\Controllers;
use App\Model\Client;
use Auth;
use DB;
use Illuminate\Http\Request;

class ClientController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
		$clients = Client::orderBy('id', 'desc')->paginate(20);
		return view('admin.clients.allclient', compact('clients'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
		if (Auth::user()->type == 'admin') {
			return view('admin.clients.newclient');
		} elseif (Auth::user()->type == 'Agent') {
			return view('agent.clients.newclient');
		} else {
			return view('agent.clients.newclient');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request);
		$this->validate($request, [
			'clientname' => 'required',
			'firmname' => 'required',
			'firmaddress' => 'required',
			'city' => 'required',
			'state' => 'required',
			'country' => 'required',
			'officeaddress' => 'required',
			'pcname' => 'required',
			'phoneno' => 'required',
			'gstno' => 'required',
			'email' => 'required|unique:clients,email',
		]);
		// dd($request);
		$client = new Client;
		$client->client_name = $request->input('clientname');
		$client->user_id = AUTH::user()->id;
		$client->firm_name = $request->input('firmname');
		$client->firm_address = $request->input('firmaddress');
		$client->firm_city = $request->input('city');
		$client->firm_state = $request->input('state');
		$client->firm_country = $request->input('country');
		$client->office_address = $request->input('officeaddress');
		$client->contact_person_name = $request->input('pcname');
		$client->contact_person_phone_number = $request->input('phoneno');
		$client->contact_person_office_phone_number = $request->input('officeno');
		$client->contact_person_land_line_phone_number = $request->input('landlineno');
		$client->gst_number = $request->input('gstno');
		$client->email = $request->input('email');
		// $client->agent_name = Auth::user()->name;
		// $client->agent_id = Auth::user()->id;
		$client->save();

		$type = 'Client';
		$action = 'Added';
		$msg = $client ;
		addNotification($type, $action, $msg);

		return redirect('admin/viewallclient')->with('message', 'Create Client Successfully');
		// return back()->with('message', 'Create Client Successfully');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Request $request, $id) {

		if (Auth::user()->type == 'admin') {
			$clients = Client::where('id', $id)->get();
			return view('admin.clients.editclient', compact('clients'));
		} elseif (Auth::user()->type == 'Agent') {
			$clients = Client::where('id', $id)->get();
			return view('agent.clients.editclient', compact('clients'));
		} else {
			$clients = Client::where('id', $id)->get();
			return view('agent.clients.editclient', compact('clients'));
		}
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'clientname' => 'required',
			'firmname' => 'required',
			'firmaddress' => 'required',
			'city' => 'required',
			'state' => 'required',
			'country' => 'required',
			'officeaddress' => 'required',
			'pcname' => 'required',
			'phoneno' => 'required|integer',
			'gstno' => 'required',
			'email' => 'required',
		]);

		$users = Client::where('id', $id)->update([
			'client_name' => $request->input('clientname'),
			'firm_name' => $request->input('firmname'),
			'firm_address' => $request->input('firmaddress'),
			'firm_city' => $request->input('city'),
			'firm_state' => $request->input('state'),
			'firm_country' => $request->input('country'),
			'office_address' => $request->input('officeaddress'),
			'contact_person_name' => $request->input('pcname'),
			'email' => $request->input('email'),
			'contact_person_phone_number' => $request->input('phoneno'),
			'contact_person_office_phone_number' => $request->input('officeno'),
			'contact_person_land_line_phone_number' => $request->input('landlineno'),
			'gst_number' => $request->input('gstno'),
		]);

		$type = 'Client';
		$action = 'Updated';
		$msg = $users ;
		addNotification($type, $action, $msg);

	
			return redirect('admin/viewallclient')->with('message', 'Profile updated!');
		}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
		$client = Client::find($id);
		Client::where('id', $id)->delete();

		$type = 'Client';
		$action = 'deleted';
		$msg = $client ;
		addNotification($type, $action, $msg);
		return back();
	}
	public function search(Request $request) {
		//dd($request);
		//
		$id = $request->cid;
		$gstno = $request->gstno;
		$phoneno = $request->phoneno;
		if (AUTH::user()->type == 'admin') {
			$clients = Client::where('id', $id)->orWhere('gst_number', $gstno)->orWhere('contact_person_phone_number', $phoneno)->paginate(20);
			return view('admin.clients.allclient', compact('clients'));
		} else {
			$clients = Client::where('id', $id)->orWhere('gst_number', $gstno)->orWhere('contact_person_phone_number', $phoneno)->where('agent_id', Auth::User()->id)->paginate(20);
			return view('agent.clients.clients', compact('clients'));
		}

	}

	public function agentclients(Request $request) {
		//
		$agentname = Auth::User()->id;
		$clients = Client::Where('user_id', $agentname)->paginate(20);
		return view('agent.clients.clients', compact('clients'));

	}
}
