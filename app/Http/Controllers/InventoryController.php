<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Barcode;
use App\Model\Inventory;
use App\Model\RowProduct;
use App\Model\Tradename;
use App\Model\Millreport;
use App\Model\Designs;
use App\Model\Notification;
use DB;
use Illuminate\Http\Request;
use Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;
use \PDF;

class InventoryController extends Controller {

	public function add($millreport_id) {
		$millreport = Millreport::find($millreport_id);
		$designs = Designs::where('tradename_id', $millreport->tradename_id)->get();
		
		return view('admin.inventory.add', compact('millreport', 'designs'));
	}

	public function index() {
		
		$inventories = DB::table('inventories')
                 ->select(
                 	'millreport_id',
                 	'inventories.tradename_id',
                 	'tradenames.name',
                 	'designs.number',
                 	'design_id',
                 	'color',
                 	'color_initial',
                 	DB::raw('count(*) as total')
                 )
                 ->join('tradenames', 'inventories.tradename_id', '=', 'tradenames.id')
                 ->join('designs', 'inventories.design_id', '=', 'designs.id')
                 ->groupBy('design_id')
                 ->groupBy('color')
                 ->orderBy('designs.number')
                 ->orderBy('color')
                 ->get();
		
		return view('admin.inventory.index', compact('inventories'));
	}

	public function create() {

		$rowproduct = RowProduct::all();
		return view('admin.inventory.form', compact('rowproduct'));
	}

	public function store(Request $request) {
		 
		
		$millreport = Millreport::find($request->millreport_id);
		$availabel=$millreport->orignal_meters - $millreport->used_meters;
		$add_qty=array_sum($request->quantity);
		 
		
		if($add_qty <= $availabel)
		{
			$millreport_id = $request->input('millreport_id');
			$tradename_id = $request->input('tradename_id');
			$design_id = $request->input('design_id');
			$color = $request->input('color');
			$color_initial = $request->input('color_initial');

			foreach ($request->input('quantity') as $quantity) {
				if ($quantity !== null) {
					$inventory = new Inventory;
					$inventory->millreport_id = $millreport_id;
					$inventory->user_id = \Auth::user()->id;
					$inventory->tradename_id = $tradename_id;
					$inventory->design_id = $design_id;
					$inventory->color = $color;
					$inventory->color_initial = $color_initial;
					$inventory->quantity = $quantity;
					$inventory->save();



					$this->addToUsedMeters($millreport_id, $quantity);

					//Generate Barcode
					$this->addBarcode($inventory->id,$color,$tradename_id,$design_id);
					
					//Set Notification
					$type = 'Inventory';
					$action = 'added';
					$msg = $inventory ;
					addNotification($type, $action, $msg);
				}
				
			}

			return back();
		}else{
			return  back()->with('message', 'Available Only'.$availabel.'meters');
		}
		
	}

	// TradeName, Year, Color, Design No, Inventory Id
	// E.g. SP/2019/BR/06151/1
	private function addBarcode ($inventory_id,$color,$tradename_id,$design_id) {
		// Get Trade Name Initials
		$inventory = DB::table('inventories')
            ->join('tradenames', 'inventories.tradename_id', '=', 'tradenames.id')
            ->join('designs', 'inventories.design_id', '=', 'designs.id')
            ->where('inventories.id', '=', $inventory_id)
            ->select('inventories.id', 'tradenames.initial', 'inventories.color_initial', 'designs.number')
            ->first();

		$year = date('Y');

		$generateBarcode = $inventory->initial .''.$year.''.$inventory->color_initial.''.$inventory->number.''.$inventory_id;
		$tradename = Tradename::where('id',$tradename_id)->first()->name;
		$barcode = new Barcode;
		$barcode->inventory_id = $inventory_id;
		$barcode->tradename = $tradename;
		$barcode->color = $color;
		$barcode->design_id = $design_id;
		$barcode->barcode = $generateBarcode;
		$barcode->save();

	}

	private function addToUsedMeters($millreport_id, $quantity) {
		$mill = Millreport::find($millreport_id);
		$mill->used_meters = $mill->used_meters + $quantity;
		$mill->save();

		$type = 'Mill';
		$action = 'updated';
		$msg = $mill ;
		addNotification($type, $action, $msg);
	}
	public function getinventory($tradename, $designno, $size) {
		$values = Inventory::where('TradeName', $tradename)->where('DesignNo', $designno)->where('Size', $size)->first();
		if (!$values) {
			return 0;
		}

		return 1;
	}
	public function validate_field($tradename, $designno) {
		$rowproducts = RowProduct::all();
		$designs = [];
		$trades = [];
		foreach ($rowproducts as $rowproduct) {
			array_push($designs, $rowproduct['DesignNo']);
			array_push($trades, $rowproduct['TradeName']);
		}
		if (in_array($tradename, $trades) || in_array($designno, $designs)) {
			return true;
		}
		return false;
	}
	private function checkSizes($size) {
		if ($size = null || $size <= 0) {
			return false;
		}
		return true;
	}
	public function searchforinventory(Request $req) {
		if ($req->trade !== null or $req->design !== null) {

			$trade = $req->trade;

			$design = $req->design;

			$Quantity = $req->quantity;

			$inventories = Inventory::where('trade_name', $trade)->orwhere('design_id', $design)->orwhere('quantity', $Quantity)->paginate(10);
			return view('admin.inventory.index', compact('inventories'));
		} else {
			return back()->withErrors('Minimum One Field Required');
		}
	}

	public function printBarcode($millreport_id, $tradename_id, $design_id, $color_initial) {
		
		$inventories = DB::table('inventories')
				->join('barcodes', 'inventories.id', '=', 'barcodes.inventory_id')
				->select('barcodes.barcode')
				->where('tradename_id', $tradename_id)
				->where('design_id', $design_id)
				->where('color_initial', $color_initial)
				->get();
		//dd($inventories);	
		$data = '';
		$data .= '
		<style type="text/css" media="all">
			p {padding: 50px;}
		</style>
			';

		
		foreach($inventories as $alphabet => $collection) {
			
		  $data .= DNS1D::getBarcodeHTML($collection->barcode, "C39");
		  // dump($collection->barcode);
		  $data .= $collection->barcode;
		  $data .= '<br>';
		  
		  
		}


		



		// //$data = DNS1D::getBarcodeHTML("4445645656", "C39");
		// exit;
		$pdf = PDF::loadHTML($data);
     	return $pdf->stream();
		
	}

	public function edit($millreport_id, $tradename_id, $design_id, $color) {
		$mill = Millreport::find($millreport_id);
		$inventories = Inventory::where('millreport_id', $millreport_id)
						->where('tradename_id', $tradename_id)
						->where('design_id', $design_id)
						->where('color', $color)
						->get();
		
		return view('admin.inventory.edit', compact('inventories', 'mill'));
	}

	public function update(Request $request) {
		// Update inventory
		// 
		foreach ($request->inventory as $key => $value) {
			$match = $this->checkQuantity($key, $value);
			if(!$match) {

				//Update Mill
				$this->updateUsedMeters($key, $value);
				$updateInventory = Inventory::find($key);
				$updateInventory->quantity = $value;
				$updateInventory->save();

				// Update Remaining Meters
			}
		}

		return redirect('/admin/inventory');
	}

	private function checkQuantity($inventory_id, $quantity) {
		$inventory = Inventory::where('id', $inventory_id)->where('quantity', $quantity)->first();
		if ($inventory === null) {
			return false;
		} else {
			return true;
		}
	}

	private function updateUsedMeters($inventory_id, $value) {
		// Substract old values from used
		// Add new value to used

		// Get mill id from inventory id
		$findMill = Inventory::find($inventory_id);
		$oldValue = $findMill->quantity;
		$mill_id = $findMill->millreport_id;

		$mill = Millreport::find($mill_id);
		$mill->used_meters =  $mill->used_meters - $oldValue;
		$mill->used_meters =  $mill->used_meters + $value;
		$mill->save();

		//brijesh
		$type = 'Mill';
		$action = "updated used meters from $oldValue to $value";
		$msg = $mill ;
		addNotification($type, $action, $msg);


	}

	public function destroy($id) {
		//
		Inventory::where('design_id', $id)->delete($id);
		return back();
	}

	// request colour delete from design form
	public function colourdelete($id) {
		//
		$delete = Inventory::where('id', $id)->delete($id);
		return $id . " Color Deleted";
	}
	// end

	// barcode scaning from design no
	public function barcode($DesignNo) {
		$barcode = Inventory::where('design_id', $DesignNo)->with('designs')->with('barcodes')->with('millreport')->paginate(20);
		// dd($barcode);
		return view('admin.inventory.barcode', compact('barcode'));

	}
	// end

	// generate barcode from request
	public function barcodegenerate(Request $request) {

		if ($request->barcode != null) {
			$barcode = $request->barcode;
			foreach ($barcode as $key => $value) {
				$barcodedata[] = Barcode::where('id', $value)->with('inventory')->with('inventory.designs')->first();
			};
		}
		return view('admin.inventory.generatebarcode', compact('barcodedata'));
	}
	// end
}
