<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $id = Auth::getUser();

        if($id==null)
        {
            return view('home');
        }else{
            $notify = DB::table('notifications')->where('read', 0)->orderBy('updated_at', 'desc')->get();
        return view('admin.dashboard', compact('notify'));
        }
    }
}
