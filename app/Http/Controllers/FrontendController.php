<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Auth;
use DB;
class FrontendController extends Controller
{
    public function index()
    {
    	 $id = Auth::getUser();
    	if($id==null)
    	{
    		return view('home');
    	}else{
    		$notify = DB::table('notifications')->where('read', 0)->orderBy('updated_at', 'desc')->get();
        return view('admin.dashboard', compact('confirm', 'completed', 'requested', 'notify'));
    	}
    	
    }
}
