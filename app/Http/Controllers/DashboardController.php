<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Notification;
use DB;
use Illuminate\Http\Request;

class DashboardController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {

		// $confirm = count(Order::where('Confirm', '=', '1')->where('Packed', '=', NULL)->get());
		// $completed = count(Order::where('Confirm', '=', '1')->where('Packed', '=', '1')->get());
		// $requested = count(Order::where('Requested', '=', '1')->get());

		$notify = DB::table('notifications')->where('read', 0)->orderBy('updated_at', 'desc')->get();
		return view('admin.dashboard', compact('notify'));
	}
	public function notify(Request $request) {

		Notification::where('read', 0)->update(['read' => 1]);
		$notify = Notification::orderBy('id', 'desc')->paginate(20);
		return view('admin.notification', compact('notify'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function console() {
		//
		$notify = DB::table('notifications')->orderBy('updated_at', 'desc')->get();
		return view('admin.console', compact('notify'));
	}
	public function overview() {
		return view('admin.overview');
	}


	public function AllUser()
	{
		$user = DB::table('users')->paginate(20);
			
		return view('admin.alluser',compact('user'));
	}
}
