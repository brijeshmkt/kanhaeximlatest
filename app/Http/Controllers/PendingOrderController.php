<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Client;
use App\Model\Order;

class PendingOrderController extends Controller {
	public function index() {
		//dd("hello");
		return view('admin.order.pendingorder');

	}
	public function getPendingDetails($id) {

		$order = Order::where('id', '=', $id)->with('orderproducts')->with('client')->first();
		$client = Client::where('id', '=', $order['ClientId'])->first();
		// $client_details = Order::with('client')->where('ClientId','=',$value)->first();
		return view('manager.pendingorders.pendingdetails', compact('order', 'client'));

	}
}
