<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\User;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller {
	//
	public function index() {
		// $user = User::all();
		$user = User::paginate(20);
		$flag = 0;
		return view('admin/users/index', compact('user', 'flag'));
	}

	public function crate(Request $request) {
		//dd('--');
		return view('admin.users.createuser');
	}

	

	public function store(Request $request) {
		$olduser = User::all();
		foreach ($olduser as $key => $value) {
			$name = $value->name;
			$email = $value->email;
			$type = $value->type;
		}

		$this->validate($request, [
			'uname' => 'required',
			'email' => 'required|email',
			'usertype' => 'required',
		]);

		$password = Hash::make($request->password);
		$user = new User;
		if ($name != $request->uname && $email != $request->email && $type != $request->usertype) {

			$user->name = $request->uname;
			$user->email = $request->email;
			$user->password = $password;
			$user->type = $request->usertype;
			$user->save();
			return back()->with('message', 'User Created!!!');
		} else {

			return back()->withErrors('User Already Exist');
			//dd($request);
		}

	}

	public function edit(Request $request, $id) {
		$users = User::where('id', $id)->get();
		return view('admin.users.edituser', compact('users'));
	}

	public function update(Request $request, $id) {

		$this->validate($request, [
			'uname' => 'required',
			'email' => 'required|email',
			'usertype' => 'required',
		]);

		$password = $request->oldpassword;
		$new = $request->password;

		if ($request->password) {
			$password = Hash::make($request->password);
		} else {
			$password;
		}

		$users = User::where('id', $id)->update([
			'name' => $request->input('uname'),
			'email' => $request->input('email'),
			'password' => $password,

			'type' => $request->input('usertype'),
		]);

		return redirect('admin/alluser')->with('message', 'Profile updated!');
	}

	public function destroy(Request $request, $id) {
		User::where('id', $id)->delete();
		return back();
	}
	public function searchuser(Request $req) {
		$this->validate($req, [
			'username' => 'required',
		]);
		$name = $req->username;
		$user = User::where('name', $name)->paginate(20);
		$flag = 1;
		return view('admin/users/index', compact('user', 'flag'));
	}
}
