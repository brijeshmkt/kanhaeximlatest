<?php
namespace App\Http\Controllers;

trait BarCodeTrait {
	public function getBarcode($tradename,$colour, $DesignNo, $id) {
        $tradename = strtoupper(substr($tradename, 0, 2));
        $year = date('Y');
        $colour = $this->formateColourNameForSrNo($colour);
        return $tradename.$year.$colour.$DesignNo.'/'.$id;
    }

    private function formateColourNameForSrNo($colour) {
        $result = '';
        settype($result, 'string');
        $chrArr = explode(" ", $colour);
        if (count($chrArr) > 1){
            foreach ($chrArr as $item) {
                $result = $result.substr($item,0,1);
            }
        }else{
            $result = substr($chrArr[0], 0, 2);
        }

        return strtoupper($result);
    }
}