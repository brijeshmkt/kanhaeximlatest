<?php

namespace App\Http\Controllers;
use App\Model\Barcode;
use App\Model\User;
use App\Model\Inventory;
use App\Model\Tradename;
use App\Model\OrderProduct;
use App\Model\Order;
use App\Model\Designs;

class PackagingController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
		
		return view('admin.packaging.index');
	}

	public function show($barcode,$order_id,$client_id) {
		$bcode = Barcode::where('barcode',$barcode)->first();
		
		if(!$bcode) return 0; 
		
		$hasProduct = $this->checkForUserOrder($bcode,$order_id,$client_id);
		
		return $hasProduct;	
	}

	public function checkForUserOrder($barcode,$order_id,$client_id){
		$trade_id = Tradename::where('name',$barcode->tradename);
		
		if(!$trade_id) return 0; 
		
		$trade_id =	$trade_id->first()->id;
		
		// $user_orderproduct = OrderProduct::where('order_id',$order_id)->where('user_id',$client_id)->where('tradename_id',$trade_id)->where('design_id',$barcode->design_id)->first();
		
		$inventory = Inventory::where('id',$barcode->inventory_id)->where('order_id',$order_id)->where('tradename_id',$trade_id)->where('design_id',$barcode->design_id)->where('color',$barcode->color)->first();
		if(!$inventory) return 0;
		$product = ['tradename' => $barcode->tradename,'Design' => Designs::where('id',$barcode->design_id)->first()->number, 'color'=>$barcode->color, 'barcode' => $barcode->barcode];
		
		return $product;
	}
	public function orderPacked($id)
	{
		Order::where('id',$id)->update(['status'=>'Completed']);
		
		return redirect('admin/dashboard');
	}
}
