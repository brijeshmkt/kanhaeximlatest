<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Order;

class CompleteOrderController extends Controller {
	public function completedorder($id) {
		$pdetails = Order::where('id', $id)->with('orderproducts')->with('orderproducts.designs')->with('orderproducts.tradenames')->with('orderproducts.designs.inventory')->get();
		return view('admin.packaging.index', compact('pdetails'));
	}
	public function showCompletedOrder() {
		$entries = Order::with('orderproducts')->with('client')->where('confirm', '=', '1')->where('shipped', '=', '1')->get();
		$entries = Order::with('orderproducts')->with('client')->where('confirm', '=', '1')->where('shipped', '=', '1')->paginate(20);
		return view('manager.completed', compact('entries'));
	}

}
