<?php

namespace App\Http\Controllers;

use App\Model\Client;
use App\Model\Order;
use App\Model\OrderProduct;
use Illuminate\Http\Request;
use PDF;

class PackingController extends Controller {
	public function packOrder($id){
		// $order_products = OrderProduct::where('id',$id)->first();
		$orders = Order::where('id',$id)->with('client')->with('orderproducts')->first();
		return view('admin.packaging.index',compact('orders'));
	}
	public function PackingOrder(Request $request) {
		$orderproduct = OrderProduct::where('id', $request->orderid)->get();
		$order = Order::where('id', $orderproduct[0]->order_id)->update(['status' => 'complated']);
		$order = Order::where('id', $orderproduct[0]->order_id)->with('orderproducts')->with('orderproducts.designs')->with('orderproducts.designs.inventory')->with('orderproducts.designs.inventory.barcodes')->with('Client')->first();
		$pdf = PDF::loadView('admin.packaging.orderpacked', compact('order'));
		$download = $pdf->download($order->client->name . ' order.pdf');
		return $download;
	}
	public function generate_packing_invoice(Request $request, $id) {

		$order = Order::where('id', $request->id)->with('orderproducts')->with('orderproducts.designs')->with('orderproducts.designs.inventory')->with('orderproducts.designs.inventory.barcodes')->with('Client')->first();
		if (empty($order[0]->orderproducts[0]->designs[0]->inventory)) {echo "Somthing is wrong";die();} else {
			$pdf = PDF::loadView('admin.packaging.orderpacked', compact('order'));
			$download = $pdf->download($order->client->name . ' order.pdf');
			return $download;
		}
	}
}
