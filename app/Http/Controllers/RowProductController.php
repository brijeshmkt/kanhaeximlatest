<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\RowProduct;
use DB;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;

class RowProductController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		// $row_product = RowProduct::all();
		$row_product = RowProduct::paginate(20);
		$flag = 0;
		return view('admin.rowproduct.index', compact('row_product', 'flag'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('admin.rowproduct.form');
	} //    Route::resource('atest','TestAdminController');

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'design_no' => 'required',
			'treadname' => 'required',
			'RowProductImage' => 'required',
			'color' => 'required',
			'totalsize' => 'required|integer',
			// 'wastesize'      => 'required',
		]);

		// Insert record into DB

		$row_product = new RowProduct;
		$row_product->DesignNo = $request->input('design_no');
		$row_product->TradeName = $request->input('treadname');

		if ($request->hasFile('RowProductImage')) {

			$RowProductImage = $request->file('RowProductImage');
			$file = time() . '.' . str_replace(' ', '', $request->file('RowProductImage')->getClientOriginalName());
			Image::make($request->file('RowProductImage'))->save(public_path('images/' . $file));
			$row_product->rowproduct_image = $file;

		}

		$row_product->Color = $request->input('color');
		$row_product->TotalSize = $request->input('totalsize');
		// $row_product->WasteSize = $request->input('wastesize');
		// $row_product->StockSize = $request->input('totalsize') - $request->input('wastesize');
		$row_product->save();

		DB::table('notifications')->insert(
			['data' => 'Mill Created']
		);
		return back()->with('message', 'Product Create Successfully');

	}
	public function searchrowproduct(Request $req) {
		$flag = 1;
		$trade = $req->trade;
		$design = $req->design;
		$rowprod = new RowProduct;
		if ($trade != null) {
			$rowprod = $rowprod->where('TradeName', $trade);
		}
		if ($design != null) {
			$rowprod = $rowprod->where('DesignNo', $design);
		}
		// $row_product = $rowprod->get();
		$row_product = $rowprod->paginate(20);
		return view('admin.rowproduct.index', compact('row_product', 'flag'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show() {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$update = RowProduct::where('id', $id)->get();
		return view('admin.rowproduct.updateform', compact('update'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

		$this->validate($request, [
			'design_no' => 'required',
			'treadname' => 'required',
			'color' => 'required',
			'totalsize' => 'required|integer',
			'wastesize' => 'required|integer',
			'stocksize' => 'required|integer',
		]);

		$data = RowProduct::where('id', $id)->first();
		$file = $data->rowproduct_image;
		if ($request->hasFile('RowProductImage')) {

			$RowProductImage = $request->file('RowProductImage');
			$file = time() . '.' . str_replace(' ', '', $request->file('RowProductImage')->getClientOriginalName());
			Image::make($request->file('RowProductImage'))->save(public_path('images/' . $file));
		};
		$row_product = RowProduct::where('id', $id)->update([
			'DesignNo' => $request->input('design_no'),
			'TradeName' => $request->input('treadname'),
			'rowproduct_image' => $file,
			'Color' => $request->input('color'),
			'TotalSize' => $request->input('totalsize'),
			'WasteSize' => $request->input('wastesize'),
			'StockSize' => $request->input('stocksize'),
		]);

		return redirect()->route('rowproducts.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		RowProduct::where('id', $id)->delete();
		return back();
	}

	public function pendingmill() {
		$pendingmill = DB::select("SELECT * FROM row_products WHERE DesignNo NOT IN (SELECT DesignNo FROM inventories)");
//            $pendingmill->paginate(10);
		// echo "<pre>";
		// print_r($pendingmill);
		// die();
		// dd($pendingmill);
		$flag = 0;
		return view('admin.rowproduct.index', compact('pendingmill', 'flag'));

	}
}
