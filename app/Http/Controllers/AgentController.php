<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Agent;
use App\Model\Order;
use App\Model\OrderProduct;
use App\Model\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Model\Client;
use App\Model\Designs;
use App\Model\Tradename;

class AgentController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		
		return view('agent.dashbord');
	}


	public function inventary()
	{
		$inventories = DB::table('inventories')
                 ->select(
                 	'millreport_id',
                 	'inventories.tradename_id',
                 	'tradenames.name',
                 	'designs.number',
                 	'design_id',
                 	'color',
                 	'color_initial',
                 	DB::raw('count(*) as total')
                 )
                 ->join('tradenames', 'inventories.tradename_id', '=', 'tradenames.id')
                 ->join('designs', 'inventories.design_id', '=', 'designs.id')
                 ->groupBy('design_id')
                 ->groupBy('color')
                 ->orderBy('designs.number')
                 ->orderBy('color')
                 ->get();
		return view('agent.inventory.index', compact('inventories'));
	}

	public function addnewclient(Request $request)
	{

		$this->validate($request, [
			'clientname' => 'required',
			'firmname' => 'required',
			'firmaddress' => 'required',
			'city' => 'required',
			'state' => 'required',
			'country' => 'required',
			'officeaddress' => 'required',
			'pcname' => 'required',
			'phoneno' => 'required',
			'gstno' => 'required',
			'email' => 'required|unique:clients,email',
		]);
		// dd($request);
		$client = new Client;
		$client->client_name = $request->input('clientname');
		$client->user_id = AUTH::user()->id;
		$client->firm_name = $request->input('firmname');
		$client->firm_address = $request->input('firmaddress');
		$client->firm_city = $request->input('city');
		$client->firm_state = $request->input('state');
		$client->firm_country = $request->input('country');
		$client->office_address = $request->input('officeaddress');
		$client->contact_person_name = $request->input('pcname');
		$client->contact_person_phone_number = $request->input('phoneno');
		$client->contact_person_office_phone_number = $request->input('officeno');
		$client->contact_person_land_line_phone_number = $request->input('landlineno');
		$client->gst_number = $request->input('gstno');
		$client->email = $request->input('email');
		// $client->agent_name = Auth::user()->name;
		// $client->agent_id = Auth::user()->id;
		$client->save();

		$type = 'Client';
		$action = 'Added';
		$msg = $client ;
		addNotification($type, $action, $msg);

		return redirect('agent/viewallclient')->with('message', 'Create Client Successfully');
	}

	public function allorder()
	{
		$entries = Order::where('user_id' ,"=" ,AUTH::user()->id)->with('client')->with('users')->with('orderproducts')->paginate(20);

		return view('agent.order.allorders', compact('entries'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function viewallclient() {
		
		$id = Auth::user()->id;
		
		$clients = Client::where('user_id','=', $id)->orderBy('id', 'desc')->paginate(20);
		return view('agent.clients.clients', compact('clients'));
	}





	public function insertclient()
	{
		return view('agent.clients.newclient');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$validator = Validator::make(Input::all(),
			array(
				'date' => 'required',
				'firmname' => 'required',
				'phone_no' => 'required|integer',
				'addres' => 'required',
				'name' => 'required',
				'agent' => 'required',
				'station' => 'required',
				'tp' => 'required',
				'gst_no' => 'required',
				'trade_name' => 'required',
				'design_no' => 'required',
				'rate.*' => 'integer',
				'size.*' => 'integer',
				'quantity.*' => 'required|integer',
			)
		);

		if ($validator->fails()) {
			return redirect()->back()->withInput(Input::all());
		} else {

			$val = $request->terms; //Terms and Conditions
			if (!$val) {
				$val = "0";
			}

			$userid = \Auth::user()->id;
			$ordermakertype = \Auth::user()->name;

			// Insert values in order table

			$order = new Order;
			$order->Date = $request->date;
			$order->firmname = $request->firmname;
			$order->Station = $request->station;
			$order->T_p = $request->tp;
			$order->Remark = $request->remark;
			$order->Terms_Status = $val;
			$order->UserId = $userid;
			$order->OrderMakerType = $ordermakertype;
			$order->ClientId = $client->id;
			$order->save();

			//Inserting values in OrderProduct table

			$trade_names = $request->trade_name;
			$d_no = $request->design_no;
			$rate = $request->rate;
			$size = $request->size;
			$quant = $request->quantity;
			foreach ($trade_names as $t_key => $t_value) {
				$order_product = new OrderProduct;
				$order_product->TradeName = $trade_names[$t_key];
				$order_product->DesignNo = $d_no[$t_key];
				$order_product->Quantity = $quant[$t_key];
				$order_product->Price = $rate[$t_key];
				$order_product->Size = $size[$t_key];
				$order_product->Order_id = $order->id;
				$order_product->save();
			}

			if (Auth::user()->type == 'admin') {
				DB::table('notifications')->insert(
					['heading' => 'New Order', 'data' => 'You Have Created New Order']
				);
			} else {
				DB::table('notifications')->insert(
					['heading' => 'New Order', 'data' => $ordermakertype . ' Have Created New Order']
				);
			}
			return back()->with('message', 'Order Placed!!!');
		}

	}

    public function placeorder(Request $request, $id)
    {
    	$client = Client::where('id', $id)->get();
		$designs = Designs::all();
		$tradenames = Tradename::all();

		return view('agent.order.form', compact('client', 'tradenames', 'designs'));
    }
 

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */

	public function getOrderDetails() {
		$userid = \Auth::user()->id;
		$order_details = Order::with('orderproducts')->with('client')->where('UserId', '=', $userid)->get();
		$order_details = Order::where('Confirm', null)->paginate(20);
		return view('agent.order.orderdetails', compact('order_details'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Request $request, $id) {
		//
		$agent = Agent::where('id', $id)->get();
		return view('admin.agents.editagent', compact('agent'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//

		$this->validate($request, [
			'agentname' => 'required',
			'firmname' => 'required',
			'city' => 'required',
			'state' => 'required',
			'country' => 'required',
			'officeaddress' => 'required',
			'phoneno' => 'required|integer',
			'gstno' => 'required',
			'email' => 'required',
		]);

		Agent::where('id', $id)->update([
			'name' => $request->input('agentname'),
			'firm_name' => $request->input('firmname'),
			'city' => $request->input('city'),
			'state' => $request->input('state'),
			'country' => $request->input('country'),
			'address' => $request->input('officeaddress'),
			'email' => $request->input('email'),
			'mobile_number' => $request->input('phoneno'),
			'office_phone_number' => $request->input('officeno'),
			'land_line_phone_number' => $request->input('landlineno'),
			'gst_number' => $request->input('gstno'),
		]);

		User::where('id', $id)->update([
			'name' => $request->input('agentname'),
			'email' => $request->input('email'),
		]);

		if (Auth::user()->type == 'admin') {
			DB::table('notifications')->insert(
				['heading' => 'Agent Detail Updated', 'data' => 'You Have Updated ' . $id . ' No Agent Datail']
			);
		} else {
			DB::table('notifications')->insert(
				['heading' => 'Agent Detail Updated', 'data' => Auth::user()->name . ' Have Updated ' . $id . ' No Agent Datail']
			);
		}

		return redirect('admin/allagents')->with('message', 'Agent Detail Updated');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		Agent::where('id', $id)->delete();
		User::where('agent_id', $id)->delete();
		return back();
	}
	public function search(Request $request) {
		//dd($request);
		$id = $request->aid;
		$gstno = $request->gstno;
		$phoneno = $request->phoneno;
		$search = Agent::where('id', $id)->orWhere('gst_number', $gstno)->orWhere('mobile_number', $phoneno)->paginate(20);
		return view('admin.agents.allagents', compact('search'));
	}
	public function pendingorder() {
		//
		$userid = \Auth::user()->id;
		// $order_details = Order::with('orderproducts')->with('client')->where('UserId', '=', $userid)->get();
		//  dd($order_details);
		$order_details = Order::where('Confirm', null)->with('client')->where('UserId', '=', $userid)->paginate(20);

		// $p2->setPageName('allorder');
		return view('agent.order.allorders', compact('order_details'));
	}

	public function allagent() {
		//
	
		$agents = Agent::paginate(20);

		return view('admin.agents.allagents', compact('agents'));
	}
	public function newagent() {
		
		return view('admin.agents.newagent');
	}

	public function insertagent(Request $request) {

		
		$this->validate($request, [
			'agentname' => 'required',
			'firmname' => 'required',
			'city' => 'required',
			'state' => 'required',
			'country' => 'required',
			'officeaddress' => 'required',
			'phoneno' => 'required|integer',
			'gstno' => 'required',
			'email' => 'required',
			'password' => 'required|confirmed',
		]);
		//dd($request);
		$agent = new Agent;
		$agent->name = $request->input('agentname');
		$agent->user_id = AUTH::user()->id;
		$agent->firm_name = $request->input('firmname');
		$agent->city = $request->input('city');
		$agent->state = $request->input('state');
		$agent->country = $request->input('country');
		$agent->address = $request->input('officeaddress');
		$agent->email = $request->input('email');
		$agent->mobile_number = $request->input('phoneno');
		$agent->office_phone_number = $request->input('officeno');
		$agent->land_line_phone_number = $request->input('landlineno');
		$agent->email = $request->input('email');
		$agent->gst_number = $request->input('gstno');
		$agent->save();

		$user = new User;
		$user->name = $request->input('agentname');
		$user->email = $request->input('email');
		$user->password = Hash::make($request->input('password'));
		$user->agent_id = $agent->id;
		$user->type = $request->input('type');
		$user->save();



			//Set Notification
			$type = 'agent';
			$action = 'added';
			$msg = $agent ;
			addNotification($type, $action, $msg);
		

		return redirect('admin/allagents')->with('message', 'Agent Created!!!');

	}

}
