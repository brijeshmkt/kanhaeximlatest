<?php

namespace App\Http\Controllers;

use App\Model\Client;
use App\Model\Designs;
use App\Model\Inventory;
use App\Model\Notification;
use App\Model\Order;
use App\Model\OrderProduct;
use App\Model\Tradename;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;
use PDF;

class OrderFormController extends Controller {

	private $orderForm;

	public function index() {
		//
		return view('admin.order.dashboard');
	}
	public function requestedorders() {
		// $entries = Order::where("status", "=", "outofstock")->with('client')->with('users')->with('orderproducts')->paginate(10);
		//dd('hh');
		$entries = Order::where('status', '=', 'outofstock')->with('client')->with('users')->with('orderproducts')->paginate(20);
		//print_r($entries)die;
		$entries = Order::where('status', '=', 'outofstock')
			->with('client')
			->with('users')
			->with('orderproducts')
			->with('orderproducts.designs')
			->paginate(20);

		return view('admin.requestedorders', compact('entries'));
	}

	public function requestedordersearch(Request $request) {
		$number=$request->mobile;
		$gst=$request->gstno;
		$name=strtolower($request->aname);
		
		$entries1 = Order::where('status', '=', 'outofstock')->with('client')->with('users')->with('orderproducts')->paginate(20);
		//print_r($entries)die;
		$entries1 = Order::where('status', '=', 'outofstock')
			->with('client')
			->with('users')
			->with('orderproducts')
			->with('orderproducts.designs')
			->paginate(20);

			foreach ($entries1 as $key) {
					
				if($key->client->contact_person_phone_number==$number && $key->client->gst_number==$gst && $key->users->name==$name)
				{
					$entries[]=$key;
				}
				
			}

		return view('admin.requestedordersearch', compact('entries'));
	}
	public function searchOrder(Request $request) {
		$entries = Order::where("status", "=", $request->searchval)->with('client')->with('users')->with('orderproducts')->paginate(20);
		return view('admin.order.allorder', compact('entries'));

	}
	public function allOrder() {

		$entries = Order::with('client')->with('users')->with('orderproducts')->paginate(20);

		return view('admin.order.allorder', compact('entries'));

	}
	public  function alloredersearch(Request $request){
		$number=$request->mobile;
		$gst=$request->gstno;
		$name=strtolower($request->aname);
		$entries1 = Order::with('client')->with('users')->with('orderproducts')->paginate(20);
		$entries=array();
			foreach ($entries1 as $key) {
					
				if($key->client->contact_person_phone_number==$number && $key->client->gst_number==$gst && $key->users->name==$name)
				{
					$entries[]=$key;
				}
				
			}
			
		
		return view('admin.order.allaordersearch', compact('entries'));
	}
	public function pendingorder() {

		$entries = Order::where('status', '=', 'partial')->with('client')->with('users')->with('orderproducts')->paginate(20);
		//print_r($entries)die;
		$entries = Order::where('status', '=', 'partial')
			->with('client')
			->with('users')
			->with('orderproducts')
			->with('orderproducts.designs')
			->paginate(20);
		//print_r($entries);die;
		return view('admin.order.pendingorder', compact('entries'));
	}
	public function approvalorder() {

		$entries = Order::where('approval', '=', '1')->with('client')->with('users')->with('orderproducts')->paginate(20);
		//print_r($entries)die;
		$entries = Order::where('approval', '=', '1')
			->with('client')
			->with('users')
			->with('orderproducts')
			->with('orderproducts.designs')
			->paginate(20);
		//print_r($entries);die;
		return view('admin.order.appeovaloder', compact('entries'));
	}
	public function pendingordersearch(Request $request)
	{
		$number=$request->mobile;
		$gst=$request->gstno;
		$name=strtolower($request->aname);
		$entries = Order::where('status', '=', 'partial')->with('client')->with('users')->with('orderproducts')->paginate(20);
		
		$entries1 = Order::where('status', '=', 'partial')
			->with('client')
			->with('users')
			->with('orderproducts')
			->with('orderproducts.designs')
			->paginate(20);
			$entries=array();
			foreach ($entries1 as $key) {
					
				if($key->client->contact_person_phone_number==$number && $key->client->gst_number==$gst && $key->users->name==$name)
				{
					$entries[]=$key;
				}
				
			}
		//print_r($entries);die;
		return view('admin.order.pendingordersearch', compact('entries'),compact('request'));
	}
	public function requestedorder() {

		$entries = Order::where('status', '=', 'outofstock')->with('orderproducts')->with('client')->with('users')->with('orderproducts.designs')
			->orderBy('id', 'desc')->paginate(20);

		return view('admin.order.requestedorder', compact('entries'));
	}
	public function completedorder() {
		
		$entries = Order::where('status', '=', 'complated')->with('client')
			->with('users')
			->with('orderproducts')
			->with('orderproducts.designs')->paginate(20);
		return view('admin.order.completedorder', compact('entries'));
	}
	public function neworder() {
		$entries = Order::where('Confirm', '=', NULL)->with('client')->with('users')->with('orderproducts')->paginate(20);
		return view('admin.order.pendingorder', compact('entries'));
	}
	public function confirmorders() {

		$entries = Order::where('status', '=', 'confirm')->with('client')->with('users')->with('orderproducts')->paginate(20);
		return view('admin.order.confirmorder', compact('entries'));
	}
	public function confirmorderssearch(Request $request) {
		$number=$request->mobile;
		$gst=$request->gstno;
		$name=strtolower($request->aname);
		
		$entries1 = Order::where('status', '=', 'confirm')->with('client')->with('users')->with('orderproducts')->paginate(20);
		$entries=array();
			foreach ($entries1 as $key) {
					
				if($key->client->contact_person_phone_number==$number && $key->client->gst_number==$gst && $key->users->name==$name)
				{
					$entries[]=$key;
				}
				
			}
		return view('admin.order.confirmorderssearch', compact('entries'),compact('request'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request, $id) {
		$client = Client::where('id', $id)->get();
		$designs = Designs::all();
		$tradenames = Tradename::all();

		return view('admin.order.form', compact('client', 'tradenames', 'designs'));
	}

	public function addOrderFormColumn() {
		$designs = Designs::all();
		$tradenames = Tradename::all();
		return view('admin.order.add-column', compact('tradenames', 'designs'));
	}
	public function chkpartialOrder($id) {

		$entries = Order::where('id', '=', $id)->with('orderproducts')->with('client')->with('users')->with('orderproducts.designs')->get();
		
		return view('admin.order.chkorder', compact('entries'));
	}

	public function checkOrder(Request $request) {
		$results = array();
		//	dd($request);
		for ($i = 0; $i < count($request->orderLength); $i++) {
			if ($request->tradename_id[$i]) {
				// Loop thru each design
				// Convert string to array
				$designs = explode(",", $request->design_no[$i]);
				$designs = array_filter($designs); // Remove empty values from array
				$inventory_array = array();
				foreach ($designs as $design_no) {
					//echo $design_no . '<br>';
					$desing_id = $this->getDesignIdFromNumber($design_no); // Get design id from design number
					$inventory = $this->isInventoryAvailable($request->tradename_id[$i], $desing_id);
					$single_design = [
						'design_number' => $design_no,
						'inventory' => $inventory,
						'inventory_count' => $inventory->count(),
					];
					array_push($inventory_array, $single_design);
				}

				$results[$i] = [
					'tradename_id' => $request->tradename_id[$i],
					'tradename' => $this->getTradeName($request->tradename_id[$i]),
					'inventories' => $inventory_array,
					'Quantity' => $request->quantity[$i],
					'Rate' => $request->rate[$i],
				];
			}

		}
		return view('admin.order.check')->with(['results' => $results]);
	}
	public function assignInventory($order_id, $tradename_id, $desing_id, $qty, $color) {
		// Inventory::update([])

		Inventory::where('tradename_id', $tradename_id)->where('design_id', $desing_id)->where('color', $color)->whereNull('order_id')->take($qty)->update(['order_id' => $order_id]);

	}
	public function checkStatus($qty, $st) {
		if ($qty <= $st) {
			return "Confirm";
		}
		if ($st <= $qty) {
			return "partial";
		}

		//print_r($inventory);
	}
	public function partialProcess(Request $request) {

		$results = array();
		$status = '';
		$chk = array();
		for ($i = 0; $i < count($request->orderLength); $i++) {
			$qty = $request['quantity'][$i];

			if ($request->tradename_id[$i]) {
				// Loop thru each design
				// Convert string to array
				$designs = explode(",", $request->design_no[$i]);
				$designs = array_filter($designs); // Remove empty values from array
				$inventory_array = array();
				foreach ($designs as $design_no) {
					//echo $design_no . '<br>';

					$desing_id = $this->getDesignIdFromNumber($design_no); // Get design id from design number

					$inventory = $this->isInventoryAvailable($request->tradename_id[$i], $desing_id);
					if (count($inventory) > 0) {
						for ($v1 = 0; $v1 < count($inventory); $v1++) {
							$st = $inventory[$v1]->color_count;
							$data = $this->checkStatus($qty, $st);
							//dd($data);
							$chk[] = $data;
						}

					} else {
						$chk[] = "outofstock";
					}

				}

			}

		}
		$con = 0;
		for ($i = 0; $i < count($chk); $i++) {

			if ($chk[$i] == "Confirm") {
				$con = $con + 1;
			}
		}
		if ($con == count($chk)) {
			$status = "Confirm";

		} elseif (in_array('partial', $chk)) {
			$status = "partial";
		} elseif (in_array('Confirm', $chk) && in_array('outofstock', $chk)) {
			$status = "partial";
		} else {
			$status = "outofstock";
		}
		
		$order_id = $request->order_id;

		if ($status == "Confirm") {
			//dd($status);
			Order::where('id', $order_id)->update(['status' => $status]);
			for ($i = 0; $i < count($request->orderLength); $i++) {
				$qty = $request['quantity'][$i];

				if ($request->tradename_id[$i]) {
					// Loop thru each design
					// Convert string to array
					$designs = explode(",", $request->design_no[$i]);
					$designs = array_filter($designs); // Remove empty values from array
					//dd($designs);
					$inventory_array = array();
					foreach ($designs as $design_no) {
						//echo $design_no . '<br>';

						$desing_id = $this->getDesignIdFromNumber($design_no); // Get design id from design number

						$inventory = $this->isInventoryAvailable($request->tradename_id[$i], $desing_id);
						for ($v1 = 0; $v1 < count($inventory); $v1++) {
							$this->assignInventory($order_id, $request->tradename_id[$i], $desing_id, $qty, $inventory[$v1]->color);
							//dd($inventory);
						}

					}

				}

			}
			$trade_id = $request->tradename_id;
			$all_trades = count(array_filter($request->tradename_id, function ($x) {return !empty($x);}));
			for ($i = 0; $i < $all_trades; $i++) {
				$designs = array_filter($request->design_no, function ($x) {return !empty($x);});
				$design = explode(",", $designs[$i]);
				foreach ($design as $val) {
					$designid = Designs::where('number', $val)->first()->id;
					OrderProduct::create([
						'user_id' => Auth::user()->id,
						'order_id' => $order_id,
						'tradename_id' => $request->tradename_id[$i],
						'design_id' => $designid,
						'quantity' => $request->quantity[$i],
						'rate' => $request->rate[$i],
					]);
				}
			}

		}

		
		return redirect('admin/order/allOrder');

	}
	public function processOrder(Request $request) {


		$results = array();
		$status = '';
		$chk = array();
		for ($i = 0; $i < count($request->orderLength); $i++) {
			$qty = $request['quantity'][$i];

			if ($request->tradename_id[$i]) {
				// Loop thru each design
				// Convert string to array
				$designs = explode(",", $request->design_no[$i]);
				$designs = array_filter($designs); // Remove empty values from array
				$inventory_array = array();
				foreach ($designs as $design_no) {
					//echo $design_no . '<br>';

					$desing_id = $this->getDesignIdFromNumber($design_no); // Get design id from design number

					$inventory = $this->isInventoryAvailable($request->tradename_id[$i], $desing_id);
					if (count($inventory) > 0) {
						for ($v1 = 0; $v1 < count($inventory); $v1++) {
							$st = $inventory[$v1]->color_count;
							$data = $this->checkStatus($qty, $st);
							//dd($data);
							$chk[] = $data;
						}

					} else {
						$chk[] = "outofstock";
					}

				}

			}

		}
		$con = 0;
		for ($i = 0; $i < count($chk); $i++) {

			if ($chk[$i] == "Confirm") {
				$con = $con + 1;
			}
		}
		if ($con == count($chk)) {
			$status = "Confirm";

		} elseif (in_array('partial', $chk)) {
			$status = "partial";
		} elseif (in_array('Confirm', $chk) && in_array('outofstock', $chk)) {
			$status = "partial";
		} else {
			$status = "outofstock";
		}
		$order = new Order;
		$order->user_id = Auth::user()->id;
		$order->client_id = $request->clientid;
		$order->destination_station = $request->station;
		$order->transporter_name = $request->transporter_name;
		$order->payment_terms = $request->payment;
		$order->terms = $request->terms;
		$order->remarks = $request->remark;
		$order->status = $status;
		$order->save();
		$order_id = $order->id;

		if ($status == "Confirm") {
			//dd($status);
			for ($i = 0; $i < count($request->orderLength); $i++) {
				$qty = $request['quantity'][$i];

				if ($request->tradename_id[$i]) {
					// Loop thru each design
					// Convert string to array
					$designs = explode(",", $request->design_no[$i]);
					$designs = array_filter($designs); // Remove empty values from array
					//dd($designs);
					$inventory_array = array();
					foreach ($designs as $design_no) {
						//echo $design_no . '<br>';

						$desing_id = $this->getDesignIdFromNumber($design_no); // Get design id from design number

						$inventory = $this->isInventoryAvailable($request->tradename_id[$i], $desing_id);
						for ($v1 = 0; $v1 < count($inventory); $v1++) {
							$this->assignInventory($order_id, $request->tradename_id[$i], $desing_id, $qty, $inventory[$v1]->color);
							//dd($inventory);
						}

					}

				}

			}

		}

		$trade_id = $request->tradename_id;
		$all_trades = count(array_filter($request->tradename_id, function ($x) {return !empty($x);}));
		for ($i = 0; $i < $all_trades; $i++) {
			$designs = array_filter($request->design_no, function ($x) {return !empty($x);});
			$design = explode(",", $designs[$i]);
			foreach ($design as $val) {
				$designid = Designs::where('number', $val)->first()->id;
				OrderProduct::create([
					'user_id' => Auth::user()->id,
					'order_id' => $order_id,
					'tradename_id' => $request->tradename_id[$i],
					'design_id' => $designid,
					'quantity' => $request->quantity[$i],
					'rate' => $request->rate[$i],
				]);
			}
		}

		if(Auth::user()->type == 'agent')
		{
			return redirect('agent/viewallclient');
		}
		return redirect('admin/order/allOrder');
	}
	public function checkOrderbkp(Request $request) {
		$tradename_ids = $request->input('tradename_id');
		$design_nos = $request->input('design_no');
		$quantities = $request->input('quantity');

		$results = [];
		DB::enableQueryLog();

		for ($x = 0; $x < count($request->input('orderLength')); $x++) {
			echo "string";

			if (isset($tradename_ids[$x])) {

				$isInventoryAvailable = $this->isInventoryAvailable($tradename_ids[$x], $design_nos[$x]);

				$tradeName = $this->getTradeName($tradename_ids[$x]);
				$designNumber = $this->getDesignNumber($design_nos[$x]);

				$results[$x] = [
					'tradename_id' => $tradename_ids[$x],
					'tradename' => $tradeName,
					'design_id' => $design_nos[$x],
					'design_number' => $designNumber,
					'isInventoryAvailable' => $isInventoryAvailable->count(),
					'inventoryDetails' => $isInventoryAvailable,
					'orderedQuanity' => $quantities[$x],
				];
			}
		}

		//dd($results);
		//return view('admin.order.check')->with([ 'results' => $results]);

	}

	public function getTradeName($id) {
		$tradename = Tradename::find($id);
		return $tradename->name;
	}

	public function getDesignNumber($id) {
		$design = Designs::find($id);
		return $design->number;
	}

	public function getDesignIdFromNumber($number) {
		$design = Designs::where('number', '=', $number)->first();
		return $design->id;
	}

	public function isInventoryAvailable($tradename_id, $design_id) {
		$inventories = DB::table('inventories')
			->select(DB::raw('count(*) as color_count, color'))
			->whereNull('order_id')
			->where('tradename_id', '=', $tradename_id)
			->where('design_id', '=', $design_id)
			->groupBy('color')
			->get();

		return $inventories;
	}

	public function store(Request $request) {
		dd($request);
	}

	public function storebkp(Request $request) {

		if (Auth::user()->type == 'agent') {
			$this->validate($request, [
				'agent' => 'required',
				'station' => 'required',
				'tp' => 'required',
				'trade_name' => 'required',
				'rate.*' => 'required|integer',
				'design_no.*' => 'required',
				'payment' => 'required',
				'terms' => 'required',
				'quantity.*' => 'required|integer',

			]);
			$trade_names = $request->trade_name;
			$d_no = $request->design_no;
			$rate = $request->rate;
			$size = $request->size;
			$quant = $request->quantity;

			$userid = \Auth::user()->id;
			$ordermakertype = \Auth::user()->type;

			$order = new Order;
			$order->destination_station = $request->station;
			$order->transporter_name = $request->tp;
			$order->remarks = $request->remark;
			$order->payment_terms = $request->terms;
			$order->terms = $request->payment;
			$order->user_id = $userid;
			$order->status = "unapprove";
			$order->client_id = $request->clientid;
			$order->ordermakertype = $ordermakertype;
			$order->save();

			//Inserting values in OrderProduct table

			foreach ($trade_names as $t_key => $t_value) {
				$order_product = new OrderProduct;
				$order_product->trade_name = $trade_names[$t_key];
				$order_product->design_id = $d_no[$t_key];
				$order_product->quantity = $quant[$t_key];
				$order_product->rate = $rate[$t_key];
				$order_product->order_id = $order->id;
				$order_product->save();
			}

			$getemail = Client::select('email')->where('firmname', '=', $request['firmname'])->where('gstnumber', '=', $request['gstnumber'])->first();
			// var_dump($email->email);
			// die();
			$clinet_email = $getemail->email;
			$data1 = array(
				'clientname' => $request['clientname'],
				'firmname' => $request['firmname'],
				'firmaddress' => $request['firmaddress'],
				'phoneno' => $request['phoneno'],
				'gstnumber' => $request['gstnumber'],
				'trade_name' => $request->trade_name,
				'design_no' => $request->design_no,
				'rate' => $request->rate,
				'quantity' => $request->quantity,

			);

			Mail::send('admin.order.mail', $data1, function ($message) use ($client_email, $request) {
				$message->to($client_email, $request->clientname)->cc(env('mail_username'))->subject('New Order!');
			});

			return redirect('admin/viewallclient')->with('message', 'Order Accept AS Requested Order');
		} else {

			$this->validate($request, [
				'agent' => 'required',
				'station' => 'required',
				'tp' => 'required',
				'trade_name' => 'required',
				'rate.*' => 'required|integer',
				'design_no.*' => 'required',
				'payment' => 'required',
				'terms' => 'required',
				'quantity.*' => 'required|integer',

			]);
			$trade_names = $request->trade_name;
			$d_no = $request->design_no;
			$rate = $request->rate;
			$size = $request->size;
			$quant = $request->quantity;
			$userid = \Auth::user()->id;
			$ordermakertype = \Auth::user()->type;
			$res = $this->check_inventory($trade_names, $d_no, $quant);
			$status = $res ? "unapprove" : "outofstock";
			foreach ($trade_names as $t_key => $t_value) {
				$getinventory = Inventory::where('design_id', $d_no[$t_key])->get();
				foreach ($getinventory as $key => $value) {
					if (!empty($getinventory)) {
						$inventory = Inventory::where('id', $value->id)->update([
							'quantity' => $value->quantity - $quant[$t_key],
						]);
					}
				}
			}
			// inserting order
			$order = new Order;
			$order->destination_station = $request->station;
			$order->transporter_name = $request->tp;
			$order->remarks = $request->remark;
			$order->payment_terms = $request->terms;
			$order->terms = $request->payment;
			$order->user_id = $userid;
			$order->status = $status;
			$order->client_id = $request->clientid;
			$order->ordermakertype = $ordermakertype;
			$order->save();

			//Inserting values in OrderProduct table
			foreach ($trade_names as $t_key => $t_value) {
				$order_product = new OrderProduct;
				$order_product->trade_name = $trade_names[$t_key];
				$order_product->design_id = $d_no[$t_key];
				$order_product->quantity = $quant[$t_key];
				$order_product->rate = $rate[$t_key];
				$order_product->order_id = $order->id;
				$order_product->save();

				if ($status == "outofstock") {
					$notification = new Notification;
					$notification->title = "Inventory Doesn't Exist For " . $trade_names[$t_key] . ",";
					$notification->read = 0;
					$notification->description = "You dont have inventory for tradename " . $trade_names[$t_key] . " and design number of " . $d_no[$t_key] . " for order number is " . $order_product->id;
					$notification->save();

				}
			}

			// mail process
			$getemail = Client::select('email')->where('firm_name', '=', $request['firmname'])->where('gst_number', '=', $request['gstnumber'])->first();

			$client_email = $getemail->email;
			$data1 = array(
				'clientname' => $request['clientname'],
				'firm_name' => $request['firmname'],
				'firmaddress' => $request['firmaddress'],
				'phoneno' => $request['phoneno'],
				'gst_number' => $request['gstnumber'],
				'trade_name' => $request->trade_name,
				'design_no' => $request->design_no,
				'rate' => $request->rate,
				'quantity' => $request->quantity,
			);

			Mail::send('admin.order.mail', $data1, function ($message) use ($client_email, $request) {
				$message->to($client_email, $request->clientname)->subject('New Order!');
			});

			return redirect('admin/viewallclient')->with('message', 'Order placed');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	// check this after form submit for outofstock or not
	public function check_inventory($tradename, $design, $quantity) {
		foreach ($tradename as $key => $value) {
			$res = Inventory::select('quantity')->where('trade_name', $tradename[$key])->where('design_id', $design[$key])->first();

			if (empty($res)) {
				if (!isset($res->quantity)) {
					$res = null;
				} else {
					$res = $res->quantity;
				}

				if (!$res) {
					return 0;
				} elseif ($res < $quantity[$key]) {
					return 0;
				}
			}

		}
		return 1;
	}
	// check this inventory before form submit
	public function checkinventory(Request $request) {
		foreach ($request->trade_name as $key => $value) {
			$res = Inventory::select('quantity')->where('trade_name', $request->trade_name[$key])->where('design_id', $request->design_no[$key])->first();
			$request = $request;
			if ($res == null) {
				return view('admin.inventory.checkinventory', compact('request'));
			} else {
				$this->store($request);
				return redirect('admin/viewallclient')->with('message', 'Order placed');
			}
		}

	}

	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	public function destroy($id) {
		//
		Order::where('id', $id)->delete();
		OrderProduct::where('order_id', $id)->delete();
		return back();
	}

	//agent order form
	public function agentorders(Request $request, $id) {
		//
		$client = Client::where('id', $id)->get();
		return view('agent.order.form', compact('client'));
	}
	// end

	// perticuler agent orders

	public function agentallorder() {
		
		$agentid = Auth::user()->id;
		$order_details = Order::where('UserId', '=', $agentid)->with('client')->with('orderproducts')->paginate(20);
		return view('agent.order.orderdetails', compact('order_details'));
	}
	//end

	// orderform requestorder save who have not inventory

	// end

	// order confirm button click
	public function orderconform(Request $request, $id) {
		//
		$millreport = Order::where('id', $id)->update([
			'status' => "approved",
		]);
		return back();

	}
	// end

	// order searching controller start

	public function ordersearch(Request $request) {
		//
		///dd('oo');
		//$entries = Order::with('client')->with('users')->with('orderproducts')->paginate(20);
		$aname = $request->aname;
		$mobile = $request->mobile;
		$gstno = $request->gstno;

		$search = DB::table('orders')
			->join('clients', 'orders.ClientId', '=', 'clients.id')
			->join('users', 'orders.UserId', '=', 'users.id')
			->join('order_products', 'orders.id', '=', 'order_products.order_id')
			->select('orders.*', 'clients.*', 'users.*', 'order_products.*')
			->where('clients.GstNumber', 'LIKE', $gstno)
			->where('clients.deleted_at', null)
			->where('clients.PhoneNo', 'LIKE', $mobile)
			->where('users.Name', 'LIKE', $aname)
			->where('users.deleted_at', null)
			->where('order_products.deleted_at', null)
			->where('orders.deleted_at', null)
			->where('Confirm', '=', NULL)->where('Packed', '=', NULL)->where('Requested', '=', NULL)
			->paginate(20);
		return view('admin.order.pendingorder', compact('search'));

	}

	public function requestsearch(Request $request) {
		

		$aname = $request->aname;
		$mobile = $request->mobile;
		$gstno = $request->gstno;

		$search = DB::table('orders')
			->join('clients', 'orders.client_id', '=', 'clients.id')
			->join('users', 'orders.user_id', '=', 'users.id')
			->join('order_products', 'orders.id', '=', 'order_products.order_id')
			->select('orders.*', 'clients.*', 'users.*', 'order_products.*')
			->where('clients.gst_number', $gstno)
			->where('clients.contact_person_phone_number', $mobile)
			->where('users.name', $aname)
			->where('status', '=', "outofstock")

			->paginate(20);

		return view('admin.order.requestedorder', compact('search'));

	}
	public function completedordersearch(Request $request) {
		//

		$aname = $request->aname;
		$mobile = $request->mobile;
		$gstno = $request->gstno;

		$search = DB::table('orders')
			->join('clients', 'orders.ClientId', '=', 'clients.id')
			->join('users', 'orders.UserId', '=', 'users.id')
			->join('order_products', 'orders.id', '=', 'order_products.order_id')
			->select('orders.*', 'clients.*', 'users.*', 'order_products.*')
			->where('clients.GstNumber', $gstno)
			->where('clients.deleted_at', null)
			->where('clients.PhoneNo', $mobile)
			->where('users.Name', $aname)
			->where('Packed', '=', 1)->where('Confirm', '=', NULL)->where('Requested', '=', NULL)
			->where('users.deleted_at', null)
			->where('order_products.deleted_at', null)
			->where('orders.deleted_at', null)
			->paginate(20);

		return view('admin.order.completedorder', compact('search'));

	}

	// order searching controller end
	public function download_pdf(Request $request, $id) {

		$order_product = OrderProduct::where('id', $id)->get();
		$tradename = Tradename::where('id',$order_product[0]->tradename_id)->get();
		$order = Order::where('id', $order_product[0]->order_id)->with('orderproducts')->with('orderproducts.designs')->with('client')->get();
		
		
		$pdf = PDF::loadView('admin.sample', compact('order'),compact('tradename'));
		$download = $pdf->download($request->clientname . ' order.pdf');
		return $download;

	}


	public function approval(Request $request, $id)
	{
		
		$order=Order::find($id);

		if($order->approval == 0)
		{
			$order->approval = 1;
		}
		else{
			 $order->approval = 0;
		   }
		 $order->save();  
	  return back();
	}

}
