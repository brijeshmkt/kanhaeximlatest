<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Barcode;
use App\Model\Designs;
use App\Model\Inventory;
use App\Model\Millreport;
use App\Model\Tradename;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class MillController extends Controller {
	use BarCodeTrait;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$per_page = env('PER_PAGE');
		$row_product = Millreport::orderBy('id', 'desc')->paginate($per_page);
		$flag = 0;
		 // dd($row_product);
		return view('admin.millreport.index', compact('row_product', 'flag'));
	}

	public function create() {
		$tradenames = Tradename::all();

		return view('admin.millreport.form', compact('tradenames'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'lot_numbers' => 'required',
			'quality' => 'required',
			'width' => 'required',
			'weight' => 'required',
			'orignal_meters' => 'required',
		]);

		if (isset($request->mill_id)) {
			$millreport = Millreport::find($request->mill_id);
		} else {
			$millreport = new Millreport;
		}

		$millreport->lot_numbers = $request->input('lot_numbers');
		$millreport->user_id = AUTH::user()->id;
		$millreport->yarn_quality = $request->input('quality');
		$millreport->tradename_id = $request->input('tradename_id');
		$millreport->width = $request->input('width');
		$millreport->weight = $request->input('weight');
		$millreport->number_of_pieces = $request->input('pieceno');
		$millreport->orignal_meters = $request->input('orignal_meters');
		$millreport->remarks = $request->input('remarks');
		$millreport->save();

		return redirect('admin/millreport')->with('message', 'Mill Has Been Store');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$data = Millreport::where('id', $id)->with('tradenames')->first();
		return view('admin.millreport.single', ['product' => $data]);

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$tradenames = Tradename::all();
		$mill = Millreport::where('id', $id)->first();
		return view('admin.millreport.edit', compact('mill', 'tradenames'));
	}

	/**
	 * Update the specified resource in storage Of Mill report.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
		$this->validate($request, [
			'lot.*' => 'required',
			'quality' => 'required',
			'treadname' => 'required',
			'width' => 'required',
			'waight' => 'required',
			'pieceno' => 'required',
			'totalmeter' => 'required',
		]);

		$lot_numbers = json_encode(array_values($request->input('lot')));
		$tradenames = Tradename::where('tradename', $request->treadname)->orwhere('design_id', '!=', NULL)->get();
		if (empty($tradenames) || $tradenames == '[]') {
			return back()->withErrors('Design not avaliable with ' . $request->treadname . ' this tradename');
		} else {
			$request->treadname = $tradenames[0]->id;
		}
		$millreport = Millreport::where('id', $id)->update([
			'lot_numbers' => $lot_numbers,
			'yarn_quality' => $request->input('quality'),
			'trade_name' => $request->treadname,
			'width' => $request->input('width'),
			'weight' => $request->input('waight'),
			'number_of_pieces' => $request->input('pieceno'),
			'remaining_meters' => $request->input('totalmeter'),
			'orignal_meters' => $request->input('totalmeter'),
			'remarks' => $request->input('remarks'),
		]);

		DB::table('notifications')->insert(
			['title' => 'Mill Updated', 'description' => 'Your Mill Has Been Updated With Number Of ' . $id]
		);
		if (Auth::user()->type == 'admin') {
			return redirect('admin/millreport')->with('message', 'Mill Report Updated');
		} else {
			return redirect('manager/millreport')->with('message', 'Mill Report Updated');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
		Millreport::where('id', $id)->delete();
		return back();
	}
	public function tradeid($trade_namename)
	{
		
		$trade= DB::table('tradenames')->where('name',$trade_namename)->first();
		return $trade->id;
	}
	public function searchrowproduct(Request $req) {
		//dd($req);
		$flag = 1;
		$trade='';
		if ($req->trade !== null or $req->mrno !== null) {
			if($req->trade!=null)
			{
			 $trade = $this->tradeid($req->trade);
			}
			 
			$mrno = $req->mrno;
			$row_product = Millreport::where('tradename_id', $trade)->orWhere('id', $mrno)->paginate(20);
			if ($trade != null) {
				$rowprod = $row_product->where('tradename_id', $trade);
			}
			if ($mrno != null) {
				$rowprod = $row_product->where('id', $mrno);
			}
			return view('admin.millreport.index', compact('row_product', 'flag'));
		} else {
			return back()->withErrors('Minimum one field is required');

		}
	}
	public function pendingmill() {

		$row_product = Millreport::whereRaw('millreports.used_meters < millreports.orignal_meters')
			->orderBy('id', 'DESC')->paginate(20);
		$unusedInventory = true;

		return view('admin.millreport.index', compact('row_product', 'unusedInventory'));

	}

	// Inventory Process start

	// Inventory add form

	// end

	// Inventory Store Process
	public function storeinventory(Request $request, $id) {
		$this->validate($request, [
			'des_number' => 'required|integer',
			'colour.*' => 'required',
			'quantity.*' => 'required|integer',

		], [
			'des_number.required' => 'Design number is required.',
			'des_number.integer' => 'Design number should be integer.',
		]
		);
		$design_id = Designs::select('id')->where('number', $request->des_number)->first();
		if (empty($design_id)) {
			return back()->withErrors('Design number does not exists')->withInput();
		}
		// dd($design_id['id']);
		$quant = $request->input('quantity');
		$color = $request->input('colour');
		$mill = Millreport::where('id', $request->id)->first();
		$totalMit = array_sum($quant);
		switch (true) {
		case ($mill->remaining_meters < $totalMit):
			return back()->withErrors("Entered Quantity is grater then Mill Report")->withInput(Input::all());
			break;
		}
		Millreport::where('id', $request->id)->update([
			'remaining_meters' => $mill->remaining_meters - $totalMit,
		]);
		foreach ($quant as $i_key => $i_value) {

			// dump($mill->remaining_meters - $quant[$i_key]);
			$inventory = new Inventory;
			$inventory->design_id = $design_id['id'];
			$inventory->color = $color[$i_key];
			$inventory->quantity = $quant[$i_key];
			$inventory->millreport_id = $request->id;
			$inventory->trade_name = $request->tradenameid;
			$inventory->user_id = Auth::user()->id;
			$inventory->save();

			$barcode = new Barcode;
			$btradename = substr($request->tradename, 0, 2);
			$barcode->inventory_id = $inventory->id;
			$barcode->barcode = $btradename . date("Y") . $color[$i_key] . $request->design_no . $inventory->id;
			$barcode->save();

		}
		// die();
		DB::table('notifications')->insert(
			['title' => 'Inventory Created', 'description' => 'Your Inventory Has Been Added In The Mill With ' . $inventory->design_id . ' Design Number']
		);
		if (Auth::user()->type == 'admin') {
			return redirect('admin/inventory')->with('message', 'Product Create Successfully');
		} else {
			return redirect('manager/inventory')->with('message', 'Product Create Successfully');
		}
	}
	//end

	// Inventory Upadte
	public function updateinventory(Request $request, $id) {
		$this->validate($request, [
			'des_number' => 'required',
			'colour.*' => 'required',
			'quantity.*' => 'required|integer',
		]);

		// $this->checkMillReportSize()
		$d_no = $request->input('des_number');
		$colour = $request->input('colour');
		$quant = $request->input('quantity');
		$orignalid = $request->input('orignalid');
		$mainid = $request->input('mainid');
		// Barcode Process
		$mill = Millreport::where('id', $request->id)->first();
		$totalMit = array_sum($quant);
		switch (true) {
		case ($mill->remaining_meters < $totalMit):
			return back()->withErrors("Entered Quantity is grater then Mill Report")->withInput(Input::all());
			break;
		}
		$designid = Designs::select('id')->where('number', $d_no)->first();

		$srno = $request->input('mainid');
		// if (mysql_num_rows($query) == 0) {
		foreach ($srno as $i_key => $i_value) {
			$data = Inventory::where('id', '=', $i_value)->get();
			if ($data == null or $data == '[]') {
				$inventory = new Inventory;
				$inventory->design_id = $designid->id;
				$inventory->color = $colour[$i_key];
				$inventory->quantity = $quant[$i_key];
				$inventory->millreport_id = $request->id;
				$inventory->trade_name = $request->tradename;
				$inventory->save();
				$barcode = new Barcode;
				$btradename = substr($request->tradename, 0, 2);
				$barcode->inventory_id = $inventory->id;
				$barcode->barcode = $btradename . date("Y") . $color[$i_key] . $d_no . $inventory->id;
				$barcode->save();

			} else {
				Inventory::where('id', $srno[$i_key])->update([
					'design_id' => $designid->id,
					'color' => $colour[$i_key],
					'Quantity' => $quant[$i_key],
					'millreport_id' => $request->id,
					'trade_name' => $request->tradename,

				]);
				$btradename = substr($request->tradename, 0, 2);
				Barcode::where('inventory_id', $srno[$i_key])->update([
					'barcode' => $btradename . date("Y") . $colour[$i_key] . $d_no . $srno[$i_key],

				]);
			}

		}
		DB::table('notifications')->insert(
			['title' => 'Inventory Updated', 'description' => 'Your Inventory Has Been Updated With Design No. of ' . $d_no]
		);
		if (Auth::user()->type == 'admin') {
			return redirect('admin/inventory')->with('message', 'Inventory Updated');
		} else {
			return redirect('manager/inventory')->with('message', 'Inventory Updated');
		}
	}
	// Inventory update End

	// Inventory Process End
}
