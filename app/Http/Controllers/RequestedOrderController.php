<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Client;
use App\Model\Order;

class RequestedOrderController extends Controller {
	public function index() {
		$entries = Order::with('orderproducts')->with('client')->where('Requested', '=', '1')->get();
		$entries = Order::with('orderproducts')->with('client')->where('Requested', '=', '1')->paginate(20);
		return view('manager.requestedorders', compact('entries'));
	}
	public function getOrderDetails($id) {
		$order = Order::where('id', '=', $id)->with('orderproducts')->with('client')->first();
		$client = Client::where('id', '=', $order['ClientId'])->first();
		return view('manager.orderdetails', compact('order', 'client'));
	}
}
