<?php

namespace App\Http\Middleware;

use Closure;
use App\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;

class ManagerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     protected $auth;
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    public function handle($request, Closure $next)
    {
        if($this->auth->getUser()->type !== "manager"){
            // return view('restricted');
            abort(403,'Unauthorized action');
        }
        return $next($request);
    }
}
