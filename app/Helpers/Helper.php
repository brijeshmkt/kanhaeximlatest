<?php
use App\Model\Designs;
use App\Model\Tradename;
use App\Model\Notification;

/*
type:
detail,
act for user id
 */
function addNotification(
	$type = 'test', $action = 'action', $detail = 'detail', $act_for_user_id = null
) {
	$notification = new Notification;
	$notification->act_of_user_id = Auth::user()->id;
	$notification->type = $type;
	$notification->action = $action;	
	$notification->detail = $detail;
	$notification->act_for_user_id = $act_for_user_id;
	$notification->save();
}

function getdesignnumber($id) {

	$res = Designs::where('id', '=', $id)->first();
	return $res->number;
}
function gettradename($id) {

	$res = Tradename::where('id', '=', $id)->first();
	return $res->name;
}