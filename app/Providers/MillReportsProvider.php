<?php

namespace App\Providers;

use App\Model\Millreport;
use Illuminate\Support\ServiceProvider;

class MillReportsProvider extends ServiceProvider {
	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot() {
		
	}

	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}
}
