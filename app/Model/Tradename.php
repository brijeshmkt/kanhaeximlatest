<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\OrderProduct;


class Tradename extends Model {
	protected $fillable = ['name','initial'];
	public $timestamps = false;

	// public function millreport() {
	// 	return $this->hasOne('App\Model\Millreport', 'trade_name', 'id'); // this matches the Eloquent model
	// }public function designs() {
	// 	return $this->hasOne('App\Model\Designs', 'id', 'design_id'); // this matches the Eloquent model
	// }


}
