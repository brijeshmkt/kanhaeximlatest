<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Notification extends Model {

	protected $fillable = [
		'type',
		'act_of_user_id',
		'act_for_user_id',
		'action',
		'detail',
		'read'
	];
}
