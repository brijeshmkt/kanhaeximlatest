<?php

namespace App\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends BaseModel {
	use softDeletes;

	protected $fillable = [
		'Name',
		'Address',
		'PhoneNo',
		'GstNumber',
	];

	public function orders() {
		return $this->belongsTo(Order::class, 'id', 'ClientId');
	}
}
