<?php

namespace App\Model;
use App\Model\Inventory;
use Illuminate\Database\Eloquent\SoftDeletes;

class RowProduct extends BaseModel {
	use softDeletes;

	protected $fillable = [
		'DesignNo',
		'rowproduct_image',
		'TradeName',
		'Color',
		'TotalSize',
		'WasteSize',
		'StockSize',
	];

	// protected $table = 'row_products';

	// protected $name = 'RowProductId';

	public function inventory() {
		return $this->hasMany('App\Model\Inventory', 'mr_id', 'id'); // this matches the Eloquent model
	}

}
