<?php

namespace App\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProduct extends BaseModel {
	use softDeletes;

	protected $fillable = [
		'user_id',
		'order_id',
		'tradename_id',
		'design_id',
		'quantity',
		'rate',
	];
	// public function inventory() {
	// 	return $this->hasOne(Inventory::class, 'design_id', 'design_id');
	// }
	public function order() {
		return $this->hasMany('App\Model\Order', 'id', 'order_id');
	}

	public function getDesignNoList() {
		return implode(',', $this->pluck('design_id'));
	}
	public function designs() {
		return $this->hasMany('App\Model\Designs', 'id', 'design_id');
	}

	// public function tradenames() {
	// 	return $this->hasOne('App\Model\Tradename', 'id', 'trade_name');
	// }

}
