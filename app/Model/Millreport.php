<?php

namespace App\Model;
use App\Model\Inventory;
use App\Model\Tradename;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Millreport extends Model {
	

	protected $fillable = [
		'date',
		'lot_numbers',
		'quality',
		'tradename_id',
		'width',
		'weight',
		'pieceno',
		'total_meters',
		'remarks',
	];

	public function tradenames()
	{
	    return $this->belongsTo('App\Model\Tradename', 'tradename_id');
	}

}
