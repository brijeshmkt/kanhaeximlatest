<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Designs extends Model {
	

	protected $fillable = [
		'number',
		'image',
		'description',
	];
	protected $table = 'designs';

	// public function inventory() {
	// 	return $this->hasMany('App\Model\Inventory', 'design_id', 'id'); // this matches the Eloquent model
	// }
	public function tradename() {
		return $this->belongsTo('App\Model\Tradename');
		
	}
}
