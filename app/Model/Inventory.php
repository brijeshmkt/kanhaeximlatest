<?php

namespace App\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends BaseModel {
	

	protected $fillable = [
		'millreport_id',
		'user_id',
		'tradename_id',
		'design_id',
		'color',
		'color_initial',
		'quantity'
	];

	public function millreport() {
		return $this->belongsTo('App\Model\Millreport', 'millreport_id', 'id');
	}
	public function designs() {
		return $this->hasOne(Designs::class, 'id', 'design_id');
	}
	public function barcodes() {
		return $this->hasOne(Barcode::class, 'inventory_id', 'id');
	}

	public function tradename() {
		return $this->belongsTo('App\Model\Tradename', 'tradename_id');
	}
	
}
