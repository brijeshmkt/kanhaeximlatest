<?php

namespace App\Model;

// use Illuminate\Database\Eloquent\Model;

class InventoryDetail extends BaseModel {
	use softDeletes;

	protected $fillable = [
		'Size',
		'DesignNo',
		'Quantity',
	];
}
