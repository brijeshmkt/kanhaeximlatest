<?php

namespace App\model;
use App\Model\Inventory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Barcode extends Model {
	//
	// use Notifiable, softDeletes;
	public $timestamps = false;
	protected $fillable = [
		'inventory_id',
		'barcode',
	];

	public function inventory() {
		return $this->hasOne('App\Model\Inventory', 'id', 'inventory_id'); // this matches the Eloquent model
	}
}
