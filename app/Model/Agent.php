<?php

namespace App\Model;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Agent extends Model {
	//
	use Notifiable, softDeletes;

	protected $fillable = [
		'agent_name',
		'firmname',
		'officeaddress',
		'city',
		'state',
		'country',
		'mobileno',
		'officeno',
		'landlineno',
		'email',
		'gstno',
	];

	public function user() {
		return $this->belongsTo('App\Model\User', 'agent_id', 'id');
	}
}
