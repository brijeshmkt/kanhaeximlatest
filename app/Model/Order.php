<?php

namespace App\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends BaseModel {
	use softDeletes;

	protected $fillable = ['Date', 'Station', 'T_p', 'Remark', 'Terms_Status', 'UserId', 'OrderMakerType', 'ClientId'];

	// public function client() {
	//     return $this->hasOne('App\Model\Client','ClientId','ClientId');
	// }

	// public function user() {
	//     return $this->hasOne('App\Model\User','id','UserId');
	// }

	public function client() {
		return $this->hasOne(Client::class, 'id', 'client_id');
	}

	public function orderproducts() {
		return $this->hasMany(OrderProduct::class, 'order_id', 'id');
	}

	public function users() {
		return $this->hasOne(User::class, 'id', 'user_id');
	}

}