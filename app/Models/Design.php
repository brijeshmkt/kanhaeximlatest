<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Design extends Model {
	

	protected $fillable = [
		'number',
		'image',
		'description',
	];

	// public function inventory() {
	// 	return $this->hasMany('App\Model\Inventory', 'design_id', 'id'); // this matches the Eloquent model
	// }
	public function tradename() {
		return $this->belongsTo('App\Model\Tradename');
		
	}
}
